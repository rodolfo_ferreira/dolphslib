package dolphs;

import java.io.IOException;

public class IdleTime {

    public static void main(String[] args) throws IOException, InterruptedException {
        while (true) {
            java.lang.Runtime rt = java.lang.Runtime.getRuntime();
            java.lang.Process p = rt.exec("xprintidle");
            p.waitFor();
            String commandResp = convertStreamToString(p.getInputStream());
            int idleTime = Integer.parseInt(commandResp);
            int seconds = idleTime / 1000;
            int minutes = seconds / 60;
            int hours = minutes / 60;
            int secondsRem = seconds % 60;
            int minutesRem = minutes % 60;
            String time;
            if (seconds < 60) {
                time = seconds + "s";
            } else if (hours == 0 && minutes < 60) {
                time = minutes + "m " + secondsRem + "s";
            } else {
                time = hours + "h " + minutesRem + "m " + secondsRem + "s";
            }
            System.out.printf(time);
            Thread.sleep(1000);
            if (minutes < 5) {
                System.out.printf("\b");
            } else {
                System.out.printf("\n");
            }
        }
    }

    private static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().trim() : "";
    }
}
