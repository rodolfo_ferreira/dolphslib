package dolphs.exceptions;

import dolphs.io.exceptions.FileSystemException;

public class CouldNotReadException extends FileSystemException {

    public CouldNotReadException(String absolutePath) {
        super(absolutePath);
    }
}
