package dolphs.exceptions;

public class FileNotCreatedException extends Exception {

    private static final String FORMATTER = "The file '%S' could not be created";

    public FileNotCreatedException(String absolutePath) {
        super(String.format(FORMATTER, absolutePath));
    }
}
