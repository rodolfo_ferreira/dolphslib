package dolphs.ia.machinelearning;

import dolphs.exceptions.CouldNotReadException;
import dolphs.io.FileUtils;
import dolphs.util.MatrixD;
import dolphs.util.VectorD;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

public class ArffFile {

    private String relation;

    private Hashtable<String, String> attributes;

    private Hashtable<String, VectorD<Double[]>> classData;

    private VectorD<String> classes;

    private MatrixD<String> data;

    public ArffFile(File file) throws IOException, CouldNotReadException {
        this.attributes = new Hashtable<>();
        this.classes = new VectorD<>();
        String content = FileUtils.read(file);
        processContent(content);
        this.classData = separeteDataClasses(data);
    }

    public ArffFile(String content) {
        this.attributes = new Hashtable<>();
        this.classes = new VectorD<>();
        processContent(content);
        this.classData = separeteDataClasses(data);
    }

    private void processContent(String string) {
        String[] lines = string.split("\n");
        int length = lines.length;
        for (int i = 0; i < length; i++) {
            String line = lines[i];
            if (line.length() > 3) {
                if (line.contains("@RELATION")) {
                    this.relation = extractRelation(line);
                } else if (line.contains("@ATTRIBUTE")) {
                    extractAttribute(line);
                } else if (line.contains("@DATA")) {
                    this.data = extractData(i, lines);
                }
            }
        }
    }

    private MatrixD<String> extractData(int index, String[] lines) {
        int length = lines.length;
        int start = index + 1;
        MatrixD<String> result = new MatrixD<String>(length - start, lines[start].split(",").length);
        for (int i = start; i < length; i++) {
            String[] attributes = lines[i].split(",");
            for (int j = 0; j < attributes.length; j++) {
                result.set(i - start, j, attributes[j]);
            }
        }
        return result;
    }

    private Hashtable<String, VectorD<Double[]>> separeteDataClasses(MatrixD<String> data) {
        Hashtable<String, VectorD<Double[]>> hashtable = new Hashtable<String, VectorD<Double[]>>();
        for (int i = 0; i < data.rowSize(); i++) {
            String[] attributes = new String[data.columnSize()];
            for (int j = 0; j < attributes.length; j++) {
                attributes[j] = data.get(i, j);
            }
            String className = attributes[data.columnSize() - 1];
            VectorD<Double[]> classList = hashtable.get(className);
            if (classList == null) {
                classList = new VectorD<Double[]>();
                hashtable.put(className, classList);
                classes.add(className);
            }
            Double[] continuousValues = convertToContinuous(attributes);
            classList.add(continuousValues);
        }
        return hashtable;
    }

    private Double[] convertToContinuous(String[] attributes) {
        int length = attributes.length;
        Double[] result = new Double[length];
        for (int i = 0; i < length; i++) {
            Double value = null;
            try {
                value = Double.parseDouble(attributes[i]);
            } catch (NumberFormatException e) {
                if (Boolean.parseBoolean(attributes[i]))
                    value = 1d;
                else
                    value = 0d;
            }
            if (value == null)
                throw new NullPointerException();
            result[i] = value;
        }
        return result;
    }

    private void extractAttribute(String line) {
        String key, value = "";
        if (line.contains("class") && line.contains("{") && line.contains("}")) {
            key = "class";
            value = line.substring(line.indexOf("{") + 1, line.indexOf("}"));
        } else {
            key = line.replaceAll("@ATTRIBUTE[ \t]+([A-z0-9]+)[ \tA-z]+", "$1");
            value = line.replaceAll("@ATTRIBUTE[ \t]+[A-z0-9]+[ \t]+([A-z0-9]+)", "$1");
        }

        attributes.put(key, value);
    }

    private String extractRelation(String line) {
        String result = line.replaceAll("@RELATION[ \t]+", "");
        return result;
    }

    public Hashtable<String, String> getAttributes() {
        return attributes;
    }

    public int getNumberofAttributes() {
        return attributes.size() - 1;
    }

    public MatrixD<String> getData() {
        return data;
    }

    public String getRelation() {
        return relation;
    }

    public Hashtable<String, VectorD<Double[]>> getClassData() {
        return classData;
    }

    public VectorD<String> getClasses() {
        return classes;
    }

    public int getNumberOfClasses() {
        return attributes.get("class").split(",").length;
    }

}
