package dolphs.ia.machinelearning;

import java.util.Random;

import dolphs.util.VectorD;

public class Perceptron {

	private VectorD<Double> weight;

	private double learningRate;

	private double threshold;

	public Perceptron(int vectorLength, double learningRate, double threshold,
			boolean randomWeight) {
		this.weight = new VectorD<Double>(vectorLength);
		this.learningRate = learningRate;
		this.threshold = threshold;
		startAlgorithm(vectorLength, randomWeight);
	}

	private void startAlgorithm(int vectorLength, boolean randomWeight) {
		// System.out.println("Starting");
		int length = vectorLength;
		for (int i = 0; i < length; i++) {
			Random random = new Random();
			double weightValue;
			if (randomWeight)
				weightValue = random.nextDouble();
			else
				weightValue = 0d;
			weight.add(weightValue); // TENTAR DEPOIS COM ALEATÓRIO
		}
	}

	public double activate(Double[] x, double d) throws ClassNotFoundException {
		// System.out.println("Activating");
		double y = signal(x);
		adaptWeigh(d, y, x);
		return y;
	}
	
	public double classify(Double[] x) throws ClassNotFoundException {
		// System.out.println("Activating");
		double y = signal(x);
		return y;
	}

	public double test(Double[] x, double d) throws ClassNotFoundException {
		// System.out.println("Testing");
		double y = signal(x);
		return y;
	}

	private double signal(Double[] x) {
		double sum = 0d;
		int result = 0;
		int length = weight.size();
		for (int i = 0; i < length; i++)
			sum += (weight.get(i) * x[i]);
		if (sum >= threshold)
			result = 1;
		return result;
	}

	public void adaptWeigh(double d, double y, Double[] x)
			throws ClassNotFoundException {
		// System.out.println("Adapting weight vector");
		double error = d - y;
		double correction = learningRate * error;
		int lenght = weight.size();
		for (int i = 0; i < lenght; i++) {
			double newWeight = weight.get(i) + (x[i] * correction);
			weight.set(i, newWeight);
		}
	}
}
