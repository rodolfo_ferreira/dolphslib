package dolphs.ia.machinelearning;

import java.util.Hashtable;

import dolphs.util.MatrixD;
import dolphs.util.VectorD;

public class PerceptronManager {

	private ArffFile arff;

	private MatrixD<Double> learningSet;

	// private MatrixD<Double> validationSet;

	private MatrixD<Double> testSet;

	private Perceptron perceptron;

	private int learningPercent;

	private int testPercent;

	private double falsePositive;

	private double falseNegative;

	private double truePositive;

	private double trueNegative;

	public PerceptronManager(ArffFile arff, Perceptron perceptron) {
		this.arff = arff;
		this.perceptron = perceptron;
		this.learningPercent = 0;
		this.testPercent = 0;
		this.testSet = new MatrixD<Double>();
		this.learningSet = new MatrixD<Double>();
	}

	private void processData(MatrixD<String> data) {
		VectorD<String> classes = arff.getClasses();
		Hashtable<String, VectorD<Double[]>> hashtable = arff.getClassData();
		if (classes.size() != 2)
			throw new ClassFormatError();
		VectorD<Double[]> firstClass = hashtable.get(classes.get(0));
		VectorD<Double[]> secondClass = hashtable.get(classes.get(1));
		extractLearning(firstClass, secondClass);
		extractTest(firstClass, secondClass);
	}

	private void extractLearning(VectorD<Double[]> firstClass,
			VectorD<Double[]> secondClass) {
		double totalPercent = learningPercent / 100d;
		double sizeA = firstClass.size() * totalPercent;
		double sizeB = secondClass.size() * totalPercent;

		for (int i = 0; i <= sizeA - 1; i++) {
			Double[] attributes = firstClass.get(i);
			if (dolphs.util.DolphsUtil.checkVectorNotNull(attributes)) {
				for (int j = 0; j < attributes.length; j++) {
					learningSet.set(i, j, attributes[j]);
				}
			}
		}

		for (int i = (int) sizeA; i < sizeA + sizeB; i++) {
			Double[] attributes = secondClass.get((int) (i - sizeA));
			if (dolphs.util.DolphsUtil.checkVectorNotNull(attributes)) {
				for (int j = 0; j < attributes.length; j++) {
					learningSet.set(i, j, attributes[j]);
				}
			}
		}
	}

	private void extractTest(VectorD<Double[]> firstClass,
			VectorD<Double[]> secondClass) {
		double learningPercent = this.learningPercent / 100d;
		double sizeLearningA = firstClass.size() * learningPercent;
		double sizeLearningB = secondClass.size() * learningPercent;

		double testPercent = this.testPercent / 100d;
		double sizeTestA = firstClass.size() * testPercent;
		double sizeTestB = secondClass.size() * testPercent;

		for (int i = 0; i <= sizeTestA - 1; i++) {
			Double[] attributes = firstClass.get((int) sizeLearningA + i);
			if (dolphs.util.DolphsUtil.checkVectorNotNull(attributes)) {
				for (int j = 0; j < attributes.length; j++) {
					testSet.set(i, j, attributes[j]);
				}
			}
		}

		for (int i = (int) sizeTestA; i < sizeTestA + sizeTestB; i++) {
			Double[] attributes = secondClass
					.get((int) (sizeLearningB + (i - sizeTestA)));
			if (dolphs.util.DolphsUtil.checkVectorNotNull(attributes)) {
				for (int j = 0; j < attributes.length; j++) {
					testSet.set(i, j, attributes[j]);
				}
			}
		}
	}

	public void setPercentage(int trainingPercent, int testPercent) {
		if (trainingPercent + testPercent > 100)
			throw new ArithmeticException();
		this.learningPercent = trainingPercent;
		this.testPercent = testPercent;
		processData(arff.getData());
	}

	public void train(int n) throws ClassNotFoundException {
		int x = 0;

		do {
			resetCounters();
			for (int i = 0; i < learningSet.rowSize(); i++) {
				Double[] attributes = new Double[learningSet.columnSize() - 1];
				for (int j = 0; j < learningSet.columnSize() - 1; j++) {
					attributes[j] = learningSet.get(i, j);
				}
				double d = learningSet.get(i, learningSet.columnSize() - 1);
				double y = perceptron.activate(attributes, d);

				if (d == y) {
					if (d == 1)
						truePositive++;
					else
						trueNegative++;
				} else {
					if (y == 0 && d == 1)
						falseNegative++;
					else
						falsePositive++;
				}
			}
			x++;
		} while (x < n);

	}

	public void train() throws ClassNotFoundException {
		int bestIteraction = 0;
		double bestResult = 0;
		int iteraction = 0;
		do {
			resetCounters();
			for (int i = 0; i < learningSet.rowSize(); i++) {
				Double[] attributes = new Double[learningSet.columnSize() - 1];
				for (int j = 0; j < learningSet.columnSize() - 1; j++) {
					attributes[j] = learningSet.get(i, j);
				}
				double d = learningSet.get(i, learningSet.columnSize() - 1);
				double y = perceptron.activate(attributes, d);

				if (d == y) {
					if (d == 1)
						truePositive++;
					else
						trueNegative++;
				} else {
					if (y == 0 && d == 1)
						falseNegative++;
					else
						falsePositive++;
				}
			}

			double total = learningSet.rowSize();
			double ok = truePositive + trueNegative;
			double result = ok / total * 100f;
			if (result > bestResult) {
				bestResult = result;
				bestIteraction = iteraction;
			}

			iteraction++;
		} while (iteraction < 1000 && bestResult < 100);

		System.err
				.printf("A iteração número %d foi a mais eficaz com %.2f%% de acertos.\n",
						bestIteraction, bestResult);

	}

	public void test() throws ClassNotFoundException {
		resetCounters();
		for (int i = 0; i < testSet.rowSize(); i++) {
			Double[] attributes = new Double[testSet.columnSize() - 1];
			for (int j = 0; j < testSet.columnSize() - 1; j++) {
				attributes[j] = testSet.get(i, j);
			}
			double d = testSet.get(i, testSet.columnSize() - 1);
			double y = perceptron.test(attributes, d);

			if (d == y) {
				if (d == 1)
					truePositive++;
				else
					trueNegative++;
			} else {
				if (y == 0 && d == 1)
					falseNegative++;
				else
					falsePositive++;
			}
		}
	}

	private void resetCounters() {
		falsePositive = 0;
		falseNegative = 0;
		truePositive = 0;
		trueNegative = 0;

	}

	public MatrixD<Object> getConfusionMatrix() {
		MatrixD<Object> result = new MatrixD<Object>();
		result.set(0, 0, "");
		result.set(0, 1, 1);
		result.set(0, 2, 0);
		result.set(1, 0, 1);
		result.set(1, 1, (int) truePositive);
		result.set(1, 2, (int) falseNegative);
		result.set(2, 0, 0);
		result.set(2, 1, (int) falsePositive);
		result.set(2, 2, (int) trueNegative);
		return result;
	}

	public MatrixD<Object> getEvaluationMatrix() {
		MatrixD<Object> result = new MatrixD<Object>();
		result.set(0, 0, "Acc.");
		result.set(0, 1, "Error");
		result.set(0, 2, "Prec.");
		result.set(0, 3, "Recall");
		result.set(0, 4, "F-Measure");
		String accPercent = String.format("%.2f%%", (getAccuracy() * 100));
		result.set(1, 0, accPercent);
		String errorPercent = String.format("%.2f%%", (getError() * 100));
		result.set(1, 1, errorPercent);
		String precisionPercent = String.format("%.2f%%",
				(getPrecision() * 100));
		result.set(1, 2, precisionPercent);
		String recallPercent = String.format("%.2f%%", (getRecall() * 100));
		result.set(1, 3, recallPercent);
		String fMeasurePercent = String.format("%.2f%%", (getFMeasure() * 100));
		result.set(1, 4, fMeasurePercent);
		return result;
	}

	public double getAccuracy() {
		double result = 0d;
		double total = falsePositive + falseNegative + trueNegative
				+ truePositive;
		result = (truePositive + trueNegative) / total;
		return result;
	}

	public double getError() {
		double result = 0d;
		double total = falsePositive + falseNegative + trueNegative
				+ truePositive;
		result = (falsePositive + falseNegative) / total;
		return result;
	}

	public double getPrecision() {
		double result = 0d;
		result = truePositive / (truePositive + falsePositive);
		return result;
	}

	public double getRecall() {
		double result = 0d;
		result = truePositive / (truePositive + falseNegative);
		return result;
	}

	public double getFMeasure() {
		double result = 0d;
		double precision = getPrecision();
		double recall = getRecall();
		result = 2 * ((precision * recall) / (precision + recall));
		return result;
	}

	public double classify(VectorD<Double> vector) throws ClassNotFoundException {
		Object[] array = vector.toArray();
		Double[] result = new Double[array.length];
		for (int i = 0; i < array.length; i++){
			result[i] = (Double) array[i];
		}
		return perceptron.classify(result);
		
	}
}
