package dolphs.io;

import java.io.File;

/**
 * The type File filter used by ClassesD to filter Files
 */
public class FileFilterD {

    private String[] extensions;

    private boolean findFiles;

    private boolean findDirectories;

    /**
     * Instantiates a new File filter;
     *
     * @param findDirectories if the filter should check files
     * @param findFiles if the filter should check directories
     * @param extensions the extensions to be filtered
     */
    public FileFilterD(boolean findDirectories, boolean findFiles, String... extensions) {
        this.findDirectories = findDirectories;
        this.findFiles = findFiles;
        this.extensions = extensions;
    }

    /**
     * Get extensions.
     *
     * @return the string array with the extensions
     */
    public String[] getExtensions() {
        return extensions;
    }

    /**
     * Sets extensions.
     *
     * @param extensions the extensions array to be seted
     */
    public void setExtensions(String[] extensions) {
        this.extensions = extensions;
    }

    boolean validate(File file) {
        boolean result = false;
        if (file.isDirectory() && findDirectories) {
            result = true;
        } else if (file.isFile() && findFiles) {
            result = validateFile(file);
        }
        return result;
    }

    private boolean validateFile(File file) {
        if (file == null) {
            return false;
        }

        boolean result = false;
        String fileName = file.getName();

        // If there is no extensions accepts all files
        if (extensions == null || extensions.length == 0) {
            result = true;
        } else {
            // Interects over the extensions and compares if the file is valid
            for (String extension : extensions) {
                if (fileName.toLowerCase().endsWith(extension.toLowerCase())) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }
}
