package dolphs.io;

import dolphs.exceptions.CouldNotReadException;
import dolphs.io.exceptions.CouldNotWriteException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    private static final String NOT_ALLOWED_CHARS = "\\\\/:*?<>|";

    public static String read(File file) throws CouldNotReadException {
        StringBuilder builder = new StringBuilder("");
        if (file.isFile() || !file.exists()) {
            try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr)) {
                while (br.ready()) {
                    builder.append(br.readLine());
                    builder.append('\n');
                }
                builder.setLength(builder.length() - 1);
            } catch (IOException e) {
                throw new CouldNotReadException(e.getMessage());
            }
        } else {
            throw new CouldNotReadException(file.getAbsolutePath());
        }
        return builder.toString();
    }

    public static void write(File file, String content) throws CouldNotWriteException {
        if (file.isFile() || !file.exists()) {
            File folder = file.getParentFile();
            if (!folder.exists()) {
                folder.mkdirs();
            }
            try (FileWriter fw = new FileWriter(file); BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write(content);
            } catch (IOException e) {
                e.printStackTrace();
                throw new CouldNotWriteException(file.getAbsolutePath());
            }
        } else {
            throw new CouldNotWriteException(file.getAbsolutePath());
        }
    }

    public static void write(File file, Object[] objects) throws CouldNotWriteException {
        StringBuilder build = new StringBuilder("");
        for (Object object : objects) {
            build.append(object.toString());
            build.append(", ");
        }
        build.setLength(build.length() - 2);
        FileUtils.write(file, build.toString());
    }

    public static File rename(File file, String newFileName) {
        String normalizedFileName = newFileName.replaceAll("[\\/:*?\"<>|]+", "");
        String path = file.getAbsolutePath();
        String directoryPath = path.substring(0, path.lastIndexOf(File.separator) + 1);
        String newPath = directoryPath + normalizedFileName;
        File newFile = new File(newPath);
        file.renameTo(newFile);
        return newFile;
    }

    public static File moveTo(File src, File dst, boolean overwrite) throws IOException {
        File resultFile = null;

        if (!src.exists()) {
            throw new FileNotFoundException(src.getAbsolutePath());
        }

        if (!dst.exists()) {
            String name = dst.getName();
            dst.getParentFile().mkdirs();
            if (name.matches(".+\\..{0,3}")) {
                dst.createNewFile();
            } else {
                dst.mkdirs();
            }
        }

        if (src.isFile()) {
            if (dst.isFile()) {
                resultFile = dst;
            } else {
                resultFile = new File(dst.getAbsolutePath(), src.getName());
            }
        } else {
            if (dst.isDirectory()) {
                String newDir = dst.getAbsolutePath();
                String oldDir = src.getParentFile().getAbsolutePath();

                List<File> files = listFiles(src);

                for (File file : files) {
                    String filePath = file.getAbsolutePath();
                    String newPath = filePath.replace(oldDir, newDir);

                    File newFile = new File(newPath);
                    file.renameTo(newFile);
                }

                resultFile = new File(dst.getAbsolutePath(), src.getName());
            } else {
                throw new IllegalStateException("Trying to move a Folder to a File");
            }
        }

        if (resultFile.exists() && overwrite) {
            resultFile.delete();
        }

        src.renameTo(resultFile);
        return resultFile;
    }

    public static void delete(File file) {
        if (file.isDirectory()) {
            List<File> files = list(file);
            for (File innerFile : files) {
                innerFile.delete();
            }
        }

        if (file.exists()) {
            file.delete();
        }
    }

    public static List<File> listFiles(File file) {
        List<File> files = new ArrayList<>();
        findFilesRecursively(files, file, new FileFilterD(false, true));
        return files;

    }

    public static List<File> list(File file) {
        return list(file, new FileFilterD(true, true));
    }

    public static List<File> list(File file, FileFilterD fileFilterD) {
        List<File> files = new ArrayList<>();
        findFilesRecursively(files, file, fileFilterD);
        return files;

    }

    public static List<File> listDirectories(File file) {
        List<File> files = new ArrayList<>();
        findFilesRecursively(files, file, new FileFilterD(true, false));
        return files;
    }

    /**
     * Finds all alements recursively.
     *
     * @param fileList the list of files to add
     * @param file the file to be evaluated
     * @param fileFilterD the File filter rule
     */
    private static void findFilesRecursively(List<File> fileList, File file, FileFilterD fileFilterD) {
        if (file.isDirectory()) {
            File[] filesList = file.listFiles();
            if (filesList != null) {
                for (File innerFile : filesList) {
                    findFilesRecursively(fileList, innerFile, fileFilterD);
                }
            }
        }

        if (fileFilterD == null || fileFilterD.validate(file)) {
            fileList.add(file);
        }
    }

    public static String normalizeForWindows(String string) {
        String notAllowedCharsWithoutSeparator = "[:\\*\\?<>|]+";
        return string.replaceAll(notAllowedCharsWithoutSeparator, "");
    }
}
