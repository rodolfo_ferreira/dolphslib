package dolphs.io.exceptions;

public class ConvertibleTypeNotFound extends RuntimeException {

    private static final String FORMATTER = "Could not found convertible type for %d";

    public ConvertibleTypeNotFound(int convertibleType) {
        super(String.format(FORMATTER, convertibleType));
    }
}
