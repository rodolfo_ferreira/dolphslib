package dolphs.io.exceptions;

public class CouldNotWriteException extends FileSystemException {

    private static final String FORMATTER = "The file '%S' could not be written";

    public CouldNotWriteException(String absolutePath){
        super(String.format(FORMATTER, absolutePath));

    }
}
