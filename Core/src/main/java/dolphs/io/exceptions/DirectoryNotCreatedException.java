/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.io.exceptions;

/**
 * The Directory Not Created Exception Class is used to show if a given
 * Directory couldn't be created due to some write issues.
 */
@SuppressWarnings("serial")
public class DirectoryNotCreatedException extends FileSystemException {

	/** The Constant Exception Message. */
	private static final String DIRECTORY_EXCEPTION_MESSAGE = "The directory \"%s\" couldn't be created";

	/**
	 * Instantiates a new directory not created exception.
	 *
	 * @param path
	 *            the directory path where the problem was found
	 */
	public DirectoryNotCreatedException(String path) {
		super(String.format(DIRECTORY_EXCEPTION_MESSAGE, path));
	}
}
