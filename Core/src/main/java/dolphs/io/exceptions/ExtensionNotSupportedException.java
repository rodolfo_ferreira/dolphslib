package dolphs.io.exceptions;

public class ExtensionNotSupportedException extends RuntimeException {

    private static final String FORMATTER = "The extension of the file \"%s\" is not supported. (Only .csv, .xls and " +
            ".xlsx are supported)";

    public ExtensionNotSupportedException(String fileName) {
        super(String.format(FORMATTER, fileName));
    }
}
