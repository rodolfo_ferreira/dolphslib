package dolphs.io.exceptions;

public class FileSystemException extends Exception {

    public FileSystemException(String message) {
        super(message);
    }
}
