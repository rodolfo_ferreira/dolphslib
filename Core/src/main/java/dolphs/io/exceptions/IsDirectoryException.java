/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.io.exceptions;

/**
 * The Is Directory Exception Class is used to show if a given path is a
 * Directory when a File is expected.
 */
@SuppressWarnings("serial")
public class IsDirectoryException extends FileSystemException {

	/** The Constant Exception Message. */
	private static final String DIRECTORY_EXCEPTION_MESSAGE = "The current path \"%s\" is a directory, try to use the Directory class instead.";

	/**
	 * Instantiates a new checks if is file exception.
	 *
	 * @param path
	 *            the path which the problem was found.
	 */
	public IsDirectoryException(String path) {
		super(String.format(DIRECTORY_EXCEPTION_MESSAGE, path));
	}
}
