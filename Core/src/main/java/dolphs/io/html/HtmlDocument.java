package dolphs.io.html;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dolphs.util.HtmlHelper;
import dolphs.util.VectorD;

public class HtmlDocument {

	private static final String ALL_REGEX = "[\n\t\r A-z0-9\\(\\)\\{\\}\\[\\]=\\-\"/\\>_\'.:,;&|?!]+";

	private final String html;

	private HtmlElement tree;

	private HtmlHelper helper;

	private VectorD<HtmlElement> fila;

	public HtmlDocument(String html) {
		this.helper = new HtmlHelper();
		this.html = html;
		fila = new VectorD<HtmlElement>();
		createHtmlTree();
	}

	// public void createHtmlTree() {
	//
	// String aux = impl.replaceAll("[\n]*<", "\n<");
	// lines = aux.split("<");
	// index = 1;
	// tree = new HtmlElement();
	// getElementNode(tree);
	// try {
	// FileD file = new FileD("C:\\Users\\rodol\\Documents\\Eclipse
	// Output\\console.txt");
	// file.write(CONSOLE_BUFFER);
	// } catch (Exception e) {
	//
	// }
	//
	// }

	public void createHtmlTree() {
		String aux = html.replaceAll("[\n]*<", "\n<");
		String[] lines = aux.split("<");
		tree = new HtmlElement();
		fila.add(tree);
		String tab = "";
		for (int i = 0; i < lines.length; i++) {
			String line = "<" + lines[i];
			if (line.length() > 2) {

				if (line.startsWith("/")) {
					HtmlElement element = fila.removeLast();
					HtmlElement lastElement = (HtmlElement) fila.getLast();
					lastElement.addChild(element);
					tab = tab.substring(0, tab.length() - 1);

				} else {

					HtmlElement element = readHtmlElement(line);
					if (helper.isVoidFunction(element.getType())) {
						fila.getLast().addChild(element);
					} else {
						tab += "\t";
						// CONSOLE_BUFFER += tab + element.getType() + "\n";
						fila.add(element);
					}
				}

			}
		}

		do {
			HtmlElement lastElement = fila.removeLast();
			fila.getLast().addChild(lastElement);
		} while (fila.size() > 1);
	}

	// private HtmlElement getElementNode(HtmlElement fElement) {
	// String line = readLine();
	// if (line.startsWith("/"))
	// return null;
	// HtmlElement element = readHtmlElement(line);
	// do {
	// if (nextLine() && !helper.isVoidFunction(fElement.getType())) {
	// CONSOLE_BUFFER += " " + fElement.getType() + "\n";
	// fElement.addChild(getElementNode(element));
	// }
	// } while (index < lines.length);
	// return element;
	// }

	private HtmlElement readHtmlElement(String line) {
		HtmlElement element = new HtmlElement();
		String tag;

		if (!line.startsWith("<")) {
			line = "<" + line;
		}

		core: {
			int spaceIndex = line.indexOf(" ");
			int symIndex = line.indexOf(">");
			try {
				if (spaceIndex < 0) {
					tag = line.substring(1, line.indexOf(">"));
				} else if (symIndex < 0) {
					tag = line.substring(1, line.indexOf(" "));
				} else if (spaceIndex < symIndex) {
					tag = line.substring(1, line.indexOf(" "));
				} else if (symIndex < spaceIndex) {
					tag = line.substring(1, line.indexOf(">"));
				} else {
					break core;
				}
			} catch (StringIndexOutOfBoundsException e) {
				break core;
			}

			element.setType(helper.getHtmlElement(tag));

			Pattern pattern = Pattern.compile("[A-z]+=\"" + ALL_REGEX + "?\"");
			Matcher matcher = pattern.matcher(line);
			if (matcher.find()) {
				for (int j = 0; j <= matcher.groupCount(); j++) {
					String part = matcher.group(j);
					if (part.startsWith("id=\"")) {
						try {
							element.setId(part.substring(part.lastIndexOf("=\"") + 2, part.lastIndexOf("\"")));
						} catch (StringIndexOutOfBoundsException e) {

						}
					}
					if (part.startsWith("class=\"")) {
						try {
							element.setClass(part.substring(part.lastIndexOf("=\"") + 2, part.lastIndexOf("\"")));
						} catch (StringIndexOutOfBoundsException e) {

						}
					}
				}
			}

			String content = line.substring(line.indexOf(">") + 1);
			if (content.replaceAll("[\n\r\t]+[ ]*", "").length() > 1)
				element.setContent(content);
		}
		return element;
	}

	public HtmlElement getTree() {
		return tree;
	}
}
