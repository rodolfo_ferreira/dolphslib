package dolphs.io.html;

import dolphs.util.ElementType;
import dolphs.util.VectorD;

public class HtmlElement {

	private ElementType type;

	private String id;

	private String cls;

	private VectorD<HtmlElement> children;

	private String content;

	public HtmlElement() {
		this.children = new VectorD<HtmlElement>();
	}

	public HtmlElement(ElementType type, String id, String cls) {
		this.type = type;
		this.id = id;
		this.cls = cls;
		this.children = new VectorD<HtmlElement>();
	}

	public HtmlElement(ElementType type) {
		this.type = type;
		children = new VectorD<HtmlElement>();
	}

	public HtmlElement getChildren(int index) {
		return children.get(index);
	}

	public int getChildrenSize() {
		return children.size();
	}

	public ElementType getType() {
		return type;
	}

	public void setType(ElementType type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCls() {
		return cls;
	}

	public String getContent() {
		return content;
	}

	public void setClass(String cls) {
		this.cls = cls;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void addChild(HtmlElement child) {
		this.children.add(child);
	}

	public void removeChild(HtmlElement child) {
		this.children.remove(child);
	}

	@Override
	public String toString() {
		return type + "";
	}
}
