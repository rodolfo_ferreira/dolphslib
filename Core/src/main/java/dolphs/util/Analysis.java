package dolphs.util;

public class Analysis {

	private long startTime;

	private long endTime;

	private long performance;

	public Analysis(long startTime, long endTime) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.performance = endTime - startTime;
	}

	public Analysis() {

	}

	public void start() {
		this.startTime = System.nanoTime();
	}

	public void end() {
		this.endTime = System.nanoTime();
		this.performance = endTime - startTime;
	}

	@Override
	public String toString() {

		int minutes = (int) (getPerformance() / 60000000000.0);
		int seconds = (int) (getPerformance() / 1000000000.0) - (minutes * 60);
		int millisecs = (int) (((getPerformance() / 1000000000.0) - (seconds + minutes * 60)) * 1000);

		if (minutes == 0 && seconds == 0)
			return millisecs + "ms";
		else if (minutes == 0 && millisecs == 0)
			return seconds + "s";
		else if (seconds == 0 && millisecs == 0)
			return minutes + "min";
		else if (minutes == 0)
			return seconds + "s " + millisecs + "ms";
		else if (seconds == 0)
			return minutes + "min " + millisecs + "ms";
		else if (millisecs == 0)
			return minutes + "min " + seconds + "s";

		return minutes + "min " + seconds + "s " + millisecs + "ms";
	}

	public long getPerformance() {
		return performance;
	}

}
