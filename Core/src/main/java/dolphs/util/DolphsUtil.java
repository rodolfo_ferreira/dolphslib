package dolphs.util;


public class DolphsUtil {

    public static boolean compareVector(Object[] object1, Object[] object2) {
        boolean result = true;
        int object1length = object1.length;
        int object2length = object2.length;
        if (object1length == object2length) {
            for (int i = 0; i < object1length; i++) {
                if (!object1[i].equals(object2[i])) {
                    result = false;
                }
            }
        } else {
            result = false;
        }

        return result;
    }

    public static boolean compareVector(double[] object1, double[] object2) {
        boolean result = true;
        int object1length = object1.length;
        int object2length = object2.length;
        if (object1length == object2length) {
            for (int i = 0; i < object1length; i++) {
                if (object1[i] != object2[i]) {
                    result = false;
                }
            }
        } else {
            result = false;
        }

        return result;
    }

    public static boolean checkVectorNotNull(Object[] vector) {
        boolean result = true;
        int length = vector.length;
        for (int i = 0; i < length; i++) {
            if (vector[i] == null) {
                result = false;
                break;
            }
        }
        return result;
    }

    public static boolean checkVectorNotNull(Double[] vector) {
        boolean result = true;
        int length = vector.length;
        for (int i = 0; i < length; i++) {
            if (vector[i] == null) {
                result = false;
                break;
            }
        }
        return result;
    }

    public static void copyMatrix(Object[][] from, Object[][] to) {
        int rowLength = from.length;
        int columnLength = from[0].length;
        for (int i = 0; i < rowLength; i++) {
            for (int j = 0; j < columnLength; j++) {
                to[i][j] = from[i][j];
            }
        }

    }

    public static boolean hasNull(Object... object) {
        int length = object.length;
        boolean result = false;
        for (int i = 0; i < length; i++) {
            if (object[i] == null) {
                result = true;
            }
        }
        return result;
    }

    public static void compareLines(String a, String b) {
        String[] linesA = a.split("\n");
        String[] linesB = b.split("\n");
        int size = linesB.length;
        if (linesA.length < linesB.length) {
            size = linesA.length;
        }
        for (int i = 0; i < size; i++) {
            if (!linesA[i].equals(linesB[i])) {
                System.out.println(linesA[i] + " != " + linesB[i]);
            }
        }
    }
}