package dolphs.util;

public enum ElementType {
	DOCTYPE, html, head, meta, link, title, titlescript, script, body, div, header, span, nav, a, section, button, p, ul, li, input, label, form, article, h1, h2, h3, h4, h5, h6, img, ol, noscript, textarea, footer, i, blockquote, cite, area, base, br, col, command, hr, keygen, param, track, wbr
}
