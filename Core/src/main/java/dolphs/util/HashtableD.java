package dolphs.util;

public class HashtableD<A, B, C> {

	private MatrixD<Object> matrix;

	private int line;

	public HashtableD() {
		matrix = new MatrixD<Object>();
		this.line = 0;
	}

	public void put(A a, B b, C c) {
		matrix.set(line, 0, a);
		matrix.set(line, 1, b);
		matrix.set(line, 2, c);
		line++;
	}

	public B getB(A a) {
		for (int i = 0; i < matrix.rowSize(); i++) {
			A object = (A) matrix.get(i, 0);
			if (a.equals(object))
				return (B) matrix.get(i, 1);

		}
		return null;
	}

	public C getC(A a) {
		for (int i = 0; i < matrix.rowSize(); i++) {
			A object = (A) matrix.get(i, 0);
			if (a.equals(object))
				return (C) matrix.get(i, 2);

		}
		return null;
	}

	public B getLastB() {
		return (B) matrix.get(line - 1, 1);
	}

	public C getLastC() {
		return (C) matrix.get(line - 1, 2);
	}

}
