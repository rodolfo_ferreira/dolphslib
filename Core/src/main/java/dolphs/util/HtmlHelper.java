package dolphs.util;

import java.util.Hashtable;

public class HtmlHelper {

	private VectorD<ElementType> voidFunctions;

	private Hashtable<String, ElementType> htmlTag;

	public HtmlHelper() {
		htmlTag = new Hashtable<String, ElementType>();
		voidFunctions = new VectorD<ElementType>();

		voidFunctions.add(ElementType.meta);
		voidFunctions.add(ElementType.area);
		voidFunctions.add(ElementType.base);
		voidFunctions.add(ElementType.br);
		voidFunctions.add(ElementType.col);
		voidFunctions.add(ElementType.command);
		voidFunctions.add(ElementType.hr);
		voidFunctions.add(ElementType.img);
		voidFunctions.add(ElementType.input);
		voidFunctions.add(ElementType.keygen);
		voidFunctions.add(ElementType.link);
		voidFunctions.add(ElementType.param);
		voidFunctions.add(ElementType.track);
		voidFunctions.add(ElementType.wbr);

		htmlTag.put("!DOCTYPE", ElementType.DOCTYPE);
		htmlTag.put("a", ElementType.a);
		htmlTag.put("article", ElementType.article);
		htmlTag.put("blockquote", ElementType.blockquote);
		htmlTag.put("button", ElementType.button);
		htmlTag.put("body", ElementType.body);
		htmlTag.put("cite", ElementType.cite);
		htmlTag.put("div", ElementType.div);
		htmlTag.put("footer", ElementType.footer);
		htmlTag.put("form", ElementType.form);
		htmlTag.put("h1", ElementType.h1);
		htmlTag.put("h2", ElementType.h2);
		htmlTag.put("h3", ElementType.h3);
		htmlTag.put("h4", ElementType.h4);
		htmlTag.put("h5", ElementType.h5);
		htmlTag.put("h6", ElementType.h6);
		htmlTag.put("head", ElementType.head);
		htmlTag.put("header", ElementType.header);
		htmlTag.put("impl", ElementType.html);
		htmlTag.put("i", ElementType.i);
		htmlTag.put("img", ElementType.img);
		htmlTag.put("input", ElementType.input);
		htmlTag.put("label", ElementType.label);
		htmlTag.put("li", ElementType.li);
		htmlTag.put("link", ElementType.link);
		htmlTag.put("meta", ElementType.meta);
		htmlTag.put("nav", ElementType.nav);
		htmlTag.put("noscript", ElementType.noscript);
		htmlTag.put("ol", ElementType.ol);
		htmlTag.put("p", ElementType.p);
		htmlTag.put("script", ElementType.script);
		htmlTag.put("section", ElementType.section);
		htmlTag.put("span", ElementType.span);
		htmlTag.put("textarea", ElementType.textarea);
		htmlTag.put("title", ElementType.title);
		htmlTag.put("titlescript", ElementType.titlescript);
		htmlTag.put("ul", ElementType.ul);
	}

	public ElementType getHtmlElement(String elemnt) {
		return htmlTag.get(elemnt);

	}

	public boolean isVoidFunction(ElementType element) {
		boolean result = false;
		if (voidFunctions.contains(element))
			result = true;
		return result;
	}

}
