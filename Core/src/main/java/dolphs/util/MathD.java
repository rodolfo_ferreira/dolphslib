package dolphs.util;

public class MathD {

	public static double calculateProbDrop(double rate, int times) {
		double result = 0d;
		double x = 1d - (rate / 100);
		double y = Math.pow(x, times);
		result = 1d - y;
		return result;

	}

	public static void main(String[] args) {
		float result = (float) MathD.calculateProbDrop(0.3d, 400);
		System.out.println(result);
	}

}
