package dolphs.util;

import java.util.List;

public class MatrixD<T extends Object> {

    private int columnSize;

    private int rowSize;

    private Object[][] elements;

    public MatrixD() {
        this(10, 10);
    }

    public MatrixD(int rowSize, int columnSize) {
        this.rowSize = 0;
        this.columnSize = 0;
        this.elements = new Object[rowSize][columnSize];

    }

    public MatrixD(T[][] matrix) {
        this.elements = matrix;
        findSizes();

    }

    public void set(int row, int column, T item) {
        checkSize(row + 1, column + 1);
        if (row >= rowSize)
            rowSize = row + 1;
        if (column >= columnSize)
            columnSize = column + 1;
        elements[row][column] = item;
    }

    private void checkSize(int minRowLength, int minColumnLength) {
        int currentRowLength = elements.length;
        int currentColumnLength = elements[0].length;
        if (currentRowLength < minRowLength && currentColumnLength < minColumnLength) {
            Object[][] newMatrix = new Object[currentRowLength + (currentRowLength / 2)][currentColumnLength +
                    (currentColumnLength / 2)];
            dolphs.util.DolphsUtil.copyMatrix(elements, newMatrix);
            this.elements = newMatrix;
        } else if (currentRowLength < minRowLength) {
            Object[][] newMatrix = new Object[currentRowLength + (currentRowLength / 2)][currentColumnLength];
            dolphs.util.DolphsUtil.copyMatrix(elements, newMatrix);
            this.elements = newMatrix;
        } else if (currentColumnLength < minColumnLength) {
            Object[][] newMatrix = new Object[currentRowLength][currentColumnLength + (currentColumnLength / 2)];
            dolphs.util.DolphsUtil.copyMatrix(elements, newMatrix);
            this.elements = newMatrix;
        }

    }

    private void findSizes() {
        for (int i = 0; i < elements.length; i++) {
            for (int j = 0; j < elements[i].length; j++) {
                if (elements[i][j] != null) {
                    if (i > rowSize())
                        this.rowSize = i;
                    if (j > columnSize()) {
                        this.columnSize = j;
                    }
                }
            }
        }
    }

    public T get(int row, int column) {
        T result = null;
        if (row < rowSize() || column < columnSize())
            result = (T) elements[row][column];
        else
            throw new ArrayIndexOutOfBoundsException();
        return result;
    }

    public void addLine(List<T> lineElements) {
        addLine(0, lineElements);
    }

    public void addLine(int offset, List<T> lineElements) {
        checkSize(rowSize + 1, lineElements.size());
        for (int i = 0; i < lineElements.size(); i++) {
            elements[rowSize][offset + i] = lineElements.get(i);
        }
    }

    // public void remove(int row, int column) {
    // elements[row][column] = null;
    // }

    @Override
    public String toString() {
        String result = "";
        StringBuilder build = new StringBuilder("");
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < columnSize; j++) {
                build.append(get(i, j) + "\t");
            }
            build.append("\n");
        }
        result = build.toString();
        return result;
    }

    public VectorD<T> toVector() {
        VectorD<T> result = new VectorD<T>();
        for (int i = 0; i < elements.length; i++) {
            Object[] row = elements[i];
            for (int j = 0; j < row.length; j++) {
                result.add((T) elements[i][j]);
            }
        }
        return result;
    }

    public int columnSize() {
        return columnSize;
    }

    public int rowSize() {
        return rowSize;
    }
}
