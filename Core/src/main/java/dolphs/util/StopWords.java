/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.util;

// TODO: Auto-generated Javadoc
/**
 * The Class StopWords.
 */
public class StopWords {

	/** The stop words. */
	private static String stopWords[] = { "a", "about", "above", "above", "across", "after", "afterwards", "again",
			"against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among",
			"amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway",
			"anywhere", "are", "around", "as", "at", "back", "be", "became", "because", "become", "becomes", "becoming",
			"been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond",
			"bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt",
			"cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either",
			"eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything",
			"everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former",
			"formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has",
			"hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers",
			"herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed",
			"interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less",
			"ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most",
			"mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless",
			"next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off",
			"often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours",
			"ourselves", "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see",
			"seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since",
			"sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes",
			"somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves",
			"then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they",
			"thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus",
			"to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until",
			"up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence",
			"whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether",
			"which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within",
			"without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the" };

	/**
	 * Filtrar stop words.
	 *
	 * @param palavra
	 *            the palavra
	 * @return the string
	 */
	// public static String filtrarStopWords(String palavra) {
	// boolean contem = false;
	// String palavraReconstruida = "";
	//
	// String partido[] = palavra.toLowerCase().split(" ");
	//
	// for (int i = 0; i < partido.length; i++) {
	//
	// if (partido[i].endsWith(",") || partido[i].endsWith(" "))
	// partido[i] = partido[i].substring(0, partido[i].length() - 1);
	// if (partido[i].startsWith(" "))
	// partido[i] = partido[i].substring(1, partido[i].length());
	//
	// EnglishAnalyzer en_an = new EnglishAnalyzer(Version.LUCENE_48);
	// QueryParser parser = new QueryParser(Version.LUCENE_48, "", en_an);
	// try {
	// partido[i] = parser.parse(partido[i]).toString();
	// } catch (ParseException e) {
	//
	// }
	//
	// for (int j = 0; j < stopWords.length; j++) {
	//
	// if (partido[i].equals(stopWords[j])) {
	// contem = true;
	// break;
	// } else {
	// contem = false;
	// }
	// }
	// if (!contem) {
	// palavraReconstruida += partido[i] + " ";
	// }
	// }
	// return palavraReconstruida;
	// }

	public static String removeStopWords(String text) {
		String result = "";
		String[] split = text.split("\\W");
		int length = split.length;
		for (int i = 0; i < length; i++) {
			if (!isStopWord(split[i])) {
				result += split[i] + " ";
			}
		}
		return result;
	}

	private static boolean isStopWord(String word) {
		boolean result = false;
		int length = stopWords.length;
		for (int i = 0; i < length; i++) {
			if (word.toLowerCase().equals(stopWords[i])) {
				result = true;
				break;
			}
		}
		return result;
	}

}
