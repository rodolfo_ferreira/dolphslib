/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc

/**
 * The Class Text.
 */
public class StringD {

    /**
     * The text.
     */
    private String string;

    /**
     * Instantiates a new text.
     *
     * @param text the text
     */
    public StringD(String text) {
        this.string = new String(text);
    }

    public void formatStartEnd() {
        string = string.replaceAll("^[\n\t\r]+", "");
        string = string.replaceAll("[\n\t\r]+$", "");
    }

    public void removeBlankLines() {
        string = string.replaceAll("[\n]{3,}", "\n\n");
    }

    public void removeBlankLines(int max) {
        String regexBlank = "[\n]{%d,}";
        String replace = "";
        max = max + 1;

        for (int i = 0; i < max; i++) {
            replace += "\n";
        }
        string = string.replaceAll(String.format(regexBlank, max + 1), replace);
    }

    public void concat(String... strings) {
        for (int i = 0; i < strings.length; i++) {
            string += strings[i];
        }

    }

    /**
     * Gets the sentences.
     *
     * @return the sentences
     */
    public ArrayList<String> getSentences() {
        ArrayList<String> result = new ArrayList<String>();
        Pattern re = Pattern.compile("[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(?=\\s|$)", Pattern.MULTILINE | Pattern.COMMENTS);
        Matcher reMatcher = re.matcher(string);
        while (reMatcher.find()) {
            result.add(reMatcher.group());
        }
        return result;
    }

}
