package dolphs.util;

import java.util.List;
import java.util.Map;

/**
 * This class handles the manipulations on Strings
 */
public class StringUtils {

    private StringUtils() {}

    /**
     * This class gets a list placing the elements side by side with ', ' delimiter
     * @param list, the list to be used
     * @return the String formatted
     */
    public static String asString(List<?> list) {
        return asString(list, ", ");
    }

    /**
     * This class gets a list placing the elements side by side with given delimiter
     * @param list, the list to be used
     * @param delimiter, the delimiter to be placed in each item
     * @return the String formatted
     */
    public static String asString(List<?> list, String delimiter) {
        StringBuilder attributesBuilder = new StringBuilder("");
        list.forEach(item -> {
            attributesBuilder.append(item);
            attributesBuilder.append(delimiter);
        });
        if (attributesBuilder.length() > delimiter.length()) {
            attributesBuilder.setLength(attributesBuilder.length() - delimiter.length());
        }
        return attributesBuilder.toString();
    }

    /**
     * This class appends the key value and then adds the delimiter ','
     * @param map, the Map containing the keys and values
     * @return the formatted String
     */
    public static String asString(Map<?, ?> map, boolean invertValues) {
        StringBuilder attributesBuilder = new StringBuilder("");
        map.forEach((key, value) -> {
            if (invertValues) {
                attributesBuilder.append(value.toString());
                attributesBuilder.append(" ");
                attributesBuilder.append(key.toString());
                attributesBuilder.append(", ");
            } else {
                attributesBuilder.append(key.toString());
                attributesBuilder.append(" ");
                attributesBuilder.append(value.toString());
                attributesBuilder.append(", ");
            }

        });
        if (attributesBuilder.length() > 2) {
            attributesBuilder.setLength(attributesBuilder.length() - 2);
        }
        return attributesBuilder.toString();
    }
}
