package dolphs.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Table<T> {

    private List<List<T>> table;

    private int columnSize;

    public Table() {
        table = new ArrayList<>();
        columnSize = 0;
    }

    public Table(T[][] matrix) {
        for (T[] row : matrix) {
            List<T> list = new ArrayList<>(Arrays.asList(row));
            table.add(list);
            if (columnSize < list.size()) {
                columnSize = list.size();
            }
        }
    }

    public int rowSize() {
        return table.size();
    }

    public int columnSize() {
        return columnSize;
    }

    public void addRow(List<T> values) {
        table.add(values);
        if (columnSize < values.size()) {
            columnSize = values.size();
        }
    }

    public void removeRow(int index) {
        table.remove(index);
    }

    public void setRow(int index, List<T> values) {
        table.set(index, values);
    }

    public List<T> getRow(int index) {
        return table.get(index);
    }

    public void addColumn(List<T> values) {
        for (int i = 0; i < values.size(); i++) {
            T value = values.get(i);
            if (i > table.size()) {
                table.add(new ArrayList<>());
            }
            List<T> row = table.get(i);
            while (row.size() < columnSize) {
                row.add(null);
            }
            row.add(value);
        }
    }

    public void removeColumn(int index) {
        for (List<T> row : table) {
            row.remove(index);
        }
    }

    public void setColumn(int index, List<T> values) {
        for (int i = 0; i < values.size(); i++) {
            T value = values.get(i);
            if (i > table.size()) {
                table.add(new ArrayList<>());
            }
            List<T> row = table.get(i);
            row.set(index, value);
        }
    }

    public List<T> getColumn(int index) {
        List<T> result = new ArrayList<>();
        for (List<T> row : table) {
            result.add(row.get(index));
        }
        return result;
    }

    public T getCell(int row, int column) {
        return table.get(row).get(column);
    }

    public void setCell(int row, int column, T element) {
        table.get(row).set(column, element);
    }
}
