/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class Text.
 */
public class Text {

	/** The text. */
	private String text;

	/**
	 * Instantiates a new text.
	 *
	 * @param text the text
	 */
	public Text(String text) {
		this.text = text;
	}

	/**
	 * Gets the sentences.
	 *
	 * @return the sentences
	 */
	public ArrayList<String> getSentences() {
		ArrayList<String> result = new ArrayList<String>();
		Pattern re = Pattern
				.compile(
						"[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(?=\\s|$)",
						Pattern.MULTILINE | Pattern.COMMENTS);
		Matcher reMatcher = re.matcher(text);
		while (reMatcher.find()) {
			result.add(reMatcher.group());
		}
		return result;
	}

}
