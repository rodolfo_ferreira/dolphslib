/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.util;

import java.util.AbstractList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

// TODO: Auto-generated Javadoc
/**
 * The Class ListD.
 *
 * @param <E>
 *            the generic type
 */
@SuppressWarnings("unchecked")
@Deprecated
public class VectorD<E> extends AbstractList<E> implements List<E>, Cloneable,
		java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2082733510075753787L;

	/** The objects. */
	private transient E[] elements;

	/** The size. */
	private int size;

	/**
	 * Instantiates a new list d.
	 */
	public VectorD() {
		this(10);
	}

	/**
	 * Instantiates a new list d.
	 */
	public VectorD(int initialSize) {
		super();
		if (initialSize < 0) {
			throw new IllegalArgumentException("Illegal Size: " + initialSize);
		}
		elements = (E[]) new Object[initialSize];
	}

	/**
	 * Adds the.
	 *
	 * @param e
	 *            the e
	 */
	public boolean add(E e) {
		checkSize(size() + 1);
		elements[size++] = e;
		return true;
	}

	/**
	 * Adds the distinct.
	 *
	 * @param t
	 *            the t
	 * @return true, if successful
	 */
	public boolean addDistinct(E t) {
		boolean hasIt = false;
		for (int i = 0; i < size(); i++) {
			E thisT = (E) elements[i];
			if (t.equals(thisT)) {
				hasIt = true;
				break;
			}
		}
		if (!hasIt)
			add(t);

		return !hasIt;
	}

	/**
	 * Removes the.
	 *
	 * @param object
	 *            the t
	 */
	public boolean remove(Object object) {
		for (int i = 0; i < size(); i++) {
			E thisT = (E) elements[i];
			if (object.equals(thisT)) {
				remove(i);
				break;
			}
		}
		return true;
	}

	/**
	 * Removes the all.
	 *
	 * @param t
	 *            the t
	 */
	public void removeAll(E t) {
		for (int i = 0; i < size(); i++) {
			E thisT = (E) elements[i];
			if (thisT.equals(t))
				remove(t);
		}
	}

	/**
	 * Removes the.
	 *
	 * @param index
	 *            the index
	 */
	public E remove(int index) {
		E result = elements[index];
		Object[] vector = new Object[size() - 1];
		for (int i = 0; i < size(); i++) {
			for (int j = 0; j < size() - 1; j++) {
				if (i != index)
					vector[j] = elements[i];
			}
		}
		setSize(size() - 1);
		return result;
	}

	/**
	 * Gets the.
	 *
	 * @param i
	 *            the i
	 * @return the t
	 */
	public E get(int i) {
		if (i >= size())
			throw new ArrayIndexOutOfBoundsException();
		return (E) elements[i];
	}

	/**
	 * Removes the all recorrences.
	 */
	public void removeAllRecorrences() {
		Object[] aux = new Object[size()];
		aux[0] = elements[0];
		int newLength = 1;
		for (int i = 0; i < elements.length; i++) {
			E object = (E) elements[i];
			boolean hasIt = false;
			for (int j = 0; j < newLength; j++) {
				E auxiliar = (E) aux[j];
				if (object != null && object.equals(auxiliar))
					hasIt = true;
			}
			if (!hasIt && object != null) {
				aux[newLength] = object;
				newLength++;
			}
		}
		elements = (E[]) aux;
		setSize(newLength);
	}

	/**
	 * Increase.
	 */
	private void checkSize(int minLegth) {
		int currentLength = elements.length;
		if (minLegth > currentLength) {
			Object[] vector = new Object[currentLength + (currentLength / 2)];
			for (int i = 0; i < elements.length; i++) {
				vector[i] = elements[i];
			}

			elements = (E[]) vector;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = "";
		StringBuilder build = new StringBuilder("");
		for (int i = 0; i < size(); i++) {
			build.append(elements[i] + ",");
		}
		result = build.toString();
		return result;
	}

	/**
	 * Size.
	 *
	 * @return the int
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Sets the size.
	 *
	 * @param size
	 *            the new size
	 */
	public void setSize(int size) {
		// TODO CHECK;
		this.size = size;
	}

	@Override
	public void add(int position, E e) {
		// TODO CHECK
		checkSize(position + 1);
		elements[position] = e;
	}

	@Override
	public boolean addAll(Collection<? extends E> collection) {
		Object[] collectionArray = collection.toArray();
		int colSize = collectionArray.length;
		checkSize(colSize);
		for (int i = 0; i < colSize; i++) {
			E e = (E) collectionArray[i];
			add(e);
		}
		return true;
	}

	@Override
	public boolean addAll(int beginIndex, Collection<? extends E> collection) {
		Object[] collectionArray = collection.toArray();
		int colSize = collectionArray.length;
		checkSize(colSize);
		for (int i = 0; i < colSize; i++) {
			elements[beginIndex + i] = (E) collectionArray[i];
		}
		return true;
	}

	@Override
	public void clear() {
		for (int i = 0; i < size(); i++) {
			elements[i] = null;
		}

	}

	@Override
	public boolean contains(Object object) {
		boolean result = false;
		for (int i = 0; i < size(); i++) {
			if (object.equals(elements[i])) {
				result = true;
				break;
			}
		}
		return result;
	}

	@Override
	public boolean containsAll(Collection<?> collection) {
		boolean result = true;
		Object[] colArray = collection.toArray();
		for (int i = 0; i < colArray.length; i++) {
			boolean hasIt = false;
			for (int j = 0; j < size(); j++) {
				if (colArray[i].equals(elements[j])) {
					hasIt = true;
					break;
				}
			}
			if (!hasIt) {
				result = false;
				break;
			}
		}
		return result;
	}

	@Override
	public int indexOf(Object object) {
		int result = -1;
		for (int i = 0; i < size(); i++) {
			if (object.equals(elements[i])) {
				result = i;
				break;
			}
		}
		return result;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public Iterator<E> iterator() {
		return new Itr();
	}

	@Override
	public int lastIndexOf(Object object) {
		int result = -1;
		for (int i = size() - 1; i >= 0; i++) {
			if (object.equals(elements[i])) {
				result = i;
				break;
			}
		}
		return result;
	}

	@Override
	public ListIterator<E> listIterator() {
		return new ListItr(0);
	}

	@Override
	public ListIterator<E> listIterator(int arg0) {
		return new ListItr(arg0);
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public E set(int index, E element) {
		checkRange(index);
		E old = elements[index];
		elements[index] = element;
		return old;
	}

	private void checkRange(int index) {
		if (index >= size)
			throw new IndexOutOfBoundsException();
	}

	@Override
	public List<E> subList(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		Object[] result = new Object[size()];
		for (int i = 0; i < size(); i++) {
			result[i] = elements[i];
		}

		return result;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}

    public E removeLast() {
		return elements[--size];
    }

    private class Itr implements Iterator<E> {
		int cursor; // index of next element to return
		int lastRet = -1; // index of last element returned; -1 if no such
		int expectedModCount = modCount;

		@Override
		public boolean hasNext() {
			return cursor != size;
		}

		@Override
		public E next() {
			checkForComodification();
			int i = cursor;
			if (i >= size)
				throw new NoSuchElementException();
			Object[] elementData = VectorD.this.elements;
			if (i >= elementData.length)
				throw new ConcurrentModificationException();
			cursor = i + 1;
			return (E) elementData[lastRet = i];
		}

		final void checkForComodification() {
			if (modCount != expectedModCount)
				throw new ConcurrentModificationException();
		}

	}

	private class ListItr extends Itr implements ListIterator<E> {

		ListItr(int index) {
			super();
			cursor = index;
		}

		@Override
		public boolean hasPrevious() {
			return cursor != 0;
		}

		@Override
		public E previous() {
			checkForComodification();
			int i = cursor - 1;
			if (i < 0)
				throw new NoSuchElementException();
			Object[] elementData = VectorD.this.elements;
			if (i >= elementData.length)
				throw new ConcurrentModificationException();
			cursor = i;
			return (E) elementData[lastRet = i];
		}

		@Override
		public int nextIndex() {
			return cursor;
		}

		@Override
		public int previousIndex() {
			return cursor - 1;
		}

		@Override
		public void set(E e) {
			if (lastRet < 0)
				throw new IllegalStateException();
			checkForComodification();

			try {
				VectorD.this.set(lastRet, e);
			} catch (IndexOutOfBoundsException ex) {
				throw new ConcurrentModificationException();
			}

		}

		@Override
		public void add(E e) {
			checkForComodification();

			try {
				int i = cursor;
				VectorD.this.add(i, e);
				cursor = i + 1;
				lastRet = -1;
				expectedModCount = modCount;
			} catch (IndexOutOfBoundsException ex) {
				throw new ConcurrentModificationException();
			}

		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}

	}

	public E getLast() {
		return elements[size() - 1];
	}
}
