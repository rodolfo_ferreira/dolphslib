/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.util.database;

import dolphs.io.exceptions.IsDirectoryException;
import dolphs.io.exceptions.IsFileException;

import java.io.File;

// TODO: Auto-generated Javadoc
/**
 * The Class Script.
 */
public class Script {

	/** The script path. */
	private String scriptPath;

	/**
	 * Instantiates a new script.
	 *
	 * @param scriptPath the script path
	 */
	public Script(String scriptPath) {
		this.scriptPath = scriptPath;
	}

	/**
	 * To schema.
	 *
	 * @return the string
	 * @throws IsDirectoryException the is directory exception
	 * @throws IsFileException the is file exception
	 */
	public String toSchema() throws IsDirectoryException, IsFileException {
		String result = "";
		File file = new File(scriptPath);
		String content = file.toString();
		String[] tables = content.split(";");

		result += StringHelper.SCHEMA_FILE_HEADER;

		for (int i = 0; i < tables.length; i++) {
			if (tables[i].length() > 10) {
				String tableName = tables[i].substring(
						tables[i].indexOf("TABLE") + 5, tables[i].indexOf("("))
						.replace(" ", "");

				result += "\t$" + StringHelper.SCHEMA + "['"
						+ StringHelper.TABLE_PREFIX + "_" + tableName
						+ "'] = array(\n";
				result += getFields(tables[i]
						.substring(tables[i].indexOf("(") + 1));
				result += getPrimaryKey(tables[i]);
				result += getForeignKey(tables[i]);
				result += "\t);\n\n";
			}
		}
		result += "\n\treturn $schema;\n}";
		return result;
	}

	/**
	 * Gets the fields.
	 *
	 * @param table the table
	 * @return the fields
	 */
	private String getFields(String table) {
		String result = "\t\t'" + StringHelper.SCHEMA_FIELDS + "' "
				+ StringHelper.SCHEMA_ASSIGNMENT + " array(\n";
		String[] fields = table.split(",");
		for (int i = 0; i < fields.length; i++) {
			if (!fields[i].contains("PRIMARY KEY")
					&& !fields[i].contains("CONSTRAINT")
					&& !fields[i].contains("UNIQUE")
					&& !fields[i].contains("INDEX") && fields[i].length() > 10) {
				fields[i] = fields[i].replace("\n  ", "");
				String fieldName = fields[i].substring(0,
						fields[i].indexOf(" "));
				if (fieldName.length() > 3) {
					result += "\t\t\t'" + fieldName + "' "
							+ StringHelper.SCHEMA_ASSIGNMENT + " array(\n";

					if (fields[i].contains("integer")) {
						if (fields[i].contains("AUTO_INCREMENT")) {
							result += "\t\t\t\t'" + StringHelper.SCHEMA_TYPE
									+ "' " + StringHelper.SCHEMA_ASSIGNMENT
									+ " 'serial',\n";
						} else {
							result += "\t\t\t\t'" + StringHelper.SCHEMA_TYPE
									+ "' " + StringHelper.SCHEMA_ASSIGNMENT
									+ " 'integer',\n";
							result += "\t\t\t\t'" + StringHelper.SCHEMA_DEFAULT
									+ "' " + StringHelper.SCHEMA_ASSIGNMENT
									+ " 0,\n";
						}
					} else if (fields[i].contains("varchar")) {
						result += "\t\t\t\t'" + StringHelper.SCHEMA_TYPE + "' "
								+ StringHelper.SCHEMA_ASSIGNMENT
								+ " 'varchar',\n";
						result += "\t\t\t\t'" + StringHelper.SCHEMA_LENGTH
								+ "' " + StringHelper.SCHEMA_ASSIGNMENT
								+ " 255,\n";
						result += "\t\t\t\t'" + StringHelper.SCHEMA_DEFAULT
								+ "' " + StringHelper.SCHEMA_ASSIGNMENT
								+ " '',\n";
					} else if (fields[i].contains("int")) {
						if (fields[i].contains("auto_increment")) {
							result += "\t\t\t\t'" + StringHelper.SCHEMA_TYPE
									+ "' " + StringHelper.SCHEMA_ASSIGNMENT
									+ " 'serial',\n";
							result += "\t\t\t\t'" + StringHelper.SCHEMA_DEFAULT
									+ "' " + StringHelper.SCHEMA_ASSIGNMENT
									+ " 0,\n";
						} else {
							result += "\t\t\t\t'" + StringHelper.SCHEMA_TYPE
									+ "' " + StringHelper.SCHEMA_ASSIGNMENT
									+ " 'int',\n";
						}
					} else if (fields[i].contains("char")) {
						result += "\t\t\t\t'" + StringHelper.SCHEMA_TYPE + "' "
								+ StringHelper.SCHEMA_ASSIGNMENT + " 'char',\n";
						result += "\t\t\t\t'" + StringHelper.SCHEMA_LENGTH
								+ "' " + StringHelper.SCHEMA_ASSIGNMENT
								+ " 255,\n";
						result += "\t\t\t\t'" + StringHelper.SCHEMA_DEFAULT
								+ "' " + StringHelper.SCHEMA_ASSIGNMENT
								+ " '',\n";
					} else if (fields[i].contains("date")) {
						result += "\t\t\t\t'" + StringHelper.SCHEMA_TYPE + "' "
								+ StringHelper.SCHEMA_ASSIGNMENT + " 'date',\n";
					} else if (fields[i].contains("boolean")) {
						result += "\t\t\t\t'" + StringHelper.SCHEMA_TYPE + "' "
								+ StringHelper.SCHEMA_ASSIGNMENT
								+ " 'boolean',\n";
					}
					if (fields[i].contains("NOT NULL")) {
						result += "\t\t\t\t'" + StringHelper.SCHEMA_NOT_NULL
								+ "' " + StringHelper.SCHEMA_ASSIGNMENT
								+ " TRUE,\n";
					}
					if (fields[i].contains("UNSIGNED")) {
						result += "\t\t\t\t'" + StringHelper.SCHEMA_UNSIGNED
								+ "' " + StringHelper.SCHEMA_ASSIGNMENT
								+ " TRUE,\n";
					}
					result = result.substring(0, result.length() - 2);
					result += "\n\t\t\t),\n";
				}
			}

		}
		result += "\t\t),\n";
		return result;
	}

	/**
	 * Gets the primary key.
	 *
	 * @param table the table
	 * @return the primary key
	 */
	private String getPrimaryKey(String table) {
		String result = "";
		String[] fields = table.split(",");
		for (int i = fields.length - 1; i >= 0; i--) {
			if (fields[i].contains(StringHelper.PRIMARY_KEY)) {
				result += "\t\t'" + StringHelper.PRIMARY_KEY.toLowerCase()
						+ "' " + StringHelper.SCHEMA_ASSIGNMENT + " array(\n";
				result += "\t\t\t'"
						+ fields[i].replace("\n", "").replace(")", "")
								.replaceAll("[a-zA-Z_ ]+\\(", "") + "'\n";
				result += "\t\t),\n";
			}

		}

		return result;
	}

	/**
	 * Gets the foreign key.
	 *
	 * @param table the table
	 * @return the foreign key
	 */
	private String getForeignKey(String table) {
		String result = "";
		String[] fields = table.split(",");
		for (int i = fields.length - 1; i >= 0; i--) {
			if (fields[i].contains(StringHelper.FOREIGN_KEY)) {
				String field = fields[i].substring(fields[i].indexOf("("));
				String[] references = field.split("REFERENCES");
				String thisField = references[0].replace("(", "")
						.replace(")", "").replace("`", "").replace("\n", "")
						.replace(" ", "");
				String refTable = references[1]
						.substring(0, references[1].indexOf("("))
						.replace("`", "").replace("\n", "").replace(" ", "");
				String refKey = references[1]
						.substring(references[1].indexOf("(")).replace("(", "")
						.replace(")", "").replace("`", "").replace("\n", "")
						.replace(" ", "");
				result += "\t\t\t'" + thisField + "' "
						+ StringHelper.SCHEMA_ASSIGNMENT + " array(\n";
				result += "\t\t\t\t'" + StringHelper.SCHEMA_TABLE + "' "
						+ StringHelper.SCHEMA_ASSIGNMENT + " '" + refTable
						+ "',\n";
				result += "\t\t\t\t'" + StringHelper.SCHEMA_COLUMNS + "' "
						+ StringHelper.SCHEMA_ASSIGNMENT + " array('"
						+ thisField + "' " + StringHelper.SCHEMA_ASSIGNMENT
						+ " '" + refKey + "'),\n";
				result += "\t\t\t),\n";
			}
		}

		if (result.length() > 0) {
			result += "\t\t),\n";
			result = "\t\t'" + StringHelper.FOREIGN_KEY.toLowerCase() + "' "
					+ StringHelper.SCHEMA_ASSIGNMENT + " array(\n" + result;
		}

		return result;
	}
}
