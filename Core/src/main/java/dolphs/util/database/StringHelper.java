/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.util.database;

// TODO: Auto-generated Javadoc
/**
 * The Class StringHelper.
 */
public class StringHelper {

	/** The Constant SCHEMA_TYPE. */
	protected static final String SCHEMA_TYPE = "type";

	/** The Constant SCHEMA_NOT_NULL. */
	protected static final String SCHEMA_NOT_NULL = "not null";

	/** The Constant SCHEMA_LENGTH. */
	protected static final String SCHEMA_LENGTH = "length";

	/** The Constant SCHEMA_DEFAULT. */
	protected static final String SCHEMA_DEFAULT = "default";

	/** The Constant SCHEMA_FIELDS. */
	protected static final String SCHEMA_FIELDS = "fields";

	/** The Constant SCHEMA_UNSIGNED. */
	protected static final String SCHEMA_UNSIGNED = "unsigned";

	/** The Constant SCHEMA_TABLE. */
	protected static final String SCHEMA_TABLE = "table";

	/** The Constant SCHEMA_COLUMNS. */
	protected static final String SCHEMA_COLUMNS = "columns";

	/** The Constant SCHEMA_ASSIGNMENT. */
	protected static final String SCHEMA_ASSIGNMENT = "=>";

	/** The Constant TABLE_PREFIX. */
	protected static final String TABLE_PREFIX = "UrTracking";

	/** The Constant SCHEMA_FILE_HEADER. */
	protected static final String SCHEMA_FILE_HEADER = "<?php\n\n/*\n* Cria as tabelas necessarias no banco de dados com suas respectivas colunas. \n*/\n\nfunction "
			+ TABLE_PREFIX + "_schema() {\n";

	/** The Constant SCHEMA. */
	protected static final String SCHEMA = "schema";

	/** The Constant PRIMARY_KEY. */
	protected static final String PRIMARY_KEY = "PRIMARY KEY";

	/** The Constant FOREIGN_KEY. */
	protected static final String FOREIGN_KEY = "FOREIGN KEY";

}
