package dolphs.util.imgprocessing;

import dolphs.exceptions.FileNotCreatedException;
import dolphs.io.FileUtils;
import dolphs.io.exceptions.CouldNotWriteException;
import dolphs.io.exceptions.DirectoryNotCreatedException;
import dolphs.io.exceptions.IsDirectoryException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Image {

    private BufferedImage bufferedImage;

    public Image(File file) throws IOException {
        bufferedImage = ImageIO.read(file);
    }

    public static void main(String[] args) throws IOException, DirectoryNotCreatedException, IsDirectoryException, CouldNotWriteException, FileNotCreatedException {
        File file = new File("C:\\Users\\Rodolfo\\Desktop\\Screenshot_2015-05-29-17-59-35-2.jpg");
        Image image = new Image(file);
        String toFile = "";

        Double[][] pixels = image.getBinaryPixels();
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                toFile += pixels[i][j] + " ";
            }
            toFile += "\n";
        }

        File fileD = new File("C:\\Users\\Rodolfo\\Documents\\binary_image.txt");
        FileUtils.write(fileD, toFile);
    }

    public Double[][] getBinaryPixels() {
        int height = bufferedImage.getHeight();
        int width = bufferedImage.getWidth();
        Double[][] result = new Double[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                int red = (rgb >> 16) & 0xFF;
                int green = (rgb >> 8) & 0xFF;
                int blue = (rgb) & 0xFF;
                // System.out.printf("(%d, %d, %d)\t", blue, green, red);
                int soma = red + green + blue;
                Double color;
                if (soma > 382) color = 1d;
                else color = 0d;

                result[i][j] = color;
            }
        }
        return result;
    }

    public int[][] getARGBPixels() {
        int height = bufferedImage.getHeight();
        int width = bufferedImage.getWidth();
        int[][] result = new int[height][width];
        for (int i = 0; i < 314; i++) {
            for (int j = 0; j < 585; j++) {
                result[i][j] = bufferedImage.getRGB(j, i);
            }
        }
        return result;
    }

    public String[][] getRGBPixelsAsString() {
        int height = bufferedImage.getHeight();
        int width = bufferedImage.getWidth();
        String[][] result = new String[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgb = bufferedImage.getRGB(j, i);
                int red = (rgb >> 16) & 0xFF;
                int green = (rgb >> 8) & 0xFF;
                int blue = (rgb) & 0xFF;
                result[i][j] = String.format("(%d, %d, %d)\t", blue, green, red);
            }
        }
        return result;
    }

    private int getWidth() {
        return bufferedImage.getWidth();
    }

    private int getHeight() {
        return bufferedImage.getHeight();
    }
}
