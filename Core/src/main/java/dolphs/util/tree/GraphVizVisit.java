package dolphs.util.tree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import dolphs.util.tree.graphiz.GraphViz;

public class GraphVizVisit implements Visitor<Object, Object> {

    private int production;

    private int leafNumber;

    private int index;

    private String syntax;

    private ArrayList<String> stack;

    public GraphVizVisit() {
        this.syntax = "";
        this.leafNumber = 0;
        this.index = 0;
        this.production = 0;
        this.stack = new ArrayList<String>();
    }

    @Override
    public String visit(Node<Object> node) {
        syntax = getNodeSyntax(node);
        return syntax;
    }

    @Override
    public String visit(Leaf<Object> leaf) {
        syntax = leaf.toString();
        return syntax;
    }

    public void saveToFile(byte GraphVizOS, File file, String type) throws FileNotFoundException {
        GraphViz gv = new GraphViz(GraphVizOS);
        gv.addln(gv.start_graph());
        gv.add(syntax);
        gv.addln(gv.end_graph());
        byte[] img = gv.getGraph(gv.getDotSource(), type);
        if (!file.getName().endsWith("." + type)) {
            file = new File(file.getAbsolutePath() + "." + type);
        }
        gv.writeGraphToFile(img, file);
    }

    private String getNodeSyntax(Node<?> node) {
        String result = "";
        // Cria a sintaxe da Raiz
        String type = node.toString();
        String formater = String.format("\"" + type + production + "_%d\" -- ", index);
        if (stack.contains(formater))
            for (String s : stack) {
                if (s.equals(formater))
                    index++;
            }
        stack.add(formater);

        // Cria a sintaxe de cada filho da Raiz
        for (int i = 0; i < node.getChildren().size(); i++) {
            Tree tree = node.getChildren().get(i);
            // Se for um n�
            if (tree instanceof Node) {
                Node<?> thisNode = (Node<?>) tree;
                result += getNodeSyntax(thisNode);
            } else {
                Leaf<?> leaf = (Leaf<?>) tree;
                stack.add("node" + leafNumber + "\n");
                stack.add("node" + leafNumber + " [label =\"" + leaf.toString() + "\"]\n");
                for (int j = 0; j < stack.size(); j++) {
                    result += stack.get(j);
                }
                leafNumber++;
                production++;
                stack.remove(stack.size() - 1);
            }
            stack.remove(stack.size() - 1);
        }

        if (node.getChildren().size() == 0) {
            stack.add("node" + leafNumber + "\n");
            stack.add("node" + leafNumber + " [label =\"" + node.toString() + "\"]\n");
            for (int j = 0; j < stack.size(); j++) {
                result += stack.get(j);
            }
            leafNumber++;
            production++;
            stack.remove(stack.size() - 1);
            stack.remove(stack.size() - 1);
        }


        return result;
    }

    @Override
    public String toString() {
        return syntax;
    }

    @Override
    public Object visit(Tree tree) {
        if (tree instanceof Node<?>) {
            Node<?> node = (Node<?>) tree;
            return visit(node);
        } else if (tree instanceof Leaf<?>) {
            Leaf<?> leaf = (Leaf<?>) tree;
            return leaf;
        } else {
            throw new ClassCastException();
        }
    }
}
