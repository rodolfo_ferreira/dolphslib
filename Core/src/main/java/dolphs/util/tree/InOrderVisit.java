package dolphs.util.tree;

import dolphs.util.VectorD;

import java.util.List;

public class InOrderVisit implements Visitor<Object, Object> {

    private StringBuilder build;

    public static void main(String[] args) {
        Node<String> node = new Node<String>("*");
        Node<String> node1 = new Node<String>("+");
        Node<String> node2 = new Node<String>("-");
        Node<String> leaf1 = new Node<String>("a");
        Node<String> leaf2 = new Node<String>("b");
        Node<String> leaf3 = new Node<String>("c");
        Node<String> leaf4 = new Node<String>("d");

        node.addChildren(node1);
        node.addChildren(node2);
        node1.addChildren(leaf1);
        node1.addChildren(leaf2);
        node2.addChildren(leaf3);
        node2.addChildren(leaf4);

        Tree tree = node;

        InOrderVisit inOrder = new InOrderVisit();
        tree.acceptVisitor(inOrder);
        System.out.println(inOrder);

    }

    @Override
    public String visit(Node<Object> node) {
        StringBuilder build = new StringBuilder("");
        if (node != null) {
            List<Tree> childrens = node.getChildren();
            if (childrens.size() == 0) {
                build.append(node);
            } else {
                for (int i = 0; i < childrens.size(); i++) {
                    Tree element = node.getChildren().get(i);
                    if (element instanceof Node) {
                        build.append("(");
                        build.append(this.visit((Node<?>) element));
                        build.append(")");
                        if (i < childrens.size() - 1)
                            build.append(node);
                    } else if (element instanceof Leaf) {
                        build.append(this.visit((Leaf<?>) element));
                        if (i < childrens.size() - 1)
                            build.append(node);
                    }
                }
            }
        }
        this.build = build;
        return build.toString();
    }

    @Override
    public String visit(Leaf<Object> leaf) {
        StringBuilder build = new StringBuilder("");
        if (leaf != null) {
            build.append(leaf);
        }
        return build.toString();

    }

    @Override
    public Object visit(Tree tree) {
        if (tree instanceof Node<?>) {
            Node<?> node = (Node<?>) tree;
            return visit(node);
        } else if (tree instanceof Leaf<?>) {
            Leaf<?> leaf = (Leaf<?>) tree;
            return leaf;
        } else {
            throw new ClassCastException();
        }
    }

    @Override
    public String toString() {
        return build.toString();
    }

}
