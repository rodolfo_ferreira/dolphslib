package dolphs.util.tree;


public class Leaf<E> implements Tree {

    private E e;

    public Leaf(E e) {
        super();
        this.e = e;
    }

    @Override
    public String toString() {
        return e.toString();
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public E get() {
        return e;
    }

    public void set(E t) {
        this.e = t;
    }
}
