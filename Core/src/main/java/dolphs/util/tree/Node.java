/*
 *
 */
package dolphs.util.tree;

import java.util.ArrayList;
import java.util.List;

public class Node<T> implements Tree {

    private T t;

    private List<Tree> children;

    public Node() {
        super();
        this.children = new ArrayList<>();
    }

    public Node(T t) {
        super();
        this.t = t;
        this.children = new ArrayList<>();
    }

    public void addChildren(Tree tree) {
        this.children.add(tree);
    }

    public List<Tree> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        String result = null;
        if (t != null)
            result = t.toString();
        return result;
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public T get() {
        return t;
    }

    public void set(T t) {
        this.t = t;
    }
}
