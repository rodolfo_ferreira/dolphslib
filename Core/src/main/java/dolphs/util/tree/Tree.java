package dolphs.util.tree;


public interface Tree {

    void acceptVisitor(Visitor visitor);

    Object get();
}
