package dolphs.util.tree;

public interface Visitor<T, E> {

    E visit(Node<T> node);

    E visit(Leaf<T> leaf);

    E visit(Tree tree);

}
