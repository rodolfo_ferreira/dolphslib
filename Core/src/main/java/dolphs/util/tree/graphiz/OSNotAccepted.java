package dolphs.util.tree.graphiz;

public class OSNotAccepted extends RuntimeException {

	private static final String message = "Check if you set the right Operacional System through the statics attributes WINDOWS_OS and LINUX_OS";

	public OSNotAccepted(byte OS) {
		super(message);
	}

}
