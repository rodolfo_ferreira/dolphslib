/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.web;

import java.io.IOException;
import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class GoogleNews.
 */
public class GoogleNews {

	/** The Constant WEB_URL. */
	private static final String WEB_URL = "https://www.google.com/search?q=";

	/** The Constant URL_ATTRIBUTES. */
	private static final String URL_ATTRIBUTES = "&tbm=nws&start=";

	/** The page. */
	private int page;

	/** The search. */
	private String search;

	/**
	 * Instantiates a new google news.
	 *
	 * @param search the search
	 */
	public GoogleNews(String search) {
		this.search = search;
		this.page = 0;
	}

	/**
	 * Search.
	 *
	 * @return the array list
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ArrayList<String> search() throws IOException {
		ArrayList<String> result = new ArrayList<String>();
		String buildUrl = WEB_URL + search + URL_ATTRIBUTES + page + "0";
		WebPage wp = new WebPage(buildUrl);
		wp.connect();
		String html = wp.getHtml();
		html = html.substring(html.indexOf("id=\"search\">"));
		html = html.substring(0, html.indexOf("id=\"bottomads\""));
		String[] links = html.split("href=\"");

		for (int i = 1; i < links.length; i++) {
			String link = links[i].substring(0, links[i].indexOf("\""));
			if (!result.contains(link) && !link.contains("news.google"))
				result.add(link);
		}

		return result;
	}

	/**
	 * Next page.
	 */
	public void nextPage() {
		this.page++;
	}

}
