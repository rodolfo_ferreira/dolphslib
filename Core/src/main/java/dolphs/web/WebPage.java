/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.jsoup.Jsoup;

import dolphs.util.VectorD;
import dolphs.web.exceptions.TitleNotFound;
import org.jsoup.nodes.Document;

/**
 * The Class WebPage.
 */
public class WebPage {

	/** The url. */
	private URL url;

	/** The impl. */
	private String html;

	/** The web url. */
	private String webUrl;

	/** The connection. */
	private URLConnection connection;

	/**
	 * Instantiates a new web page.
	 *
	 * @param url
	 *            the url
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public WebPage(String url) throws IOException {
		if (url == null)
			throw new NullPointerException();
		this.webUrl = url;
		this.url = new URL(url);
	}

	/**
	 * Connect.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void connect() throws IOException {
		connection = url.openConnection();
		connection.addRequestProperty("User-Agent",
				"Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1");
		connection.setRequestProperty("Accept-Language", "en-US");
		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuilder htmlBuilder = new StringBuilder();
		String line;
		while ((line = br.readLine()) != null) {
		    htmlBuilder.append(line);
		    htmlBuilder.append("\n");
		}
		this.html = htmlBuilder.toString().replaceAll("<", "\n<");
	}

	/**
	 * Gets the impl.
	 *
	 * @return the impl
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String getHtml() throws IOException {
		if (html == null || this.html.isEmpty()) {
			Document document = Jsoup.connect(webUrl).get();
			document.outputSettings(new Document.OutputSettings().prettyPrint(false));
			this.html = document.html();
		}
		return html;
	}

	/**
	 * Gets the content.
	 *
	 * @return the content
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String getContent() throws IOException {
		if (html == null)
			getHtml();
		String result;
		result = html.replaceAll("\\<[^>]*>", "");
		return result;
	}

	/**
	 * Gets the content.
	 *
	 * @param startHtmlString
	 *            the start impl string
	 * @param endHtmlString
	 *            the end impl string
	 * @return the content
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String getContent(String startHtmlString, String endHtmlString) throws IOException {
		String result;
		result = html.substring(html.indexOf(startHtmlString));
		result = result.substring(0, result.indexOf(endHtmlString));
		result = result.replaceAll("\\<[^>]*>", "\n");
		result = result.replace("&#039;", "'");
		result = result.replace("&quot;", "\"");
		result = result.replace("&nbsp;", "");
		result = result.replaceAll("[\n\t]{3,}", "");
		result = result.replaceAll("[ ]{3,}", " ");
		return result;

	}

	/**
	 * Gets the links.
	 *
	 * @return the links
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public VectorD<WebPage> getLinks() throws IOException {
		if (html == null)
			getHtml();
		String normal = webUrl.replaceAll("http[s]*://", "").replaceAll("/[0-9A-z .]+", "").replaceAll("[/]+", "/");
		VectorD<WebPage> result = new VectorD<WebPage>();
		String[] links = html.split("href=\"");
		for (int i = 0; i < links.length; i++) {
			String source = links[i];
			String link = source.substring(0, source.indexOf("\""));
			if (link.startsWith("/")) {
				link = getWebUrl().replaceAll("(http:[\\/]+[A-z\\.])/[0-9A-z\\.\\-\\+\\/]", "$1") + link;
			}
			if (link.contains(normal) && link.contains("://") && !link.endsWith(".jpg") && !link.endsWith(".ico")) {
				try {
					link = link.replaceAll("([0-9A-z\\-]+)[\\/]{2,}", "$1/");
					WebPage wp = new WebPage(link);
					result.addDistinct(wp);
				} catch (MalformedURLException e) {

				}
			}
		}
		return result;
	}

	/**
	 * Gets the page title.
	 *
	 * @return the page title
	 * @throws TitleNotFound
	 *             the title not found
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String getPageTitle() throws TitleNotFound, IOException {
		if (html == null)
			getHtml();
		String result = null;
		try {
			result = html.substring(html.indexOf("<title>"), html.indexOf("</title>")).replaceAll("\\<[^>]*>", "")
					.replaceAll("[\\/:*?\"<>|]+", "");
		} catch (StringIndexOutOfBoundsException e) {
			throw new TitleNotFound();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		WebPage webPage = (WebPage) obj;
		return getWebUrl().equals(webPage.getWebUrl());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return webUrl;
	}

	public String getWebUrl() {
		return webUrl;
	}
}