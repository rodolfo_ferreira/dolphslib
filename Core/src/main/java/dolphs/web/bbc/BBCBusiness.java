/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure 
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */
package dolphs.web.bbc;

import java.io.IOException;

import dolphs.web.WebPage;

// TODO: Auto-generated Javadoc
/**
 * The Class BBCBusiness.
 */
public class BBCBusiness {

	/** The web page. */
	private WebPage webPage;

	/**
	 * Instantiates a new BBC business.
	 *
	 * @param webPage the web page
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public BBCBusiness(WebPage webPage) throws IOException {
		this.webPage = webPage;
		webPage.connect();
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getTitle() throws IOException {
		return webPage.getContent("<h1 class=\"story-body__h1\">", "</h1>");
	}

	/**
	 * Gets the article.
	 *
	 * @return the article
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getArticle() throws IOException {
		return webPage.getContent("</p><p>", "</p>\n    </div>");
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getDescription() throws IOException {
		return webPage.getContent("<p class=\"story-body__introduction\">", "</p>");
	}

}
