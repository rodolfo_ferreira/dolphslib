package dolphs.web.exceptions;

public class TitleNotFound extends Exception {
	
	private final static String message = "Title couldn't be found";
	
	public TitleNotFound() {
		super(message);
	}
	
	public TitleNotFound(String message) {
		super(message);
	}
}
