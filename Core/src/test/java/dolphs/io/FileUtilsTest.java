package dolphs.io;

import dolphs.io.exceptions.FileSystemException;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class FileUtilsTest {

    private final String fileName = "test.txt";
    private final String baseDir = "test" + File.separator;
    private final String directory = "test";
    private final String newFileName = "newName.txt";
    private final String newDirectory = "newDirectory";
    private final String content = "content1";
    private final String content2 = "content2";
    private final Object[] arrayContent = {"Love", 32, 9.7};

    @Test
    public void read() throws FileSystemException {
        File file = new File(baseDir + directory, fileName);
        dolphs.io.FileUtils.write(file, content);
        String content = FileUtils.read(file);
        assertEquals(this.content, content);
    }

    @Test(expected = FileSystemException.class)
    public void readDirectory() throws FileSystemException {
        File file = new File(baseDir + directory);
        file.mkdirs();
        String content = FileUtils.read(file);
        assertEquals(this.content, content);
    }

    @Test
    public void writeString() throws FileSystemException {
        File file = new File(baseDir + directory, fileName);
        FileUtils.write(file, content);
        String content = FileUtils.read(file);
        assertEquals(this.content, content);
    }

    @Test
    public void writeArray() throws FileSystemException {
        File file = new File(baseDir + directory, fileName);
        FileUtils.write(file, arrayContent);
        String content = FileUtils.read(file);
        for (Object object : arrayContent) {
            assertTrue(content.contains(object.toString()));
        }
    }

    @Test(expected = FileSystemException.class)
    public void writeDirectory() throws FileSystemException {
        File file = new File(baseDir + directory);
        file.mkdirs();
        FileUtils.write(file, content);
        String content = FileUtils.read(file);
        assertEquals(this.content, content);
    }

    @Test
    public void rename() throws FileSystemException {
        File file = new File(baseDir + directory, fileName);
        FileUtils.write(file, content);
        assertTrue(file.exists());
        File newFile = FileUtils.rename(file, newFileName);
        assertEquals(newFileName, newFile.getName());
        assertEquals(content, FileUtils.read(newFile));
    }

    @Test
    public void renameDirectory() throws FileSystemException {
        File dir = new File(baseDir + directory);
        dir.mkdirs();
        assertTrue(dir.exists());
        File newDir = FileUtils.rename(dir, baseDir + newDirectory);
        assertTrue(newDir.getAbsolutePath().endsWith(baseDir + newDirectory));
    }

    @Test
    public void moveFileToFile() throws FileSystemException, IOException {
        File file = new File(baseDir + directory, fileName);
        FileUtils.write(file, content + content);
        File newFile = new File(baseDir + directory, newFileName);
        FileUtils.write(newFile, content + content2);

        // File to File
        FileUtils.moveTo(file, newFile, false);
        assertEquals(content + content, FileUtils.read(file));
        assertEquals(content + content2, FileUtils.read(newFile));

        FileUtils.moveTo(file, newFile, true);
        assertFalse(file.exists());
        assertEquals(content + content, FileUtils.read(newFile));
    }

    @Test
    public void moveFileToFolder() throws FileSystemException, IOException {
        File file = new File(baseDir + directory, fileName);
        FileUtils.write(file, content + content);

        File fileNewFolder = new File(baseDir + newDirectory, fileName);
        FileUtils.write(fileNewFolder, content + content2);

        File newFolder = new File(baseDir + newDirectory);

        // File to Folder
        FileUtils.moveTo(file, newFolder, false);
        assertEquals(content + content, FileUtils.read(file));
        assertEquals(content + content2, FileUtils.read(fileNewFolder));

        FileUtils.moveTo(file, newFolder, true);
        assertFalse(file.exists());
        assertEquals(content + content, FileUtils.read(fileNewFolder));
    }

    @Test
    public void moveFolderToFolder() throws FileSystemException, IOException {
        File file = new File(baseDir + directory, fileName);
        FileUtils.write(file, content + content);

        File fileNewFolder = new File(baseDir + newDirectory, fileName);
        FileUtils.write(fileNewFolder, content + content2);

        File folder = new File(baseDir + directory);
        File newFolder = new File(baseDir + newDirectory);

        file = new File(baseDir + newDirectory + File.separator + directory, fileName);

        // Folder to Folder
        FileUtils.moveTo(folder, newFolder, false);
        assertEquals(content + content, FileUtils.read(file));
        assertEquals(content + content2, FileUtils.read(fileNewFolder));

        FileUtils.moveTo(file, newFolder, true);
        assertFalse(file.exists());
        assertEquals(content + content, FileUtils.read(fileNewFolder));
    }


    @After
    public void tearDown() {
        FileUtils.delete(new File("test"));
    }
}