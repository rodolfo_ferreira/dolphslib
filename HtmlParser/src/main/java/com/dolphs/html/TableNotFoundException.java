package com.dolphs.html;

public class TableNotFoundException extends Exception {

    private static final String MESSAGE_FORMAT = "Could not find sibling tables with tag \"%s\" and text \"%s\"";
    private static final String OFFSET_MESSAGE = " and offset \"%d\"";

    public TableNotFoundException(String tag, String title, int offset) {
        super((String.format(MESSAGE_FORMAT, tag, title)) + (offset == 0 ? "" : String.format(OFFSET_MESSAGE, offset)));
    }

    public TableNotFoundException(String tag, int offset) {
        super((String.format(MESSAGE_FORMAT, tag, "")) + (offset == 0 ? "" : String.format(OFFSET_MESSAGE, offset)));
    }
}
