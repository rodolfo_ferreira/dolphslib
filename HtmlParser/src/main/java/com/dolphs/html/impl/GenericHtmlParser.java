package com.dolphs.html.impl;

import com.dolphs.html.TableNotFoundException;
import org.jsoup.nodes.Element;
import sheets.beans.ISpreadSheet;
import sheets.beans.WorkingBook;
import sheets.ioable.Ioable;
import sheets.ioable.MicrosoftSpreadSheet2017;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Abstract Release Note Html parser. This class has the default structure needed to parse the Html release note files.
 */
public class GenericHtmlParser {

    private HTMLParser parser;

    private String fileName;

    public GenericHtmlParser(String content) {
        this.fileName = "";
        this.parser = new HTMLParser(content);
    }

    public GenericHtmlParser(File file, String charset) throws IOException {
        this.fileName = file.getName();
        this.parser = new HTMLParser(file, charset);
    }

    /**
     * This method get the data of the table with the given tag with the given title and the position given by offset.
     *
     * @param tag, the tag name to be searched
     * @param title, the title to be searched inside the elements of the given tag
     * @param offset, the position of the element
     * @return two-dimensional array containing the data of the table
     * @throws TableNotFoundException this exception will be thrown if tables were not found.
     */
    public String[][] getTableDataByTagAndText(String tag, String title, int offset) throws TableNotFoundException {
        Element tableElement = parser.findTableByTagAndText(tag, title, offset);

        if (tableElement == null) {
            throw new TableNotFoundException(tag, title, offset);
        }

        return HTMLParser.parseElementTable(tableElement).getData();
    }

    /**
     * This method get the data of the table with the given tag and the position given by offset.
     *
     * @param tag, the tag name to be searched
     * @param offset, the position of the element
     * @return two-dimensional array containing the data of the table
     * @throws TableNotFoundException this exception will be thrown if tables were not found.
     */
    public String[][] getTableDataByTag(String tag, int offset) throws TableNotFoundException {
        Element tableElement = parser.findTableByTag(tag, offset);

        if (tableElement == null) {
            throw new TableNotFoundException(tag, offset);
        }

        return HTMLParser.parseElementTable(tableElement).getData();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
