package com.dolphs.html.impl;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HTMLParser {

    private Document document;

    public HTMLParser(String content) {
        this.document = Jsoup.parse(content);
    }

    public HTMLParser(File file, String charset) throws IOException {
        this.document = Jsoup.parse(file, charset);
    }

    /**
     * This method parses the Html Element table and returns a {@link HtmlTable} containing the title (the Caption tag content)
     * and the data (The elements in the table) represented in a 2-dimensional array.
     *
     * @param table, the element that contains the impl table tag
     * @return {@link HtmlTable} containing the content of the table
     */
    public static HtmlTable parseElementTable(Element table) {
        HtmlTable htmlTable;
        if (table != null && !table.getElementsByTag("table").isEmpty()) {

            List<Elements> rowElements = new ArrayList<>();

            Element titleElement = table.getElementsByTag("caption").first();
            Element headElement = table.getElementsByTag("thead").first();
            Element bodyElement = table.getElementsByTag("tbody").first();

            if (headElement != null) {
                rowElements.add(headElement.children());
            }
            if (bodyElement != null) {
                rowElements.add(bodyElement.children());
            }

            htmlTable = initTable(rowElements);
            setTitle(htmlTable, titleElement);
            addRowsOfElements(htmlTable, rowElements);
        } else {
            htmlTable = new HtmlTable();
            htmlTable.init(0, 0);
        }

        return htmlTable;
    }

    /**
     * This method gets each column of the given table row element and adds it to a String array.
     *
     * @param tableRow, the table row element containing the columns
     * @return an array containing the table row values
     */
    private static String[] getRowData(Element tableRow) {
        Elements cols = tableRow.children();

        String[] data = new String[cols.size()];
        for (int i = 0; i < cols.size(); i++) {
            Element col = cols.get(i);
            data[i] = col.text();
        }

        return data;
    }

    /**
     * Checks if the title element exists and, if so, sets the {@link HtmlTable#title} with the element text.
     *
     * @param htmlTable, the table to have its title set
     * @param titleElement, the element that contains the title text (caption tag usually)
     */
    private static void setTitle(HtmlTable htmlTable, Element titleElement) {
        if (titleElement != null) {
            htmlTable.setTitle(titleElement.text());
        }
    }

    /**
     * This method initiates the given HTMLTable, it will iterate over the table content elements (thead and tbody tag usually)
     * finding the number of rows and columns and calls {@link HtmlTable#init(int, int)} with these values
     *
     * @param tableContentElements, the elements containing the rows and cols (thead and tbody tag usually).
     */
    private static HtmlTable initTable(List<Elements> tableContentElements) {
        HtmlTable htmlTable = new HtmlTable();
        int rows = 0;
        int cols = 0;

        for (Elements elements : tableContentElements) {
            rows += elements.size();

            Element firstRow = elements.first();
            if (firstRow != null && firstRow.children().size() > cols) {
                cols = firstRow.children().size();
            }
        }

        htmlTable.init(rows, cols);
        return htmlTable;
    }

    /**
     * This method iterates over the Table content elements (tbody and thead tags usually) and then for each row it gets the
     * columns data as an array and adds it to the given {@link HtmlTable}.
     *
     * @param htmlTable, the table to have the data added to
     * @param tableContentElements, the table content (tbody and thead tags usually)
     */
    private static void addRowsOfElements(HtmlTable htmlTable, List<Elements> tableContentElements) {
        for (Elements elements : tableContentElements) {
            for (Element row : elements) {
                String[] data = getRowData(row);
                htmlTable.addData(data);
            }
        }
    }

    /**
     * This method gets all elements by given tag in the {@link #document}, then iterates over it find the one that contains
     * the given text (include children text) in the position given by offset.
     *
     * @param tag, the impl tag name
     * @param text, the text containing in the element (includes children text)
     * @param offset, the position of the element found
     * @return the element found.
     */
    public Element findTableByTagAndText(String tag, String text, int offset) {
        Element result = null;
        Elements elements = document.getElementsByTag(tag);
        Iterator<Element> elementIterator = elements.iterator();

        while (result == null && elementIterator.hasNext()) {
            Element element = elementIterator.next();

            if (element.text().contains(text)) {
                Element parent = element.parent();
                Elements tableElements = parent.getElementsByTag("table");
                if (!tableElements.isEmpty()) {
                    result = tableElements.get(offset);
                }
            }
        }
        return result;
    }

    /**
     * This method gets all elements by given tag in the {@link #document}, then iterates over it find the one that contains
     * the given text (include children text) in the position given by offset.
     *
     * @param tag, the impl tag name
     * @param text, the text containing in the element (includes children text)
     * @param offset, the position of the element found
     * @return the element found.
     */
    public Element findTableByTag(String tag, int offset) {
        Element result = null;
        Elements elements = document.getElementsByTag(tag);

        if (!elements.isEmpty()) {
            result = elements.get(offset);
        }
        return result;
    }
}
