package com.dolphs.html.impl;

/**
 * This class represents a Html table, it will wrap the caption tag in {@link #title} field and tbody and thead data in
 * {@link #data} field
 */
public class HtmlTable {

    private String title;

    private String[][] data;

    private int rowCount;

    public HtmlTable() {
        rowCount = 0;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String[][] getData() {
        return data;
    }

    public void init(int rows, int cols) {
        this.data = new String[rows][cols];
    }

    public void addData(String[] data) {
        System.arraycopy(data, 0, this.data[rowCount++], 0, data.length);
    }
}
