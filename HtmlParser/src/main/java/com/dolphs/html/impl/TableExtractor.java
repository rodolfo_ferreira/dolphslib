package com.dolphs.html.impl;

import java.util.ArrayList;
import java.util.List;

public abstract class TableExtractor<T> {

    private Class<T> clazz;

    public TableExtractor(Class<T> clazz) {
        this.clazz = clazz;
    }

    public List<T> convert(String[][] table) throws IllegalAccessException, InstantiationException {
        List<T> result = new ArrayList<>();
        for (int i = 1; i< table.length; i++) {
            T object = clazz.newInstance();
            for (int j = 0; j < table[i].length; j++) {
                readByTitle(table[0][j], object, table[i][j]);
            }
            result.add(object);
        }
        return result;
    }

    public abstract void  readByTitle(String s, T object, String s1);
}
