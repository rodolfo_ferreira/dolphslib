package com.dolphs.html;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ApkReleaseNoteHtmlTest {

    private List<ApkReleaseNote> releaseNotesFromString;

    private List<ApkReleaseNote> releaseNotesFromFile;

    private static final String CR_REGEX = "[A-Za-z0-9]+-[0-9]+";

    @Before
    public void setUp() throws Exception {
        String path =
                System.getProperty("user.home") + File.separator + "Documents" + File.separator + "releaseNoteFiles" + File.separator + "apk";
        releaseNotesFromFile = new ArrayList<>();
        releaseNotesFromString = new ArrayList<>();

        File[] files = new File(path).listFiles();
        for (File file : files) {
            releaseNotesFromFile.add(ApkReleaseNoteHtml.parse(file, StandardCharsets.UTF_8.name()));
            releaseNotesFromString.add(ApkReleaseNoteHtml.parse(TestUtils.readFile(file)));
        }
    }

    @Test
    public void getChangeHistory() {
        for (int i = 0; i < releaseNotesFromFile.size(); i++) {
            ApkReleaseNote releaseNoteFromFile = releaseNotesFromFile.get(i);
            ApkReleaseNote releaseNoteFromString = releaseNotesFromString.get(i);

            List<ChangeHistory> fromFile = releaseNoteFromFile.getChangeHistory();
            List<ChangeHistory> fromString = releaseNoteFromString.getChangeHistory();

            Assert.assertEquals(fromFile, fromString);

            for(ChangeHistory changeHistory : fromFile) {
                Assert.assertNotNull(changeHistory.getProject());
                Assert.assertNotNull(changeHistory.getOldCommit());
                Assert.assertNotNull(changeHistory.getNewCommit());
                Assert.assertNotNull(changeHistory.getGerritId());
                Assert.assertNotNull(changeHistory.getCommit());
                Assert.assertNotNull(changeHistory.getAuthor());
                Assert.assertNotNull(changeHistory.getDate());
                Assert.assertNotNull(changeHistory.getComment());
                Assert.assertNotNull(changeHistory.getSubDir());
                Assert.assertNotNull(changeHistory.getCrs());
                for (String cr : changeHistory.getCrs()) {
                    Assert.assertTrue(cr.matches(CR_REGEX));
                }

            }
        }
    }

    @Test
    public void getApkInfo() {
        for (int i = 0; i < releaseNotesFromFile.size(); i++) {
            ApkReleaseNote releaseNoteFromFile = releaseNotesFromFile.get(i);
            ApkReleaseNote releaseNoteFromString = releaseNotesFromString.get(i);

            List<APKInfo> fileAPKInfos = releaseNoteFromFile.getApkInfo();
            List<APKInfo> stringAPKInfos = releaseNoteFromString.getApkInfo();

            Assert.assertEquals(fileAPKInfos, stringAPKInfos);
        }
    }
}
