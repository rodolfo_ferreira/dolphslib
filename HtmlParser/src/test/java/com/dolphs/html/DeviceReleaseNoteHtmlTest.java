package com.dolphs.html;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class DeviceReleaseNoteHtmlTest {

    private List<DeviceReleaseNote> releaseNotesFromString;

    private List<DeviceReleaseNote> releaseNotesFromFile;

    private static final String CR_REGEX = "[A-Za-z0-9]+-[0-9]+";

    @Before
    public void setUp() throws Exception {
        String path =
                System.getProperty("user.home") + File.separator + "Documents" + File.separator + "releaseNoteFiles"  + File.separator + "device";
        releaseNotesFromString = new ArrayList<>();
        releaseNotesFromFile = new ArrayList<>();

        File[] files = new File(path).listFiles();
        for (File file : files) {
            releaseNotesFromFile.add(DeviceReleaseNoteHtml.parse(file, StandardCharsets.UTF_8.name()));
            releaseNotesFromString.add(DeviceReleaseNoteHtml.parse(TestUtils.readFile(file)));
        }
    }

    @Test
    public void getChangeCommits() {
        for (int i = 0; i < releaseNotesFromFile.size(); i++) {
            DeviceReleaseNote releaseNoteFromFile = releaseNotesFromFile.get(i);
            DeviceReleaseNote releaseNoteFromString = releaseNotesFromString.get(i);

            List<ChangeCommit> fromFile = releaseNoteFromFile.getChangeCommits();
            List<ChangeCommit> fromString = releaseNoteFromString.getChangeCommits();

            Assert.assertEquals(fromFile, fromString);

            for (ChangeCommit changeCommit : fromFile) {
                Assert.assertNotNull(changeCommit.getRepoPath());
                Assert.assertNotNull(changeCommit.getShorCommitSHA1());
                Assert.assertNotNull(changeCommit.getGerritId());
                Assert.assertNotNull(changeCommit.getCommitMessage());
                Assert.assertNotNull(changeCommit.getCrs());
                for (String cr : changeCommit.getCrs()) {
                    Assert.assertTrue(cr.matches(CR_REGEX));
                }
                Assert.assertNotNull(changeCommit.getCommiter());
                Assert.assertNotNull(changeCommit.getAuthor());
            }
        }
    }
}
