package com.dolphs.html;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class TestUtils {

    public static String readFile(File file) throws IOException {
        String content = "";
        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {
            StringBuilder builder = new StringBuilder();
            while (br.ready()) {
                builder.append(br.readLine());
                builder.append("\n");
            }
            content = builder.toString();
        }
        return content;
    }
}