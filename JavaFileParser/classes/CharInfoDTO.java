package com.dolphs.wow;

import com.google.gson.annotations.SerializedName;
import com.dolphs.wow.ProgressionDTO;

public class CharInfoDTO {

    @SerializedName("totalHonorableKills")
    private int totalHonorableKills;

    @SerializedName("battlegroup")
    private String battlegroup;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("race")
    private int race;

    @SerializedName("gender")
    private int gender;

    @SerializedName("level")
    private int level;

    @SerializedName("progression")
    private ProgressionDTO progression;

    @SerializedName("class")
    private int cls;

    @SerializedName("achievementPoints")
    private int achievementPoints;

    @SerializedName("faction")
    private int faction;

    @SerializedName("name")
    private String name;

    @SerializedName("realm")
    private String realm;

    @SerializedName("lastModified")
    private int lastModified;

    @SerializedName("calcClass")
    private String calcClass;

    @SerializedName("quests")
    private List<Integer> quests;

    @SerializedName("guild")
    private GuildDTO guild;

    @SerializedName("audit")
    private AuditDTO audit;

    @SerializedName("hunterPets")
    private List<HunterPetsDTO> hunterPets;

    @SerializedName("items")
    private ItemsDTO items;

    @SerializedName("pvp")
    private PvpDTO pvp;

    @SerializedName("appearance")
    private AppearanceDTO appearance;

    @SerializedName("mounts")
    private MountsDTO mounts;

    @SerializedName("professions")
    private ProfessionsDTO professions;

    @SerializedName("petSlots")
    private List<PetSlotsDTO> petSlots;

    @SerializedName("pets")
    private PetsDTO pets;

    @SerializedName("titles")
    private List<TitlesDTO> titles;

    @SerializedName("reputation")
    private List<ReputationDTO> reputation;

    @SerializedName("talents")
    private List<TalentsDTO> talents;

    @SerializedName("achievements")
    private AchievementsDTO achievements;

    @SerializedName("stats")
    private StatsDTO stats;

    @SerializedName("feed")
    private List<FeedDTO> feed;

    @SerializedName("statistics")
    private StatisticsDTO statistics;

    public CharInfoDTO() {
        
    }

    public int getTotalHonorableKills() {
        return totalHonorableKills;

    }

    public void setTotalHonorableKills(int totalHonorableKills) {
        this.totalHonorableKills = totalHonorableKills;

    }

    public String getBattlegroup() {
        return battlegroup;

    }

    public void setBattlegroup(String battlegroup) {
        this.battlegroup = battlegroup;

    }

    public String getThumbnail() {
        return thumbnail;

    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;

    }

    public int getRace() {
        return race;

    }

    public void setRace(int race) {
        this.race = race;

    }

    public int getGender() {
        return gender;

    }

    public void setGender(int gender) {
        this.gender = gender;

    }

    public int getLevel() {
        return level;

    }

    public void setLevel(int level) {
        this.level = level;

    }

    public ProgressionDTO getProgression() {
        return progression;

    }

    public void setProgression(ProgressionDTO progression) {
        this.progression = progression;

    }

    public int getCls() {
        return cls;

    }

    public void setCls(int cls) {
        this.cls = cls;

    }

    public int getAchievementPoints() {
        return achievementPoints;

    }

    public void setAchievementPoints(int achievementPoints) {
        this.achievementPoints = achievementPoints;

    }

    public int getFaction() {
        return faction;

    }

    public void setFaction(int faction) {
        this.faction = faction;

    }

    public String getName() {
        return name;

    }

    public void setName(String name) {
        this.name = name;

    }

    public String getRealm() {
        return realm;

    }

    public void setRealm(String realm) {
        this.realm = realm;

    }

    public int getLastModified() {
        return lastModified;

    }

    public void setLastModified(int lastModified) {
        this.lastModified = lastModified;

    }

    public String getCalcClass() {
        return calcClass;

    }

    public void setCalcClass(String calcClass) {
        this.calcClass = calcClass;

    }

    public List<Integer> getQuests() {
        return quests;

    }

    public void setQuests(List<Integer> quests) {
        this.quests = quests;

    }

    public GuildDTO getGuild() {
        return guild;

    }

    public void setGuild(GuildDTO guild) {
        this.guild = guild;

    }

    public AuditDTO getAudit() {
        return audit;

    }

    public void setAudit(AuditDTO audit) {
        this.audit = audit;

    }

    public List<HunterPetsDTO> getHunterPets() {
        return hunterPets;

    }

    public void setHunterPets(List<HunterPetsDTO> hunterPets) {
        this.hunterPets = hunterPets;

    }

    public ItemsDTO getItems() {
        return items;

    }

    public void setItems(ItemsDTO items) {
        this.items = items;

    }

    public PvpDTO getPvp() {
        return pvp;

    }

    public void setPvp(PvpDTO pvp) {
        this.pvp = pvp;

    }

    public AppearanceDTO getAppearance() {
        return appearance;

    }

    public void setAppearance(AppearanceDTO appearance) {
        this.appearance = appearance;

    }

    public MountsDTO getMounts() {
        return mounts;

    }

    public void setMounts(MountsDTO mounts) {
        this.mounts = mounts;

    }

    public ProfessionsDTO getProfessions() {
        return professions;

    }

    public void setProfessions(ProfessionsDTO professions) {
        this.professions = professions;

    }

    public List<PetSlotsDTO> getPetSlots() {
        return petSlots;

    }

    public void setPetSlots(List<PetSlotsDTO> petSlots) {
        this.petSlots = petSlots;

    }

    public PetsDTO getPets() {
        return pets;

    }

    public void setPets(PetsDTO pets) {
        this.pets = pets;

    }

    public List<TitlesDTO> getTitles() {
        return titles;

    }

    public void setTitles(List<TitlesDTO> titles) {
        this.titles = titles;

    }

    public List<ReputationDTO> getReputation() {
        return reputation;

    }

    public void setReputation(List<ReputationDTO> reputation) {
        this.reputation = reputation;

    }

    public List<TalentsDTO> getTalents() {
        return talents;

    }

    public void setTalents(List<TalentsDTO> talents) {
        this.talents = talents;

    }

    public AchievementsDTO getAchievements() {
        return achievements;

    }

    public void setAchievements(AchievementsDTO achievements) {
        this.achievements = achievements;

    }

    public StatsDTO getStats() {
        return stats;

    }

    public void setStats(StatsDTO stats) {
        this.stats = stats;

    }

    public List<FeedDTO> getFeed() {
        return feed;

    }

    public void setFeed(List<FeedDTO> feed) {
        this.feed = feed;

    }

    public StatisticsDTO getStatistics() {
        return statistics;

    }

    public void setStatistics(StatisticsDTO statistics) {
        this.statistics = statistics;

    }
}
