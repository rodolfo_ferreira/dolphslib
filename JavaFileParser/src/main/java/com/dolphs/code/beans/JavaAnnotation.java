package com.dolphs.code.beans;

import java.util.Objects;

public class JavaAnnotation {

    private String attribute;

    private String value;

    public JavaAnnotation(String method, String value) {
        this.attribute = method;
        this.value = value;
    }

    public String getAttribute() {
        return attribute;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        String formatter = "    @%s(\"%s\")";
        return String.format(formatter, attribute, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        JavaAnnotation that = (JavaAnnotation) o;
        return Objects.equals(attribute, that.attribute) && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(attribute, value);
    }
}
