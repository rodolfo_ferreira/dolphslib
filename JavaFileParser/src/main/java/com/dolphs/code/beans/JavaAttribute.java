package com.dolphs.code.beans;

import com.dolphs.code.utils.AccessorType;
import com.dolphs.code.utils.Constants;

import java.util.Objects;

public class JavaAttribute {

    private JavaAnnotation javaAnnotation;

    private AccessorType accessor;

    private boolean isStatic;

    private boolean isFinal;

    private String type;

    private String name;

    public JavaAttribute(String type, String name) {
        this.accessor = AccessorType.PACKAGE;
        this.isStatic = false;
        this.isFinal = false;
        this.type = type;
        this.name = name;
    }

    public JavaAttribute(AccessorType accessor, String type, String name) {
        this.accessor = accessor;
        this.isStatic = false;
        this.isFinal = false;
        this.type = type;
        this.name = name;
    }

    public JavaAttribute(AccessorType accessor, boolean isStatic, boolean isFinal, String type, String name) {
        this.accessor = accessor;
        this.isStatic = isStatic;
        this.isFinal = isFinal;
        this.type = type;
        this.name = name;
    }

    public AccessorType getAccessor() {
        return accessor;
    }

    public void setAccessor(AccessorType accessor) {
        this.accessor = accessor;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JavaAnnotation getJavaAnnotation() {
        return javaAnnotation;
    }

    public void setJavaAnnotation(JavaAnnotation javaAnnotation) {
        this.javaAnnotation = javaAnnotation;
    }

    @Override
    public String toString() {
        String formatter = "%s\n    %s %s%s%s %s;";
        return String.format(formatter, javaAnnotation.toString(), accessor.toString(), isStatic ? Constants
                        .STATIC_CONSTANT + " " : "", isFinal ? Constants.FINAL_CONSTANT + " " : "", type, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        JavaAttribute that = (JavaAttribute) o;
        return isStatic == that.isStatic && isFinal == that.isFinal && Objects.equals(javaAnnotation, that
                .javaAnnotation) && accessor == that.accessor && Objects.equals(type, that.type) && Objects.equals
                (name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(javaAnnotation, accessor, isStatic, isFinal, type, name);
    }
}
