package com.dolphs.code.beans;

import com.dolphs.code.utils.AccessorType;
import dolphs.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class JavaClass {

    private JavaPackage packag;

    private List<JavaImport> imports;

    private AccessorType accessorType;

    private String name;

    private List<JavaAttribute> attributes;

    private JavaConstructor constructor;

    private List<JavaMethod> methods;

    public JavaClass() {
        this.packag = new JavaPackage();
        this.imports = new ArrayList<>();
        this.accessorType = AccessorType.PACKAGE;
        this.name = "";
        this.attributes = new ArrayList<>();
        this.constructor = new JavaConstructor(name);
        this.methods = new ArrayList<>();
        this.accessorType = AccessorType.PACKAGE;
    }

    public JavaClass(JavaPackage packag, AccessorType accessorType, String name) {
        this.packag = packag;
        this.imports = new ArrayList<>();
        this.accessorType = accessorType;
        this.name = name;
        this.attributes = new ArrayList<>();
        this.constructor = new JavaConstructor(name);
        this.methods = new ArrayList<>();
    }

    public JavaPackage getPackag() {
        return packag;
    }

    public void setPackag(JavaPackage packag) {
        this.packag = packag;
    }

    public List<JavaImport> getImports() {
        return imports;
    }

    public void setImports(List<JavaImport> imports) {
        this.imports = imports;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.constructor.setName(name);
    }

    public List<JavaAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<JavaAttribute> attributes) {
        this.attributes = attributes;
    }

    public List<JavaMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<JavaMethod> methods) {
        this.methods = methods;
    }

    public void addAttribute(JavaAttribute javaAttribute) {
        this.attributes.add(javaAttribute);
    }

    public void addMethod(JavaMethod javaMethod) {
        this.methods.add(javaMethod);
    }

    public void addImport(JavaImport javaImport) {
        this.imports.add(javaImport);
    }

    @Override
    public String toString() {
        String formatter = "%s\n\n%s\n\n%s class %s {\n\n%s\n\n%s\n\n%s\n}";
        return String.format(formatter, packag.toString(), StringUtils.asString(imports, "\n"),
                accessorType.toString(), name, StringUtils.asString(attributes, "\n\n"), constructor,
                StringUtils.asString(methods, "\n\n"));
    }

    public void setAccessorType(AccessorType accessorType) {
        this.accessorType = accessorType;
    }
}
