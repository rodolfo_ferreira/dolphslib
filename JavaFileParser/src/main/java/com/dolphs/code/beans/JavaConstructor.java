package com.dolphs.code.beans;


import com.dolphs.code.utils.AccessorType;
import dolphs.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class JavaConstructor {

    private AccessorType accessor;

    private String name;

    private Map<String, String> attributes;

    private String code;

    public JavaConstructor(String className) {
        this.accessor = AccessorType.PUBLIC;
        this.name = className;
        this.attributes = new HashMap<>();
        this.code = "";
    }

    public AccessorType getAccessor() {
        return accessor;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        String formatter = "    %s %s(%s) {\n        %s\n    }";
        return String.format(formatter, accessor.toString(), name, StringUtils.asString(attributes, true), code);
    }

    void setName(String name) {
        this.name = name;
    }
}
