package com.dolphs.code.beans;

public class JavaImport {

    private String packag;

    private String cls;

    public JavaImport(String packag, String cls) {
        this.packag = packag;
        this.cls = cls;
    }

    public String getPackag() {
        return packag;
    }

    public void setPackag(String packag) {
        this.packag = packag;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    @Override
    public String toString() {
        String formatter = "import %s.%s;";
        return String.format(formatter, packag, cls);
    }
}
