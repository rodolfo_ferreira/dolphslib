package com.dolphs.code.beans;

import com.dolphs.code.utils.AccessorType;
import com.dolphs.code.utils.Constants;
import dolphs.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class JavaMethod {

    private AccessorType accessor;

    private boolean isStatic;

    private String returnType;

    private String name;

    private Map<String, String> params;

    private String code;

    public JavaMethod(AccessorType accessor, String returnType, String name, Map<String, String> params, String code) {
        this.accessor = accessor;
        this.isStatic = false;
        this.returnType = returnType;
        this.name = name;
        this.params = params;
        this.code = code;
    }

    public JavaMethod(AccessorType accessor, String returnType, String name, Map<String, String> params) {
        this.accessor = accessor;
        this.isStatic = false;
        this.returnType = returnType;
        this.name = name;
        this.params = params;
        this.code = "";
    }

    public JavaMethod(String returnType, String name, Map<String, String> params) {
        this.accessor = AccessorType.PACKAGE;
        this.isStatic = false;
        this.returnType = returnType;
        this.name = name;
        this.params = params;
        this.code = "";
    }

    public JavaMethod(AccessorType accessor, String returnType, String name) {
        this.accessor = accessor;
        this.isStatic = false;
        this.returnType = returnType;
        this.name = name;
        this.params = new HashMap<>();
        this.code = "";
    }

    public JavaMethod(AccessorType accessor, String returnType, String name, String code) {
        this.accessor = accessor;
        this.isStatic = false;
        this.returnType = returnType;
        this.name = name;
        this.params = new HashMap<>();
        this.code = code;
    }

    public JavaMethod(String returnType, String name) {
        this.accessor = AccessorType.PACKAGE;
        this.isStatic = false;
        this.returnType = returnType;
        this.name = name;
        this.params = new HashMap<>();
        this.code = "";
    }

    public AccessorType getAccessor() {
        return accessor;
    }

    public void setAccessor(AccessorType accessor) {
        this.accessor = accessor;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        String formatter = "    %s %s%s %s(%s) {\n        %s\n    }";
        return String.format(formatter, accessor.toString(), isStatic ? Constants.STATIC_CONSTANT + " " : "",
                returnType, name, StringUtils.asString(params, true), code);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        JavaMethod that = (JavaMethod) o;
        return isStatic == that.isStatic && accessor == that.accessor && Objects.equals(returnType, that.returnType)
                && Objects.equals(name, that.name) && Objects.equals(params, that.params) && Objects.equals(code,
                that.code);
    }

    @Override
    public int hashCode() {

        return Objects.hash(accessor, isStatic, returnType, name, params, code);
    }
}
