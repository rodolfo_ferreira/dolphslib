package com.dolphs.code.beans;

public class JavaPackage {

    private String packag;

    public JavaPackage(String packag) {
        this.packag = packag;
    }

    public JavaPackage() {
        this.packag = "";
    }

    public String getPackag() {
        return packag;
    }

    public void setPackag(String packag) {
        this.packag = packag;
    }

    @Override
    public String toString() {
        String formatter = "package %s;";
        return String.format(formatter, packag);
    }
}
