package com.dolphs.code.business;

import com.dolphs.code.beans.JavaClass;
import com.dolphs.code.compiler.lexer.TokenException;
import com.dolphs.code.compiler.parser.JavaFileVisitor;
import com.dolphs.code.compiler.parser.JavaParser;
import com.dolphs.code.compiler.parser.ParserException;
import com.dolphs.code.repository.IRepository;
import com.dolphs.code.repository.Repository;
import dolphs.exceptions.CouldNotReadException;
import dolphs.io.FileUtils;
import dolphs.util.tree.Tree;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class JavaClassBO {

    IRepository repository;

    public JavaClassBO() {
        repository = Repository.getInstance();
    }

    public void createJavaClass(JavaClass javaClass) {
        repository.create(javaClass);
    }

    public List<JavaClass> getAll() {
        return repository.getAll();

    }

    public JavaClass parse(File file) throws CouldNotReadException, TokenException, ParserException {
        String content = FileUtils.read(file);
        return parse(content);
    }

    public JavaClass mergeFile(File file1, File file2) throws CouldNotReadException, ParserException, TokenException {
        JavaClass javaClass = parse(file1);
        JavaClass javaClass1 = parse(file2);
        return JavaClassUtils.merge(javaClass, javaClass1);
    }

    public JavaClass merge(JavaClass javaClass, JavaClass javaClass1) {
        return JavaClassUtils.merge(javaClass, javaClass1);
    }

    private JavaClass parse(String string) throws TokenException, ParserException {
        InputStream inputStream = new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8));
        return parse(inputStream);
    }

    private JavaClass parse(InputStream inputStream) throws TokenException, ParserException {
        JavaParser parser = new JavaParser();
        Tree tree = parser.parse(inputStream);
        JavaFileVisitor visitor = new JavaFileVisitor();
        tree.acceptVisitor(visitor);
        return visitor.getJavaClass();
    }
}
