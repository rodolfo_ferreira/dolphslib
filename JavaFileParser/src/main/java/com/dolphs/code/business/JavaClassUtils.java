package com.dolphs.code.business;

import com.dolphs.code.beans.JavaAttribute;
import com.dolphs.code.beans.JavaClass;
import com.dolphs.code.beans.JavaMethod;

import java.util.ArrayList;
import java.util.List;

public class JavaClassUtils {

    public static JavaClass merge(JavaClass javaClass1, JavaClass javaClass2) {
        // TODO this only works for methods and attributes, change it to merge all.
        List<JavaAttribute> attributeList = mergeAttributes(javaClass1.getAttributes(), javaClass2.getAttributes());
        List<JavaMethod> methodList = mergeMethods(javaClass1.getMethods(), javaClass2.getMethods());

        javaClass1.setAttributes(attributeList);
        javaClass1.setMethods(methodList);

        return javaClass1;
    }

    private static List<JavaAttribute> mergeAttributes(List<JavaAttribute> attributes, List<JavaAttribute>
            attributes1) {
        List<JavaAttribute> result = new ArrayList<>();
        result.addAll(attributes);

        attributes1.forEach(attribute -> {
            if (!result.contains(attribute))
                result.add(attribute);
        });
        return result;
    }

    private static List<JavaMethod> mergeMethods(List<JavaMethod> methods, List<JavaMethod> methods1) {
        List<JavaMethod> result = new ArrayList<>();
        result.addAll(methods);

        methods1.forEach(method -> {
            if (!result.contains(method))
                result.add(method);
        });
        return result;
    }
}
