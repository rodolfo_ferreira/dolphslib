/* Copyright (c) 2015, Rodolfo Ferreira. All rights reserved. *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is used as a support library, it was created in my leisure
 * to avoid code repetition in other projects and abstract some concepts
 * even  more.
 *
 * Please contact me, rodolfoandreferreira@gmail.com
 * if you need additional information or have any questions.
 */

package com.dolphs.code.compiler.lexer;

import java.io.IOException;
import java.io.InputStream;


/**
 * This class is responsible to lexical analysis, which consists in a process of
 * converting a sequence of characters into a sequence of tokens.
 *
 * @author Rodolfo Ferreira and Juliana Ferreira.
 *
 */
public class Lexer {

    /** The input stream which contains the content to be generated as token. */
    private InputStream inputStream;

    /** The next char to be analysed as token. */
    private int nextChar;

    /** The index of the string line which is being processed. */
    private int index;

    /** The line of the code which is being processed. */
    private int line;

    /**
     * This method resets the inputstream to prepare the analysis.
     *
     * @param inputStream
     *            the input stream to be analyzed
     * @throws TokenException
     *             the token exception if some IOException is caught.
     */
    public void reset(InputStream inputStream) throws TokenException {
        line = 1;
        index = 1;
        try {
            // cont�m o codigo fonte a ser analisado
            this.inputStream = inputStream;
            // anda para o primeiro byte da entrada
            this.nextChar = inputStream.read();

        } catch (IOException e) {
            throw new TokenException(e.getMessage());
        }
    }

    /**
     * Read the next byte from the inputstream to be converted as a char later
     * to our analysis.
     *
     * @return the int which contains the byte of the char
     * @throws TokenException
     *             the token exception if some IOException is caught.
     */
    private int readByte() throws TokenException {
        index++;
        int theByte = -1;

        try {
            theByte = inputStream.read();
        } catch (IOException e) {
            throw new TokenException(e.getMessage());
        }

        return theByte;
    }

    /**
     * This method find the token to the correspondent char. It uses the concept
     * of State Machines to construct Regular Expressions
     *
     * @return The object Token, which corresponds as the token of the given
     *         char
     * @throws TokenException
     *             the token exception with its respective error message
     */
    public Token nextToken() throws TokenException {
        Token result = null;
        TokenType tipo = null;
        StringBuilder lexema = new StringBuilder("");

        skipBlankChars();

        // Testa se chegou ao fim do arquivo
        if (isEOF(nextChar)) {
            return new Token(TokenType.EOF);
        }

        else if (isNumber(this.nextChar) || isLetter(this.nextChar)) {
            result = getIdentifierOrPrimitiveToken();
        }

        else if (nextChar == '/') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();

            if (nextChar == '/' || nextChar == '*') {
                result = getCommentToken(lexema);
            } else {
                result = new Token(TokenType.SLASH, lexema.toString());
            }

        }

        else if (nextChar == '(') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.PARENTHESIS_OPEN;
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == ')') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.PARENTHESIS_CLOSE;
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == ';') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.SEMI_COLON;
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == ':') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.COLON;
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == '{') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.BRACES_OPEN;
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == '}') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.BRACES_CLOSE;
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == '.') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.DOT;
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == '@') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.ANNOTATION_MARK;
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == '\'') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = getSimpleCharToken(lexema);

        }

        else if (nextChar == '"') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = getStringToken(lexema);

        }

        else if (nextChar == '=') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = new Token(TokenType.ASSERTION, lexema.toString());

        }

        else if (nextChar == '<' || nextChar == '>') {
            result = getLessOrGreater(lexema);
        }

        else if (nextChar == ',') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = new Token(TokenType.COMMA, lexema.toString());
        }

        else if (nextChar == '!') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = new Token(TokenType.NEGATION, lexema.toString());
        }

        else if (nextChar == '+') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = new Token(TokenType.PLUS, lexema.toString());
        }

        else if (nextChar == '-') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = new Token(TokenType.MINUS, lexema.toString());
        }

        else if (nextChar == '|') {
            tipo = TokenType.GUARD;
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();


            if (this.nextChar == '|') {
                lexema.append((char) nextChar);
                this.nextChar = this.readByte();
                tipo = TokenType.OR;
            }  else if (this.nextChar == '=') {
                lexema.append((char) nextChar);
                this.nextChar = this.readByte();
                tipo = TokenType.OR_ASSERTION;
            }
            result = new Token(tipo, lexema.toString());
        }

        else if (nextChar == '&') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();

            if (this.nextChar == '&') {
                lexema.append((char) nextChar);
                this.nextChar = this.readByte();
                result = new Token(TokenType.AND, lexema.toString());
            } else if (this.nextChar == '=') {
                lexema.append((char) nextChar);
                this.nextChar = this.readByte();
                result = new Token(TokenType.AND_ASSERTION, lexema.toString());
            } else {
                throw new TokenException((char) nextChar, line, index, TokenException.EXPECTED_AND);
            }
        }

        else if (nextChar == '[') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = new Token(TokenType.BRACKETS_OPEN, lexema.toString());
        }

        else if (nextChar == ']') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            result = new Token(TokenType.BRACKETS_CLOSE, lexema.toString());
        }

        else {
            throw new TokenException((char) nextChar, line, index);
        }

        return result;
    }

    private Token getLessOrGreater(StringBuilder lexema) throws TokenException {
        TokenType tipo;
        if (nextChar == '<') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.LESS_THAN;
        } else {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            tipo = TokenType.GREATER_THAN;
        }

        if (nextChar == '=') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
            if (tipo == TokenType.LESS_THAN) {
                tipo = TokenType.LESS_OR_EQUALS_THAN;
            } else {
                tipo = TokenType.GREATER_OR_EQUALS_THAN;
            }
        }

        return new Token(tipo, lexema.toString());
    }

    private Token getStringToken(StringBuilder lexema) throws TokenException {
        while(checkSingleCharacter(lexema, '"'));
        if (this.nextChar == '"') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
        } else {
            throw new TokenException((char) nextChar, line, index, TokenException.EXPECTED_APOSTROPHE);
        }
        return new Token(TokenType.STRING, lexema.toString());
    }

    private Token getSimpleCharToken(StringBuilder lexema) throws TokenException {
        TokenType tipo = TokenType.CHAR_TYPE;

        checkSingleCharacter(lexema, '\'');
        if (this.nextChar == '\'') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
        } else {
            throw new TokenException((char) nextChar, line, index, TokenException.EXPECTED_APOSTROPHE);
        }

        return new Token(tipo, lexema.toString());
    }

    private boolean checkSingleCharacter(StringBuilder lexema, char delimiter) throws TokenException {
        boolean result = true;
        if (nextChar != delimiter && nextChar != '\\') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();

        } else if (nextChar == '\\') {
            checkCommandChar(lexema);
        } else {
             result = false;
        }
        return result;
    }

    private void checkCommandChar(StringBuilder lexema) throws TokenException {
        lexema.append((char) nextChar);
        this.nextChar = this.readByte();

        if (nextChar == 'n' || nextChar == 't' || nextChar == 'r' || nextChar == '\\' || nextChar == '"'
                || nextChar == '\'') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
        } else {
            throw new TokenException((char) nextChar, line, index, TokenException.EXPECTED_COMMAND);
        }
    }

    private Token getIdentifierOrPrimitiveToken() throws TokenException {
        Token result = null;
        StringBuilder lexema = new StringBuilder((char) nextChar);

        if (isLetter(this.nextChar)) {
            result = getIdentifierToken(lexema);
        } else if (isNumber(nextChar)) {
            this.nextChar = this.readByte();

            result = getNumericToken(lexema);

            if (result.getTokenType() == TokenType.DOUBLE_TYPE && isLetter(this.nextChar) || nextChar == '.' ||
                    lexema.toString().endsWith(".")) {
                throw new TokenException((char) nextChar, line, index, TokenException.EXPECTED_NUMBER);
            } else {
                result = getIdentifierToken(lexema);
            }
        } else {
            throw new RuntimeException("Nao deveria ter caido aqui");
        }
        return result;
    }

    private Token getNumericToken(StringBuilder lexema) throws TokenException {
        TokenType tipo = TokenType.INT_TYPE;

        while (isNumber(this.nextChar)) {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();
        }

        if (nextChar == '.') {

            tipo = TokenType.DOUBLE_TYPE;
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();

            while (isNumber(this.nextChar)) {
                lexema.append((char) nextChar);
                this.nextChar = this.readByte();
            }

            if (isUppercaseLetter(this.nextChar) || isLowercaseLetter(this.nextChar) || nextChar == '.' || lexema
                    .charAt(lexema.length() - 1) == '.') {
                throw new TokenException((char) nextChar, line, index, TokenException.EXPECTED_NUMBER);
            }
        }

        return new Token(tipo, lexema.toString());
    }

    private Token getIdentifierToken(StringBuilder lexema) throws TokenException {
        TokenType tokenType = TokenType.IDENTIFIER;;
        while (isNumber(this.nextChar) || isLetter(this.nextChar) || nextChar == '.' || nextChar == '*' || nextChar == '_') {
            lexema.append((char) nextChar);
            this.nextChar = this.readByte();

            if (nextChar == '.') {
                tokenType = TokenType.IDENTIFIER;
            }
        }

        try {
            ReservedWords reservedWords = ReservedWords.valueOf(lexema.toString().toUpperCase());
            tokenType = reservedWords.getTokenType();
        } catch (IllegalArgumentException e) {
            //
        }

        return new Token(tokenType, lexema.toString());
    }

    private Token getCommentToken(StringBuilder lexema) throws TokenException {
        TokenType tipo = TokenType.COMMENT;
        if (nextChar == '/') {
            while (nextChar != '\n') {
                lexema.append((char) nextChar);
                nextChar = readByte();
            }
            line++;
            index = 0;

        } else if (nextChar == '*') {
            int previousChar;
            do {
                lexema.append((char) nextChar);
                if (nextChar == '\n') {
                    line++;
                    index = 0;
                }
                previousChar = nextChar;
                nextChar = readByte();
                if (isEOF(this.nextChar)) {
                    throw new TokenException(TokenException.COMMENT_END);
                }
            } while (previousChar != '*' || nextChar != '/');
            lexema.append((char) nextChar);
            nextChar = readByte();
        }
        return new Token(tipo, lexema.toString());
    }

    private boolean isEOF(int nextChar) {
        return nextChar == -1;
    }

    private boolean isLetter(int nextChar) {
        return isUppercaseLetter(nextChar) || isLowercaseLetter(nextChar);
    }

    private boolean isLowercaseLetter(int nextChar) {
        return nextChar >= 'a' && nextChar <= 'z';
    }

    private boolean isUppercaseLetter(int nextChar) {
        return nextChar >= 'A' && nextChar <= 'Z';
    }

    private boolean isNumber(int nextChar) {
        return nextChar >= '0' && nextChar <= '9';
    }

    private void skipBlankChars() throws TokenException {
        while (this.nextChar == ' ' || this.nextChar == '\n'
                || this.nextChar == '\t' || this.nextChar == '\r') {
            if (this.nextChar == '\n') {
                line++;
                index = 0;
            }
            this.nextChar = this.readByte();
        }
    }

    /**
     * Gets the line.
     *
     * @return the line
     */
    public int getLine() {
        return line;
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public int getIndex() {
        return index;
    }
}