package com.dolphs.code.compiler.lexer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws TokenException, FileNotFoundException {
        Lexer lexer = new Lexer();
        Token token = null;

        System.out.println("\n\n\n");
        System.out.println(" == TESTE DO LEXER ==\n");
        System.out
                .println(" Digite alguma string terminada em \";\" e tecle ENTER:\n\n");
        System.out.print(" ");

        // passa a entrada padrão para o lexer
        // lexer.reset(System.in);

        // parar passar um arquivo como entrada
         lexer.reset(new FileInputStream("/home/rabf/Documents/dolphslib/RestRequest/classes/audit/CharInfoDTO.java"));

        do {
            token = lexer.nextToken();
            System.out.println("\t" + token + " Line: " + lexer.getLine() +" Index: " + lexer.getIndex());

        } while (token.getTokenType() != TokenType.EOF);

        System.out.println("\n == FIM ==");
    }
}
