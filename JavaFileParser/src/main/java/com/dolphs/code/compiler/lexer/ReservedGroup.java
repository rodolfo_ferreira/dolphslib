package com.dolphs.code.compiler.lexer;

public enum ReservedGroup {
    ACCESSOR, MODIFIER, FLOW, ERROR, PACKAGE, PRIMITIVE, REFERENCE, RETURN, AVOID, LITERAL
}
