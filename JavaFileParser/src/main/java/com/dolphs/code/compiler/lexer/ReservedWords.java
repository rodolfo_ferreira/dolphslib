package com.dolphs.code.compiler.lexer;

public enum ReservedWords {

    PRIVATE("private", TokenType.RES_ACCESSOR_PRIVATE, ReservedGroup.ACCESSOR),
    PROTECTED("protected", TokenType.RE_ACCESSOR_PROTECTED, ReservedGroup.ACCESSOR),
    PUBLIC("public", TokenType.RES_ACCESSOR_PUBLIC, ReservedGroup.ACCESSOR),

    ABSTRACT("abstract", TokenType.RES_MODIFIER_ABSTRACT, ReservedGroup.MODIFIER),
    CLASS("class", TokenType.RES_MODIFIER_CLASS, ReservedGroup.MODIFIER),
    EXTENDS("extends", TokenType.RES_MODIFIER_EXTENDS, ReservedGroup.MODIFIER),
    FINAL("final", TokenType.RES_MODIFIER_FINAL, ReservedGroup.MODIFIER),
    IMPLEMENTS("implements", TokenType.RES_MODIFIER_IMPLEMENTS, ReservedGroup.MODIFIER),
    INTERFACE("interface", TokenType.RES_MODIFIER_INTERFACE, ReservedGroup.MODIFIER),
    NATIVE("native", TokenType.RES_MODIFIER_NATIVE, ReservedGroup.MODIFIER),
    NEW("new", TokenType.RES_MODIFIER_NEW, ReservedGroup.MODIFIER),
    STATIC("static", TokenType.RES_MODIFIER_STATIC, ReservedGroup.MODIFIER),
    STRICTFP("strictfp", TokenType.RES_MODIFIER_STRICTFP, ReservedGroup.MODIFIER),
    SYNCHRONIZED("synchronized", TokenType.RES_MODIFIER_SYNCHRONIZED, ReservedGroup.MODIFIER),
    TRANSIENT("transient", TokenType.RES_MODIFIER_TRANSIENT, ReservedGroup.MODIFIER),
    VOLATILE("volatile", TokenType.RES_MODIFIER_VOLATILE, ReservedGroup.MODIFIER),

    BREAK("break", TokenType.RES_FLOW_BREAK, ReservedGroup.FLOW),
    CASE("case", TokenType.RES_FLOW_CASE, ReservedGroup.FLOW),
    CONTINUE("continue", TokenType.RES_FLOW_CONTINUE, ReservedGroup.FLOW),
    DEFAULT("default", TokenType.RES_FLOW_DEFAULT, ReservedGroup.FLOW),
    DO("do", TokenType.RES_FLOW_DO, ReservedGroup.FLOW),
    ELSE("else", TokenType.RES_FLOW_ELSE, ReservedGroup.FLOW),
    FOR("for", TokenType.RES_FLOW_FOR, ReservedGroup.FLOW),
    IF("if", TokenType.RES_FLOW_IF, ReservedGroup.FLOW),
    INSTANCEOF("instanceof", TokenType.RES_FLOW_INSTANCEOF, ReservedGroup.FLOW),
    RETURN("return", TokenType.RES_FLOW_RETURN, ReservedGroup.FLOW),
    SWITCH("switch", TokenType.RES_FLOW_SWITCH, ReservedGroup.FLOW),
    WHILE("while", TokenType.RES_FLOW_WHILE, ReservedGroup.FLOW),

    ASSERT("assert", TokenType.RES_ERROR_ASSERT, ReservedGroup.ERROR),
    CATCH("catch", TokenType.RES_ERROR_CATCH, ReservedGroup.ERROR),
    FINALLY("finally", TokenType.RES_ERROR_FINALLY, ReservedGroup.ERROR),
    THROW("throw", TokenType.RES_ERROR_THROW, ReservedGroup.ERROR),
    THROWS("throws", TokenType.RES_ERROR_THROWS, ReservedGroup.ERROR),
    TRY("try", TokenType.RES_ERROR_TRY, ReservedGroup.ERROR),

    IMPORT("import", TokenType.RES_PACKAGE_IMPORT, ReservedGroup.PACKAGE),
    PACKAGE("package", TokenType.RES_PACKAGE_PACKAGE, ReservedGroup.PACKAGE),

    BOOLEAN("boolean", TokenType.RES_PRIMITIVE_BOOLEAN, ReservedGroup.PRIMITIVE),
    BYTE("byte", TokenType.RES_PRIMITIVE_BYTE, ReservedGroup.PRIMITIVE),
    CHAR("char", TokenType.RES_PRIMITIVE_CHAR, ReservedGroup.PRIMITIVE),
    DOUBLE("double", TokenType.RES_PRIMITIVE_DOUBLE, ReservedGroup.PRIMITIVE),
    FLOAT("float", TokenType.RES_PRIMITIVE_FLOAT, ReservedGroup.PRIMITIVE),
    INT("int", TokenType.RES_PRIMITIVE_INT, ReservedGroup.PRIMITIVE),
    LONG("long", TokenType.RES_PRIMITIVE_LONG, ReservedGroup.PRIMITIVE),
    SHORT("short", TokenType.RES_PRIMITIVE_SHORT, ReservedGroup.PRIMITIVE),

    SUPER("super", TokenType.RES_REFERENCE_SUPER, ReservedGroup.REFERENCE),
    THIS("this", TokenType.RES_REFERENCE_THIS, ReservedGroup.REFERENCE),

    VOID("void", TokenType.RES_RETURN_VOID, ReservedGroup.RETURN),

    CONST("const", TokenType.RES_AVOID_CONST, ReservedGroup.AVOID),
    GOTO("goto", TokenType.RES_AVOID_GOTO, ReservedGroup.AVOID),

    NULL("null", TokenType.RES_LITERAL_NULL, ReservedGroup.LITERAL),
    TRUE("true", TokenType.RES_LITERAL_TRUE, ReservedGroup.LITERAL),
    FALSE("false", TokenType.RES_LITERAL_FALSE, ReservedGroup.LITERAL);

    private String word;

    private TokenType tokenType;

    private ReservedGroup groupType;

    ReservedWords(String word, TokenType tokenType, ReservedGroup groupType) {
        this.word = word;
        this.tokenType = tokenType;
        this.groupType = groupType;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public ReservedGroup getGroupType() {
        return groupType;
    }
}
