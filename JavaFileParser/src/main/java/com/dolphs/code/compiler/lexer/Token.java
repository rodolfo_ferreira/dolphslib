package com.dolphs.code.compiler.lexer;

/**
 * This class represents the Token, which is composed by the type of the token
 * and its lexeme.
 *
 * @author Rodolfo Ferreira.
 */
public class Token {

    /**
     * The lexeme.
     */
    private String lexeme;

    /**
     * The toke type.
     */
    private TokenType tokeType;

    /**
     * Instantiates a new token with its type and its lexeme, it checks the
     * parameters, if some of them is null it throws an exception.
     *
     * @param tokenType the token type
     * @param lexeme the lexeme as string
     */
    public Token(TokenType tokenType, String lexeme) {
        if (tokenType == null || lexeme == null)
            throw new NullPointerException();
        this.tokeType = tokenType;
        this.lexeme = lexeme;
    }

    /**
     * Instantiates a new token without its lexeme, it throws an exception if
     * the parameter is null.
     *
     * @param tokenType the token type
     */
    public Token(TokenType tokenType) {
        if (tokenType == null)
            throw new NullPointerException();
        this.tokeType = tokenType;
    }

    /**
     * Gets the token type of its token.
     *
     * @return the token type
     */
    public TokenType getTokenType() {
        return tokeType;
    }

    /**
     * Gets the lexeme of its token.
     *
     * @return the lexeme
     */
    public String getLexeme() {
        return lexeme;
    }

    @Override
    public String toString() {
        return "[" + getTokenType() + "," + getLexeme() + "]";
    }
}
