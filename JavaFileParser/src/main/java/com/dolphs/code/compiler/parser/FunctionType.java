package com.dolphs.code.compiler.parser;

public enum FunctionType {
    PACKAGE,
    IMPORT,
    DECLARATION,
    METHOD,
    ATTRIBUTE, CLASS, ANNOTATION, UNKNOWN, PARAM, CONSTRUCTOR

}
