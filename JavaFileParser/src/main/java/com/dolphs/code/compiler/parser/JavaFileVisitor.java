package com.dolphs.code.compiler.parser;

import com.dolphs.code.beans.*;
import com.dolphs.code.compiler.lexer.Token;
import com.dolphs.code.compiler.lexer.TokenType;
import com.dolphs.code.utils.AccessorType;
import dolphs.util.tree.Leaf;
import dolphs.util.tree.Node;
import dolphs.util.tree.Tree;
import dolphs.util.tree.Visitor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JavaFileVisitor implements Visitor<FunctionType, Void> {

    private JavaClass javaClass;

    public JavaFileVisitor() {
        this.javaClass = new JavaClass();
    }

    @Override
    public Void visit(Node<FunctionType> node) {

        for (Tree child : node.getChildren()) {
            visit(child);
        }

        switch (node.get()) {
            case ANNOTATION:
                getAnnotation(node);
                break;
            case ATTRIBUTE:
                getAttribute(node);
                break;
            case PACKAGE:
                getPackage(node);
                break;
            case IMPORT:
                getImports(node);
                break;
            case METHOD:
                getMethod(node);
                break;
            case CONSTRUCTOR:
                // TODO getConstructor(node);
                break;
            case DECLARATION:
                getCls(node);
                break;
        }
        return null;
    }

    private void getCls(Node<FunctionType> node) {
        List<Tree> children = node.getChildren();
        int i = 0;
        Tree child = children.get(i++);
        Leaf<Token> leaf;
        while ((child instanceof Leaf) &&
                ((leaf = (Leaf<Token>) child).get().getTokenType() != TokenType.BRACES_OPEN)) {

            switch (leaf.get().getTokenType()) {
                case RES_ACCESSOR_PRIVATE:
                case RES_ACCESSOR_PUBLIC:
                    javaClass.setAccessorType(AccessorType.valueOf(leaf.get().getLexeme().toUpperCase()));
                    break;
                case IDENTIFIER:
                    javaClass.setName(leaf.get().getLexeme());
                    break;
            }
            child = children.get(i++);
        }
    }

    private void getMethod(Node<FunctionType> node) {
        JavaMethod javaMethod;

        AccessorType accessorType = null;
        String returnType = null;
        String name = null;
        String code = null;
        Map<String, String> params = new HashMap<>();

        for (Tree tree : node.getChildren()) {
            if (tree instanceof Leaf) {
                Leaf<Token> tokenLeaf = (Leaf<Token>) tree;

                switch (tokenLeaf.get().getTokenType()) {
                    case RES_ACCESSOR_PRIVATE:
                    case RES_ACCESSOR_PUBLIC:
                        accessorType = AccessorType.valueOf(tokenLeaf.get().getLexeme().toUpperCase());
                        break;
                    case RES_RETURN_VOID:
                        returnType = "void";
                        break;
                    case RES_PRIMITIVE_DOUBLE:
                    case RES_PRIMITIVE_BOOLEAN:
                    case RES_PRIMITIVE_BYTE:
                    case RES_PRIMITIVE_CHAR:
                    case RES_PRIMITIVE_FLOAT:
                    case RES_PRIMITIVE_INT:
                    case RES_PRIMITIVE_LONG:
                    case RES_PRIMITIVE_SHORT:
                        returnType = tokenLeaf.get().getLexeme();
                        break;
                    case IDENTIFIER:
                        if (returnType == null) {
                            returnType = tokenLeaf.get().getLexeme();
                        } else {
                            name = tokenLeaf.get().getLexeme();
                        }
                        break;
                }

            } else if (tree instanceof Node) {
                Node<FunctionType> nodeFunc = (Node<FunctionType>) tree;

                switch (nodeFunc.get()) {
                    case PARAM:
                        addParam(nodeFunc.getChildren(), params);
                        break;
                    case UNKNOWN:
                        code = getCode(nodeFunc.getChildren());
                }
            }
        }

        javaMethod = new JavaMethod(accessorType, returnType, name, params, code);

        javaClass.addMethod(javaMethod);
    }

    private String getCode(List<Tree> children) {
        StringBuilder code = new StringBuilder("");
        for (Tree child : children) {
            if (child instanceof Leaf) {
                Leaf<Token> leaf = (Leaf<Token>) child;
                Token token = leaf.get();
                if (token.getTokenType() == TokenType.SEMI_COLON) {
                    code.setLength(code.length() - 1);
                    code.append(leaf.get().getLexeme());
                    code.append("\n");
                } else {
                    code.append(leaf.get().getLexeme());
                    code.append(" ");
                }
            } else if (child instanceof Node) {
                Node<FunctionType> node = (Node<FunctionType>) child;
                code.append(getCode(node.getChildren()));
            }
        }
        return code.toString();
    }

    private void addParam(List<Tree> children, Map<String, String> params) {
        Leaf<Token> leaf0 = (Leaf<Token>) children.get(0);
        Leaf<Token> leaf1 = (Leaf<Token>) children.get(1);

        String type = leaf0.get().getLexeme();
        String name = leaf1.get().getLexeme();

        params.put(name, type);
    }

    private void getImports(Node node) {
        Leaf<Token> leaf0 = (Leaf<Token>) node.getChildren().get(1);

        String all = leaf0.get().getLexeme();
        String packag = all.substring(0, all.lastIndexOf('.'));
        String cls = all.substring(all.lastIndexOf('.') + 1, all.length());

        JavaImport javaImport = new JavaImport(packag, cls);
        javaClass.addImport(javaImport);
    }

    private void getPackage(Node node) {
        Leaf<Token> leaf0 = (Leaf<Token>) node.getChildren().get(1);

        String packag = leaf0.get().getLexeme();

        JavaPackage javaPackage = new JavaPackage(packag);
        javaClass.setPackag(javaPackage);
    }

    private void getAttribute(Node node) {
        List<Tree> children = node.getChildren();
        int i = 0;
        Tree child = children.get(i++);

        AccessorType accessorType = AccessorType.PACKAGE;
        String returnType = "";
        String name = "";

        while (name.isEmpty() || returnType.isEmpty() && i < children.size()) {
            if (child instanceof Leaf) {
                Leaf<Token> tokenLeaf = (Leaf<Token>) child;
                switch (tokenLeaf.get().getTokenType()) {
                    case RES_ACCESSOR_PRIVATE:
                    case RES_ACCESSOR_PUBLIC:
                        accessorType = AccessorType.valueOf(tokenLeaf.get().getLexeme().toUpperCase());
                        break;
                    case RES_PRIMITIVE_DOUBLE:
                    case RES_PRIMITIVE_BOOLEAN:
                    case RES_PRIMITIVE_BYTE:
                    case RES_PRIMITIVE_CHAR:
                    case RES_PRIMITIVE_FLOAT:
                    case RES_PRIMITIVE_INT:
                    case RES_PRIMITIVE_LONG:
                    case RES_PRIMITIVE_SHORT:
                        returnType = tokenLeaf.get().getLexeme();
                        break;
                    case IDENTIFIER:
                        if (returnType.isEmpty()) {
                            returnType = tokenLeaf.get().getLexeme();
                        } else {
                            name = tokenLeaf.get().getLexeme();
                        }
                        break;
                }
            }
            child = children.get(i++);
        }

        JavaAttribute javaAttribute = new JavaAttribute(accessorType, returnType, name);
        if (children.get(0) instanceof Node) {
            javaAttribute.setJavaAnnotation(getAnnotation((Node) children.get(0)));
        }
        javaClass.addAttribute(javaAttribute);
    }

    private JavaAnnotation getAnnotation(Node node) {
        Leaf<Token> leaf0 = (Leaf<Token>) node.getChildren().get(1);
        Leaf<Token> leaf1 = (Leaf<Token>) node.getChildren().get(3);

        String attribute = leaf0.get().getLexeme();
        String value = leaf1.get().getLexeme();
        value = value.substring(1, value.length() - 1);

        JavaAnnotation javaAnnotation = new JavaAnnotation(attribute, value);
        return javaAnnotation;
    }

    @Override
    public Void visit(Leaf leaf) {
        return null;
    }

    @Override
    public Void visit(Tree tree) {
        if (tree instanceof Node) {
            visit((Node) tree);
        } else {
            visit((Leaf) tree);
        }
        return null;
    }

    public JavaClass getJavaClass() {
        return javaClass;
    }
}
