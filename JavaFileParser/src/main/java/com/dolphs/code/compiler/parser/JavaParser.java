package com.dolphs.code.compiler.parser;

import com.dolphs.code.compiler.lexer.*;
import dolphs.util.tree.Leaf;
import dolphs.util.tree.Node;
import dolphs.util.tree.Tree;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class JavaParser {


    /** The tokenizer to receive the tokens. */
    private Lexer lexer;

    /** The current token from tokenizer. */
    private Token currentToken;

    /**
     * This method instantiates a new parser, instantiating a new Tokenizer to
     * analyze tokens.
     */
    public JavaParser() {
        this.lexer = new Lexer();
    }

    /**
     * This method resets the InputStream start to read the tokens and starts to
     * parse the Program.
     *
     * @param input
     *            the input stream, which contains the code
     * @return Tree, the header node of the syntax tree
     * @throws ParserException
     *             if some token not expected is read, it will be thrown by this
     *             exception
     * @throws TokenException
     *             if some token is not recognized
     * @throws FileNotFoundException
     *             the file not found exception
     */
    public Tree parse(InputStream input) throws ParserException, TokenException {
        lexer.reset(input);
        currentToken = lexer.nextToken();
        Node<FunctionType> node = new Node<>(FunctionType.CLASS);
        parseClassFile(node);
        acceptToken(TokenType.EOF);
        return node;
    }

    /**
     * Method that accepts the Token checked before.
     *
     * @return the syntaxt tree of this node
     * @throws TokenException
     *             if some token is not recognized
     */
    private Leaf<Token> acceptToken() throws TokenException {
        Leaf<Token> leaf = new Leaf<>(currentToken);
        currentToken = lexer.nextToken();
        return leaf;
    }

    /**
     * Method that try to accepts the Token expected, if it is not the expected
     * token will throw an exception.
     *
     * @param tokenType
     *            the token type
     * @return the syntaxt tree of this node
     * @throws ParserException
     *             if some token not expected is read, it will be thrown by this
     *             exception
     * @throws TokenException
     *             if some token is not recognized
     */
    private Leaf<Token> acceptToken(TokenType tokenType) throws ParserException,
            TokenException {
        if (currentToken.getTokenType() == tokenType) {
            return acceptToken();

        } else {
            throw new ParserException(currentToken.getTokenType(),
                    lexer.getLine(), lexer.getIndex() - 1 - 1, tokenType);
        }
    }

    /**
     * Method responsible to parse a program.
     *
     * <Program> ::= <Global Statement> <Global Statement>*
     *
     * @return the syntaxt tree of this node
     * @throws ParserException
     *             if some token not expected is read, it will be thrown by this
     *             exception
     * @throws TokenException
     *             if some token is not recognized
     */
    private void parseClassFile(Node<FunctionType> fatherNode) throws ParserException,
            TokenException {
        parseClassHeader(fatherNode);
        parseClassBody(fatherNode);
    }

    private void parseClassHeader(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        parsePackage(fatherNode);
        parseImports(fatherNode);
    }

    private void parseImports(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        while(currentToken.getTokenType() == TokenType.RES_PACKAGE_IMPORT) {
            Node<FunctionType> importNode = new Node<>(FunctionType.IMPORT);
            importNode.addChildren(acceptToken());
            importNode.addChildren(acceptToken(TokenType.IDENTIFIER));
            importNode.addChildren(acceptToken(TokenType.SEMI_COLON));
            fatherNode.addChildren(importNode);
        }
    }

    private void parsePackage(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        Node<FunctionType> packageNode = new Node<>(FunctionType.PACKAGE);
        packageNode.addChildren(acceptToken(TokenType.RES_PACKAGE_PACKAGE));
        packageNode.addChildren(acceptToken(TokenType.IDENTIFIER));
        packageNode.addChildren(acceptToken(TokenType.SEMI_COLON));
        fatherNode.addChildren(packageNode);
    }

    /**
     * Method responsible to parse a global statement.
     *
     * <Global Statement> ::= <Variable Statement>| <Function Statement>
     *
     * @return the syntaxt tree of this node
     * @throws ParserException
     *             if some token not expected is read, it will be thrown by this
     *             exception
     * @throws TokenException
     *             if some token is not recognized
     */
    private void parseClassBody(Node<FunctionType> fatherNode) throws ParserException, TokenException {
        Node<FunctionType> declarationNode = new Node<>(FunctionType.DECLARATION);
        parseAccessor(declarationNode);
        declarationNode.addChildren(acceptToken(TokenType.RES_MODIFIER_CLASS));
        declarationNode.addChildren(acceptToken(TokenType.IDENTIFIER));
        declarationNode.addChildren(acceptToken(TokenType.BRACES_OPEN));
        parseClassBodyContent(declarationNode);
        declarationNode.addChildren(acceptToken(TokenType.BRACES_CLOSE));
        fatherNode.addChildren(declarationNode);
    }

    private void parseClassBodyContent(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        while (currentToken.getTokenType() != TokenType.BRACES_CLOSE) {
            parseDeclaration(fatherNode);
        }
    }

    private void parseAnnotations(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        if (currentToken.getTokenType() == TokenType.ANNOTATION_MARK) {
            Node<FunctionType> annotationNode = new Node<>(FunctionType.ANNOTATION);
            annotationNode.addChildren(acceptToken());
            annotationNode.addChildren(acceptToken(TokenType.IDENTIFIER));
            annotationNode.addChildren(acceptToken(TokenType.PARENTHESIS_OPEN));
            annotationNode.addChildren(acceptToken(TokenType.STRING));
            annotationNode.addChildren(acceptToken(TokenType.PARENTHESIS_CLOSE));
            fatherNode.addChildren(annotationNode);
        }
    }

    private void parseDeclaration(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        Node<FunctionType> unknownNode = new Node<>(FunctionType.UNKNOWN);
        parseAnnotations(unknownNode);
        parseAccessor(unknownNode);
        parseType(unknownNode);

        if (currentToken.getTokenType() == TokenType.PARENTHESIS_OPEN) {
            unknownNode.addChildren(acceptToken());
            parseParams(unknownNode);
            unknownNode.addChildren(acceptToken(TokenType.PARENTHESIS_CLOSE));
            unknownNode.set(FunctionType.CONSTRUCTOR);

        } else {
            parseAttributeOrMethod(unknownNode);
        }

        fatherNode.addChildren(unknownNode);
    }

    private void parseAttributeOrMethod(Node<FunctionType> fatherNode) throws ParserException, TokenException {
        updateGenericLexeme(fatherNode);
        fatherNode.addChildren(acceptToken(TokenType.IDENTIFIER));

        if (currentToken.getTokenType() == TokenType.ASSERTION) {
            fatherNode.addChildren(acceptToken());
            // TODO handle assertion
            Node<FunctionType> unknownNode = new Node<>(FunctionType.UNKNOWN);
            while (currentToken.getTokenType() != TokenType.SEMI_COLON) {
                unknownNode.addChildren(acceptToken());
            }
            fatherNode.addChildren(unknownNode);
            fatherNode.set(FunctionType.ATTRIBUTE);
        }

        if (currentToken.getTokenType() == TokenType.SEMI_COLON) {
            fatherNode.addChildren(acceptToken());
            fatherNode.set(FunctionType.ATTRIBUTE);
        } else {
            parseMethodParams(fatherNode);
            parseMethodBlock(fatherNode);
            fatherNode.set(FunctionType.METHOD);
        }
    }

    private void parseType(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        if (currentToken.getTokenType() == TokenType.RES_RETURN_VOID ||
                currentToken.getTokenType() == TokenType.RES_PRIMITIVE_BOOLEAN ||
                currentToken.getTokenType() == TokenType.RES_PRIMITIVE_BYTE ||
                currentToken.getTokenType() == TokenType.RES_PRIMITIVE_CHAR ||
                currentToken.getTokenType() == TokenType.RES_PRIMITIVE_DOUBLE ||
                currentToken.getTokenType() == TokenType.RES_PRIMITIVE_FLOAT ||
                currentToken.getTokenType() == TokenType.RES_PRIMITIVE_INT ||
                currentToken.getTokenType() == TokenType.RES_PRIMITIVE_LONG ||
                currentToken.getTokenType() == TokenType.RES_PRIMITIVE_SHORT) {
            fatherNode.addChildren(acceptToken());
        } else {
            fatherNode.addChildren(acceptToken(TokenType.IDENTIFIER));
        }
    }

    private void parseMethodBlock(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        fatherNode.addChildren(acceptToken(TokenType.BRACES_OPEN));
        parseMethodCode(fatherNode);
        fatherNode.addChildren(acceptToken(TokenType.BRACES_CLOSE));
    }

    private void parseMethodCode(Node<FunctionType> fatherNode) throws TokenException {
        //TODO evaluate the expressions
        Node<FunctionType> unknownNode = new Node<>(FunctionType.UNKNOWN);
        while (currentToken.getTokenType() != TokenType.BRACES_CLOSE) {
            unknownNode.addChildren(acceptToken());
        }
        fatherNode.addChildren(unknownNode);
    }

    private void parseMethodParams(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        fatherNode.addChildren(acceptToken(TokenType.PARENTHESIS_OPEN));
        parseParams(fatherNode);
        fatherNode.addChildren(acceptToken(TokenType.PARENTHESIS_CLOSE));
    }

    private void parseParams(Node<FunctionType> fatherNode) throws TokenException, ParserException {
        while (currentToken.getTokenType() != TokenType.PARENTHESIS_CLOSE) {
            Node<FunctionType> paramNode = new Node<>(FunctionType.PARAM);
            parseType(paramNode);
            updateGenericLexeme(paramNode);
            paramNode.addChildren(acceptToken(TokenType.IDENTIFIER));
            if (currentToken.getTokenType() == TokenType.COMMA) {
                paramNode.addChildren(acceptToken());
            }
            fatherNode.addChildren(paramNode);
        }
    }

    private void updateGenericLexeme(Node<FunctionType> paramNode) throws TokenException, ParserException {
        if (currentToken.getTokenType() == TokenType.LESS_THAN) {
            Leaf<Token> tokenLeaf = (Leaf<Token>) paramNode.getChildren().get(paramNode.getChildren().size() - 1);
            // TODO CREATE AN INDEPENDENT NODE
            String lexeme = tokenLeaf.get().getLexeme();
            lexeme += currentToken.getLexeme();
            acceptToken();
            lexeme += currentToken.getLexeme();
            acceptToken(TokenType.IDENTIFIER);
            lexeme += currentToken.getLexeme();
            acceptToken(TokenType.GREATER_THAN);
            tokenLeaf.set(new Token(TokenType.IDENTIFIER, lexeme));
        }
    }

    private void parseAccessor(Node<FunctionType> fatherNode) throws TokenException {
        ReservedWords reservedWord = ReservedWords.valueOf(currentToken.getLexeme().toUpperCase());
        if (reservedWord.getGroupType() == ReservedGroup.ACCESSOR) {
            fatherNode.addChildren(acceptToken());
        }
    }
}
