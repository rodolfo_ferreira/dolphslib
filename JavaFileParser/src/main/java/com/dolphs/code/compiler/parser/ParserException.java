/*
 *
 * Copyright (c) 2015, Rodolfo Ferreira and Juliana Ferreira. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is a part of the Kerigma compiler, created to learn how compilers works.
 * It was implemented to our degree in Computer Science, in the discipline
 * called "compilers", given by professor Pablo Sampaio, from
 * Federal Rural University from Pernambuco.
 *
 * Please contact us, rodolfoandreferreira@gmail.com or julifs.13@gmail.com
 * if you need additional information or have any questions.
 *
 */
package com.dolphs.code.compiler.parser;

import com.dolphs.code.compiler.lexer.TokenType;

/**
 * This class is an Exception subclass, it's used to throw exceptions of the
 * Parser class
 *
 * @author Rodolfo Ferreira and Juliana Ferreira.
 */
@SuppressWarnings("serial")
public class ParserException extends Exception {

    /** The Constant String used when an unexpected token is found. */
    private static final String PARSE_EXCPETION_MESSAGE = "Unexpected Token at line %d, column %d: \"%s\".";

    /** The Constant String used when the parser expects other prefixes. */
    private static final String EXPECTED_MESSAGE_PREFIX = " It is expected ";

    /**
     * Instantiates a new parser exception with lexeme, line and index where the
     * problem has occurred
     *
     * @param lexeme
     *            the lexeme not expected
     * @param line
     *            the line where the unexpected lexeme was found.
     * @param index
     *            the index where the unexpected lexeme was found.
     */
    public ParserException(String lexeme, int line, int index) {
        super(String.format(PARSE_EXCPETION_MESSAGE, line, index, lexeme));
    }

    /**
     * Instantiates a new parser exception.
     *
     * @param lexeme
     *            the lexeme not expected
     * @param line
     *            the line where the unexpected lexeme was found.
     * @param index
     *            the index where the unexpected lexeme was found.
     * @param expected
     *            the expected strings.
     */
    public ParserException(String lexeme, int line, int index,
                           String... expected) {
        super(String.format(buildString(expected), lexeme, expected, line,
                index));
    }

    /**
     * Instantiates a new parser exception.
     *
     * @param tokenType
     *            the tokenType not expected
     * @param line
     *            the line where the unexpected lexeme was found.
     * @param index
     *            the index where the unexpected lexeme was found.
     */
    public ParserException(TokenType tokenType, int line, int index) {
        super(String.format(PARSE_EXCPETION_MESSAGE, line, index,
                tokenType.toString()));
    }

    /**
     * Instantiates a new parser exception.
     *
     * @param tokenType
     *            the tokenType not expected
     * @param line
     *            the line where the unexpected lexeme was found.
     * @param index
     *            the index where the unexpected lexeme was found.
     * @param tokens
     *            the expected tokens
     */
    public ParserException(TokenType tokenType, int line, int index,
                           TokenType... tokens) {
        super(String.format(buildString(tokens), line, index,
                tokenType.toString(), tokens));
    }

    /**
     * Method responsible to build the String if you have expected tokens or
     * strings
     *
     * @param tokens
     *            the tokens
     * @return the string
     */
    private static String buildString(Object[] tokens) {
        String message = PARSE_EXCPETION_MESSAGE + EXPECTED_MESSAGE_PREFIX;
        for (int i = 0; i < tokens.length; i++) {
            if (i == tokens.length - 1)
                message += tokens[i] + ".";
            else if (i == tokens.length - 2)
                message += tokens[i] + " or ";
            else
                message += tokens[i] + ", ";
        }
        return message;
    }
}