package com.dolphs.code.compiler.parser;

import com.dolphs.code.compiler.lexer.Token;

import java.util.ArrayList;
import java.util.List;

public class ParserNode {

    private List<Token> tokenList;

    private FunctionType parseType;

    public ParserNode(FunctionType parseType) {
        this();
        this.parseType = parseType;
    }

    public ParserNode() {
        this.tokenList = new ArrayList<>();
    }

    public List<Token> getTokenList() {
        return tokenList;
    }

    public void setTokenList(List<Token> tokenList) {
        this.tokenList = tokenList;
    }

    public FunctionType getParseType() {
        return parseType;
    }

    public void setParseType(FunctionType parseType) {
        this.parseType = parseType;
    }
}
