package com.dolphs.code.repository;

import com.dolphs.code.beans.JavaClass;

import java.util.List;

public interface IRepository {

    void create(JavaClass javaClass);

    List<JavaClass> getAll();
}
