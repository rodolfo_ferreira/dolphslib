package com.dolphs.code.repository;

import com.dolphs.code.beans.JavaClass;

import java.util.ArrayList;
import java.util.List;

public class Repository implements IRepository {

    private List<JavaClass> javaClassList;

    private static IRepository instance;

    public static IRepository getInstance() {
        if (instance == null) {
            instance = new Repository();
        }
        return instance;
    }

    public Repository() {
        this.javaClassList = new ArrayList<>();
    }

    @Override
    public void create(JavaClass javaClass) {
        this.javaClassList.add(javaClass);
    }

    public List<JavaClass> getAll() {
        return javaClassList;
    }
}
