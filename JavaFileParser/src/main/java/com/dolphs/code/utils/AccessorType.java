package com.dolphs.code.utils;

public enum AccessorType {
    PRIVATE("private"), PROTECTED("protected"), PACKAGE("/* package */"), PUBLIC("public");

    private String value;

    AccessorType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
