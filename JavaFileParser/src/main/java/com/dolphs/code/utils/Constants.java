package com.dolphs.code.utils;

public class Constants {

    public static final String PUBLIC_ACCESSOR_CONSTANT = "public";
    public static final String PRIVATE_ACCESSOR_CONSTANT = "private";
    public static final String PROTECTED_ACCESSOR_CONSTANT = "protected";
    public static final String STATIC_CONSTANT = "static";
    public static final String FINAL_CONSTANT = "final";
}
