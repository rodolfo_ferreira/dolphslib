package com.dolphs.code.business;

import com.dolphs.code.beans.JavaAttribute;
import com.dolphs.code.beans.JavaClass;
import com.dolphs.code.beans.JavaMethod;
import com.dolphs.code.beans.JavaPackage;
import com.dolphs.code.compiler.lexer.TokenException;
import com.dolphs.code.compiler.parser.ParserException;
import com.dolphs.code.utils.AccessorType;
import dolphs.exceptions.CouldNotReadException;
import dolphs.io.FileUtils;
import dolphs.io.exceptions.CouldNotWriteException;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class JavaClassBOTest {

    JavaClassBO javaClassBO;

    @Before
    public void presetup() {
        javaClassBO = new JavaClassBO();
    }

    @Test
    public void createJavaClass() throws CouldNotWriteException {
        JavaClass javaClass = new JavaClass(new JavaPackage("com.dolphs.wow"), AccessorType.PUBLIC, "Test");
        JavaAttribute javaAttribute = new JavaAttribute(AccessorType.PRIVATE, "String", "name");
        JavaMethod javaMethod = new JavaMethod(AccessorType.PUBLIC, "String" ,"getName");
        javaMethod.setCode("return this.name;\n");

        javaClass.addAttribute(javaAttribute);
        javaClass.addMethod(javaMethod);

        FileUtils.write(new File(System.getProperty("user.home") + "/Documents/test.txt"), javaClass.toString());
    }

    @Test
    public void parse() throws CouldNotReadException, ParserException, TokenException {
        File file = new File("/home/rabf/Documents/dolphslib/classes/audit/CharInfoDTO.java");
        javaClassBO.parse(file);
    }

    @Test
    public void mergeFile() throws CouldNotReadException, ParserException, TokenException {
        File file = new File("/home/rabf/Documents/dolphslib/RestRequest/classes/audit/CharInfoDTO.java");
        File file1 = new File("/home/rabf/Documents/dolphslib/RestRequest/classes/reputation/CharInfoDTO.java");

        System.out.println(javaClassBO.mergeFile(file, file1));
    }

    @Test
    public void mergeWowFile() throws CouldNotReadException, ParserException, TokenException, CouldNotWriteException {
        File dir = new File("/home/rabf/Documents/dolphslib/RestRequest/classes/");
        File subDirs[] = dir.listFiles();
        JavaClass javaClass = null;
        for (File subDir : subDirs) {
            File files[] = subDir.listFiles();
            for (File file : files) {
                if (file.getName().equals("CharInfoDTO.java")){
                    if (javaClass == null) {
                        javaClass = javaClassBO.parse(file);
                    } else {
                        JavaClass newJavaClass = javaClassBO.parse(file);
                        javaClass = javaClassBO.merge(javaClass, newJavaClass);
                    }
                }
            }
        }

        FileUtils.write(new File(System.getProperty("user.dir") + "/classes/" + javaClass.getName() + ".java"),
                javaClass.toString
                ());
    }
}