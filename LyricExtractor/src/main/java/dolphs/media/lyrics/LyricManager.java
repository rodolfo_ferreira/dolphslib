package dolphs.media.lyrics;

import dolphs.media.lyrics.agents.LyricAgent;
import dolphs.media.lyrics.agents.util.MetadataUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class managers the Lyric Agent and uses weighted scores to elect the best available lyric
 */
public class LyricManager {

    private static final Logger logger = Logger.getLogger(LyricManager.class.getSimpleName());

    private List<LyricAgent> agents;

    private Map<Double, String> lyricPoints;

    public LyricManager() {
        this.agents = new ArrayList<>();
    }

    /**
     * This method finds the best lyric of all registered Lyric Agents. It scores each result of the lyric agents and
     * then returns the one with the highest score.
     *
     * @param artist, the artist of the lyric
     * @param title,  the title of the lyric
     * @return the best lyric among the Lyric Agents
     */
    public String findBestLyric(String artist, String title) {
        lyricPoints = new HashMap<>();
        List<Thread> threads = startAgents(artist, title);

        while (areThreadsRunning(threads)) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        double bestScore = getBestScore();
        return lyricPoints.getOrDefault(bestScore, "");
    }

    /**
     * This method starts a thread for each Lyric Agent to find the lyric and add it in a list.
     *
     * @param artist, the artist of the lyric
     * @param title,  the title of the lyuric
     * @return a list of all triggered threads
     */
    private List<Thread> startAgents(String artist, String title) {
        List<Thread> threads = new ArrayList<>();

        for (LyricAgent lyricAgent : agents) {
            Thread thread = new Thread(() -> {
                try {
                    String lyric = lyricAgent.findLyric(artist, title);
                    double score = getScore(title, lyric);
                    logger.log(Level.INFO, "{0} Score: {1}", new String[]{lyricAgent.toString(), String.valueOf(score)});
                    lyricPoints.put(score, lyric);
                } catch (Exception e) {
                    logger.log(Level.INFO, "The Agent {0} could not retrieve lyrics", lyricAgent);
                }
            });
            threads.add(thread);
            thread.start();
        }
        return threads;
    }

    /**
     * This method evaluates the score of a given lyric. It will give 50% of weight in recurrence of the title, 30% for
     * the number of words in the lyric and 20% for the amount of break lines and sum all.
     *
     * @param title, the title of the lyric
     * @param lyric, the lyric
     * @return the sum of all weighted points.
     */
    private double getScore(String title, String lyric) {
        double titleRec = StringUtils.countMatches(MetadataUtils.normalizeString(lyric.toLowerCase()),
                MetadataUtils.normalizeString(title.toLowerCase())) * 0.5;
        double length = lyric.split("\\W+").length * 0.3;
        double numBreakLines = StringUtils.countMatches(lyric, "\n") * 0.2;
        return titleRec + numBreakLines + length;
    }

    /**
     * This method interates over the {@link #lyricPoints} map and gets the highest score.
     *
     * @return the highest score.
     */
    private double getBestScore() {
        double bestScore = 0;
        for (Map.Entry<Double, String> map : lyricPoints.entrySet()) {
            double score = map.getKey();
            if (score > bestScore) {
                bestScore = score;
            }
        }
        return bestScore;
    }

    /**
     * This method checks if there are threads running in the given list of threads.
     *
     * @param threads, the list of threads to be checked
     * @return true if there is at least one thread running, false otherwise.
     */
    private boolean areThreadsRunning(List<Thread> threads) {
        return threads.stream().anyMatch(Thread::isAlive);
    }

    /**
     * This method register a Lyric Agent in the Lyric Manager to be used to find lyrics.
     *
     * @param lyricAgent, the lyric agent to be registered
     */
    public void registerLyricAgent(LyricAgent lyricAgent) {
        this.agents.add(lyricAgent);
    }
}
