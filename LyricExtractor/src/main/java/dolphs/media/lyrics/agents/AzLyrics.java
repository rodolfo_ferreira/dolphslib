package dolphs.media.lyrics.agents;

import dolphs.media.lyrics.agents.util.LyricManipulationUtils;
import dolphs.media.lyrics.agents.util.MetadataUtils;
import dolphs.media.lyrics.extraction.BiggerLyricExtractionStrategy;

import java.util.List;

public class AzLyrics extends LyricAgent {

    private static final String URL_FORMAT = "https://www.azlyrics.com/lyrics/%s/%s.html";
    private static final String STRING_DELIMITER = "Submit Corrections";

    public AzLyrics() {
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#getUrl(String, String)
     */
    @Override
    String getUrl(String artist, String title) {
        if (title.contains(StringConstants.FEAT_CONSTANT)) {
            title = MetadataUtils.substringEndIndexOf(title, StringConstants.FEAT_CONSTANT);
        }

        if (artist.contains(StringConstants.FEAT_CONSTANT)) {
            artist = MetadataUtils.substringEndIndexOf(artist, StringConstants.FEAT_CONSTANT);
        }

        if (artist.contains(StringConstants.AMPERSAND)) {
            artist = MetadataUtils.substringEndIndexOf(artist, StringConstants.AMPERSAND);
        }

        title = MetadataUtils.removeParenthesisContent(title).trim();
        title = MetadataUtils.removeRepeated(title, " ").trim();
        this.title = title;
        registerExtractor(new BiggerLyricExtractionStrategy(2, STRING_DELIMITER));
        title = MetadataUtils.remove(title, " ").trim();
        title = MetadataUtils.removeNonAlphaNumerical(title).trim();
        title = MetadataUtils.removeRepeated(title, "-").trim();

        artist = MetadataUtils.normalizeString(artist);
        artist = MetadataUtils.removeRepeated(artist, " ");
        artist = MetadataUtils.remove(artist, " ");
        artist = MetadataUtils.removeNonAlphaNumerical(artist).trim();
        artist = MetadataUtils.removeRepeated(artist, " ").trim();
        artist = MetadataUtils.removeRepeated(artist, "-").trim();

        return MetadataUtils.removeRepeated(String.format(URL_FORMAT, artist.toLowerCase(), title.toLowerCase()), "-");
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#cleanCandidate(String) Ø
     */
    @Override
    String cleanCandidate(String candidate) {
        candidate = LyricManipulationUtils.removeExtraBreakLines(candidate);

        // Replaces one or more break lines by one break line only
        candidate = candidate.replaceAll("[\n]+ ", "\n");
        candidate = LyricManipulationUtils.squashBreakLines(candidate);
        candidate = LyricManipulationUtils.addBlankLineBeforeIfNoExist(candidate, "\\[");

        return candidate.trim();
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#removeWorstCandidates(List)
     */
    @Override
    void removeWorstCandidates(List<String> candidates) {
        // There is not worst candidates to be removed
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
