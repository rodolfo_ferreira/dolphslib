package dolphs.media.lyrics.agents;

import dolphs.media.lyrics.agents.util.LyricManipulationUtils;
import dolphs.media.lyrics.agents.util.MetadataUtils;
import dolphs.media.lyrics.extraction.BiggerLyricExtractionStrategy;
import dolphs.media.lyrics.extraction.RecurrenceLyricExtractionStrategy;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class GeniusLyrics extends LyricAgent {

    private static final String URL_FORMAT = "https://genius.com/%s-%s-lyrics";
    private static final String STRING_DELIMITER = "More on Genius";

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#getUrl(String, String)
     */
    @Override
    String getUrl(String artist, String title) {
        title = CleanTitle(title);
        artist = cleanArtist(artist);
        this.title = title;
        registerExtractor(new BiggerLyricExtractionStrategy(2, STRING_DELIMITER));
        registerExtractor(new RecurrenceLyricExtractionStrategy(2, true, STRING_DELIMITER));

        title = MetadataUtils.normalizeString(title);
        artist = MetadataUtils.normalizeString(artist);
        artist = MetadataUtils.replaceNonAlphaNumerical(artist, "-");
        title = MetadataUtils.replaceNonAlphaNumerical(title, "-");
        return String.format(URL_FORMAT, artist.replaceAll(" ", "-"), title.replaceAll(" ", "-"));
    }

    /**
     * This method cleans the title to be used in url. It normalizes, remove feat, repeated spaces and parenthesis
     * content.
     *
     * @param title, the title to be cleaned.
     * @return the string cleaned
     */
    private String CleanTitle(String title) {
        if (title.contains(StringConstants.FEAT_CONSTANT)) {
            title = MetadataUtils.substringEndIndexOf(title, StringConstants.FEAT_CONSTANT).trim();
        }
        title = MetadataUtils.removeRepeated(title, " ");
        return MetadataUtils.removeParenthesisContent(title).trim();
    }

    /**
     * This method cleans the artist to be used in url. It normalizes, remove feat, repeated spaces and replaces non
     * alphanumerical by '-'.
     *
     * @param artist, the artist to be cleaned.
     * @return the string cleaned
     */
    private String cleanArtist(String artist) {
        if (artist.contains(StringConstants.FEAT_CONSTANT)) {
            artist = MetadataUtils.substringEndIndexOf(artist, StringConstants.FEAT_CONSTANT).trim();
        }
        artist = MetadataUtils.removeRepeated(artist, " ");
        return MetadataUtils.replaceNonAlphaNumerical(artist, "-").trim();
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#cleanCandidate(String) Ø
     */
    @Override
    String cleanCandidate(String candidate) {
        candidate = LyricManipulationUtils.removeExtraBreakLines(candidate);
        candidate = LyricManipulationUtils.squashBreakLines(candidate);
        candidate = LyricManipulationUtils.addBlankLineBeforeIfNoExist(candidate, "\\[");
        return candidate.trim();
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#removeWorstCandidates(List)
     */
    @Override
    void removeWorstCandidates(List<String> candidates) {
        candidates.removeIf(s -> StringUtils.countMatches(s, "\n") < 10);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
