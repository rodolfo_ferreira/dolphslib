package dolphs.media.lyrics.agents;

import dolphs.media.lyrics.agents.util.MetadataUtils;
import dolphs.media.lyrics.extraction.BiggerLyricExtractionStrategy;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LetrasMusLyrics extends LyricAgent {

    private static final String URL_FORMAT = "https://www.letras.mus.br/%s/%s/";
    private static final String STRING_DELIMITER = "Composição:";

    public LetrasMusLyrics() {
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#getUrl(String, String)
     */
    @Override
    String getUrl(String artist, String title) {
        if (title.contains(StringConstants.FEAT_CONSTANT)) {
            title = MetadataUtils.substringEndIndexOf(title, StringConstants.FEAT_CONSTANT);
        }

        if (artist.contains(StringConstants.FEAT_CONSTANT)) {
            artist = MetadataUtils.substringEndIndexOf(artist, StringConstants.FEAT_CONSTANT);
        }

        if (artist.contains(StringConstants.AMPERSAND)) {
            artist = MetadataUtils.substringEndIndexOf(artist, StringConstants.AMPERSAND);
        }

        title = MetadataUtils.removeParenthesisContent(title).trim();
        title = MetadataUtils.removeRepeated(title, " ").trim();
        this.title = title;

        if (title.contains(StringConstants.AMPERSAND)) {
            title = title.replaceAll(StringConstants.AMPERSAND, "e");
        }

        registerExtractor(new BiggerLyricExtractionStrategy(2, STRING_DELIMITER));
        title = MetadataUtils.normalizeString(title);
        title = MetadataUtils.removeNonAlphaNumerical(title);
        title = title.replaceAll(" ", "-").trim();

        artist = MetadataUtils.normalizeString(artist);
        artist = artist.replaceAll("-", " ");
        artist = MetadataUtils.removeNonAlphaNumerical(artist);
        artist = artist.replaceAll(" ", "-").trim();

        return MetadataUtils.removeRepeated(String.format(URL_FORMAT, artist.toLowerCase(), title.toLowerCase()), "-");
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#cleanCandidate(String) Ø
     */
    @Override
    String cleanCandidate(String candidate) {
        // Do not need clean the candidate, already well formatted
        return candidate;
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#removeWorstCandidates(List)
     */
    @Override
    void removeWorstCandidates(List<String> candidates) {
        candidates.removeIf(c -> {
            int i = 0;
            Matcher matcher = Pattern.compile(".+\\n\\n.*\\n\\n").matcher(c);
            while (matcher.find() && i < 10) {
                i++;
            }
            return i >= 10;
        });
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
