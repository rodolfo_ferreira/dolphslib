package dolphs.media.lyrics.agents;

import dolphs.media.lyrics.exception.CouldNotExtractLyricException;
import dolphs.media.lyrics.extraction.ExtractionStrategy;
import dolphs.web.WebPage;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is the template method to creation of Lyric Agentes. It has the default methods to be called in extraction.
 */
public abstract class LyricAgent {

    private static final Logger logger = Logger.getLogger(LyricAgent.class.getName());
    String title;
    private List<ExtractionStrategy> strategyList;

    LyricAgent() {
        this.strategyList = new ArrayList<>();
    }

    /**
     * This method find the lyric of the given Artist and Title. This is not not a search engine, first it will get the
     * URL of the Agent and then extract its body html content. Then it uses all registered strategies to extract the
     * candidates to be the lyric string. After doing so it will remove known non lyric candidates and compare all
     * candidates to get the best one.
     *
     * @param artist, the artist of the lyric
     * @param title,  the title of the lyric
     * @return the best extracted candidate to be the lyric
     * @throws IOException,                   if some IO exception occurs in HTML extraction
     * @throws CouldNotExtractLyricException, if some error occurs during the extraction
     */
    public String findLyric(String artist, String title) throws IOException, CouldNotExtractLyricException {
        logger.log(Level.INFO, "Finding URL for {0} by {1}", new String[]{title, artist});
        String url = getUrl(artist, title);
        Element body = getBodyElementPage(url);
        logger.info("Content successfully get");
        List<String> candidates = extractCandidates(body);
        removeWorstCandidates(candidates);
        return extractBestLyric(candidates, title);
    }

    /**
     * This method register an strategy of extraction
     *
     * @param extractionStrategy, the strategy to be registered
     */
    void registerExtractor(ExtractionStrategy extractionStrategy) {
        this.strategyList.add(extractionStrategy);
    }

    /**
     * This method extracts the candidates of the body element. It will iterate over the extraction strategies applying
     * the element body and each result will be a candidate to be lyric.
     *
     * @param body, the body element to have the strategies applied
     * @return a list of candidates of each strategy
     */
    private List<String> extractCandidates(Element body) {
        logger.log(Level.INFO, "Finding best lyric");

        List<String> results = new ArrayList<>();
        List<Thread> threads = startExtractions(body, results);

        while (areThreadsRunning(threads)) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return results;
    }

    private List<Thread> startExtractions(Element body, List<String> results) {
        List<Thread> threads = new ArrayList<>();

        for (ExtractionStrategy strategy : strategyList) {
            Thread thread = new Thread(() -> {
                String lyricCandidate = cleanCandidate(strategy.extract(body, title));
                results.add(lyricCandidate);
            });
            threads.add(thread);
            thread.start();
        }

        return threads;
    }

    private boolean areThreadsRunning(List<Thread> threads) {
        return threads.stream().anyMatch(Thread::isAlive);
    }

    /**
     * This method extracts the best lyric between the candidates. It will check the recurrence of the title and the
     * length of the string to choose the best lyric.
     *
     * @param results, the candidates to be the lyric
     * @param title,   the title of the lyric
     * @return the best candidate to be the lyric
     * @throws CouldNotExtractLyricException, if some error has occur during the extraction
     */
    private String extractBestLyric(List<String> results, String title) throws CouldNotExtractLyricException {
        String lyric;
        String biggerString = null;
        String mostRecurrentString = null;
        int biggestSize = 0;
        int biggestRecurrence = 0;

        for (String result : results) {

            if (result.toLowerCase().contains(title.toLowerCase())) {
                int titleCount = StringUtils.countMatches(result, title.toLowerCase());
                if (titleCount >= biggestRecurrence) {
                    mostRecurrentString = result;
                    biggestRecurrence = titleCount;
                }
            }

            if (result.length() > biggestSize) {
                biggerString = result;
                biggestSize = result.length();
            }
        }

        if (mostRecurrentString != null) {
            logger.log(Level.INFO, "The best candidate has {0} occurrences of the title in the lyric", biggestRecurrence);
            lyric = mostRecurrentString;
        } else {
            logger.log(Level.INFO, "Lyrics does not have the title, getting the biggest");
            lyric = biggerString;
        }

        if (lyric == null || lyric.length() == 0 || lyric.contains("We detected that your IP is blocked.")) {
            throw new CouldNotExtractLyricException("Lyric was not found");
        }

        return lyric;
    }

    /**
     * This method simple gets the HTML body element of the given URL.
     *
     * @param url, the url to have its body extracted
     * @return the body element of the HTML of the URL
     * @throws IOException, it will be thrown if the address could not be reached
     */
    private Element getBodyElementPage(String url) throws IOException {
        logger.log(Level.INFO, "Connecting to {0}", url);
        WebPage wp = new WebPage(url);
        wp.connect();
        Document document = Jsoup.parse(wp.getHtml().replace(StringConstants.BR_HTML_TAG, ""));
        document.outputSettings(new Document.OutputSettings().prettyPrint(false));
        return document.body();
    }

    /**
     * This method gets the URL of artist and title. This method will be implemented to each subclass to build the url
     * matching the logic of URL build of each agent.
     *
     * @param artist, the artist to be used to build the url
     * @param title,  the title to be used to build the url
     * @return the URL to the agent of the lyric of given artist and title.
     */
    abstract String getUrl(String artist, String title);

    /**
     * This method removes the worst candidates to be the lyric. The logic containing in this method is to detect and
     * remove known strings that are not lyrical strings
     *
     * @param candidates, the candidates to be checked and removed if matches the rule of worst candidate
     */
    abstract void removeWorstCandidates(List<String> candidates);

    /**
     * This part is once the lyric has been extracted and chosen it will clean it. Remove extra break lines, spaces,
     * unknown characters and things like that. Each agent has its rules.
     *
     * @param candidate, the lyric extracted
     * @return the lyric extracted and cleaned
     */
    abstract String cleanCandidate(String candidate);

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}