package dolphs.media.lyrics.agents;

import dolphs.media.lyrics.agents.util.LyricManipulationUtils;
import dolphs.media.lyrics.agents.util.MetadataUtils;
import dolphs.media.lyrics.extraction.BiggerLyricExtractionStrategy;
import dolphs.media.lyrics.extraction.RecurrenceLyricExtractionStrategy;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by rodol on 03/12/2016.
 */

public class MusixMatch extends LyricAgent {

    private static final String URL_FORMAT = "https://www.musixmatch.com/lyrics/%s/%s";
    private static final String STRING_DELIMITER = ".*(No|[0-9]+) (T|t)ranslation[s]* available.*";

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#getUrl(String, String)
     */
    @Override
    String getUrl(String artist, String title) {

        title = MetadataUtils.normalizeString(title);
        artist = MetadataUtils.normalizeString(artist);

        if (title.contains(StringConstants.FEAT_CONSTANT)) {
            artist += " " + title.substring(title.indexOf(StringConstants.FEAT_CONSTANT));
            title = MetadataUtils.substringEndIndexOf(title, StringConstants.FEAT_CONSTANT);
        }

        artist = MetadataUtils.removeRepeated(artist, " ");
        artist = MetadataUtils.removeNonAlphaNumerical(artist).trim();

        title = MetadataUtils.removeRepeated(title, " ");

        this.title = title;
        registerExtractor(new BiggerLyricExtractionStrategy(3, STRING_DELIMITER));
        registerExtractor(new RecurrenceLyricExtractionStrategy(3, false, STRING_DELIMITER));

        title = MetadataUtils.removeNonAlphaNumerical(title);
        title = title.replaceAll("[ \']+", "-");
        title = title.trim();

        return String.format(URL_FORMAT, artist.replaceAll(" ", "-"), title.replaceAll(" ", "-"));
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#cleanCandidate(String) Ø
     */
    @Override
    String cleanCandidate(String candidate) {
        // Replaces one or more break lines by one break line only
        candidate = candidate.replaceAll("[\n]+ ", "\n");

        candidate = candidate.replaceAll("[\n\r\t ]{3,}", "\n\n");
        candidate = LyricManipulationUtils.addBlankLineBeforeIfNoExist(candidate, "\\[");
        return candidate.trim();
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see LyricAgent#removeWorstCandidates(List)
     */
    void removeWorstCandidates(List<String> results) {
        results.removeIf(s -> StringUtils.countMatches(s, "Translation") > 2);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
