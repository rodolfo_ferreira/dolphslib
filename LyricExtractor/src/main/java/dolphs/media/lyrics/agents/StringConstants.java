package dolphs.media.lyrics.agents;

/**
 * This class holds all string constants used in Lyric Agents.
 */
class StringConstants {

    static final String AMPERSAND = "&";
    static final String BR_HTML_TAG = "<br>";
    static final String FEAT_CONSTANT = "feat";

    private StringConstants() {
    }
}
