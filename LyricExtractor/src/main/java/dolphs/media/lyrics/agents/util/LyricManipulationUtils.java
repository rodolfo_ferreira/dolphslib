package dolphs.media.lyrics.agents.util;

import java.util.regex.Pattern;

/**
 * This class is the utils used in Lyric manipulation.
 */
public class LyricManipulationUtils {

    private LyricManipulationUtils() {}

    /**
     * Removes extra break line of the given String. This should be used when ALL lines have an extra break line.
     *
     * @param lyric, the lyric to have its break lines removed
     * @return the Lyric without extra break lines.
     */
    public static String removeExtraBreakLines(String lyric) {
        return lyric.replaceAll("(.+)\n", "$1");
    }

    /**
     * This method squash the break lines of the string. When it find two or more break lines it will remove one until all
     * break lines has at tops 2 of it.
     *
     * @param lyric, the lyric to have its break lines removed
     * @return the Lyric without extra break lines.
     */
    public static String squashBreakLines(String lyric) {
        while (Pattern.compile(".+[\n\r]{3,}.+").matcher(lyric).find()) {
            lyric = lyric.replaceAll("[\n\r]{2}", "\n");
        }
        return lyric;
    }

    /**
     * Adds a break line before the given string in each occurrence of it in the lyric string if it has not a break line in it.
     *
     * @param lyric, the lyric to have the break line added before the given string
     * @param string, the string to have a break line added in its beginning if it does not exist
     * @return the lyric string with a blank break line before all occurrences of string
     */
    public static String addBlankLineBeforeIfNoExist(String lyric, String string) {
        return lyric.replaceAll("(.)\\n" + string, "$1\n\n" + string);
    }
}
