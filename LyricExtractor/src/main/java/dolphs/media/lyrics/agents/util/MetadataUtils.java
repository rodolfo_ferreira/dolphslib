package dolphs.media.lyrics.agents.util;

import java.text.Normalizer;

/**
 * This class contains the util methods to manipulate metadata as title and artist string.
 */
public class MetadataUtils {

    private MetadataUtils() {
    }

    /**
     * Normalizes a given string
     *
     * @param string, the string to be normalized
     * @return the string normalized
     */
    public static String normalizeString(String string) {
        return Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    /**
     * Removes the parenthesis and its content of the string. Eg.: Lorem (ipsum) -> Lorem
     *
     * @param string, the string to have its parenthesis content removed.
     * @return the string without the parenthesis and its content.
     */
    public static String removeParenthesisContent(String string) {
        return string.replaceAll("\\(.+\\)", "");
    }

    /**
     * Removes sequences of string. Eg.: Thiiis -> This
     *
     * @param string, the string to have its content replaced
     * @param sequence, the sequence to be replaced
     * @return the string without sequences of the given sequence
     */
    public static String removeRepeated(String string, String sequence) {
        return string.replaceAll("[" + sequence + "]{2,}", sequence);
    }

    /**
     * Removes all recurrences of the string. Eg.: House Pet -(Pet)=> House
     *
     * @param string, the String containing the content to be removed
     * @param remove, the content to be removed.
     * @return the string without the string to be removed.
     */
    public static String remove(String string, String remove) {
        return string.replaceAll(remove, "");
    }

    /**
     * Removes all non alphanumeric chars of the given string
     *
     * @param string, the string to have its non alphanumeric content removed.
     * @return a string alphanumeric only
     */
    public static String removeNonAlphaNumerical(String string) {
        return replaceNonAlphaNumerical(string, "");
    }

    /**
     * Replaces all non alphanumeric characters by the replacement string.
     *
     * @param string, the string to have its non alphanumeric content replaced
     * @param replacement, the replacement of the non alphanumeric content.
     * @return a string replaced the non alphanumerical with the replacement.
     */
    public static String replaceNonAlphaNumerical(String string, String replacement) {
        return string.replaceAll("[^\\dA-Za-z ]", replacement);
    }

    /**
     * Performs a {@link String#substring(int)} from the beginning until the index of the ending string.
     *
     * @param string, the string to be cut
     * @param endingString, the ending of the string
     * @return the string substring until the ending string
     */
    public static String substringEndIndexOf(String string, String endingString) {
        return string.substring(0, string.indexOf(endingString) - 1).trim();
    }
}
