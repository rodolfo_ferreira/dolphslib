package dolphs.media.lyrics.exception;

/**
 * This exception should be thrown when some error has occur in lyric extraction step
 */
public class CouldNotExtractLyricException extends Exception {

    public CouldNotExtractLyricException(String message) {
        super(message);
    }
}
