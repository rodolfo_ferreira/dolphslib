package dolphs.media.lyrics.extraction;

import org.jsoup.nodes.Element;

import java.util.logging.Logger;

/**
 * This class uses a bigger length logic to extract the content of an HTML element.
 */
public class BiggerLyricExtractionStrategy extends ExtractionStrategy {

    private final static Logger logger = Logger.getLogger(BiggerLyricExtractionStrategy.class.getName());

    public BiggerLyricExtractionStrategy(int parentFactor, String stringDelimiter) {
        super(parentFactor, stringDelimiter);
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see ExtractionStrategy#extract(Element, String)
     */
    @Override
    Element extractContent(Element body, String title) {
        logger.info("Finding biggest element");
        Element biggerElement = body;
        int biggerLength;

        while (!biggerElement.children().isEmpty()) {
            biggerLength = 0;
            for (Element elementChild : biggerElement.children()) {
                String text = elementChild.text();
                if (text.length() >= biggerLength) {
                    biggerLength = text.length();
                    biggerElement = elementChild;
                }
            }
        }

        return biggerElement;
    }
}
