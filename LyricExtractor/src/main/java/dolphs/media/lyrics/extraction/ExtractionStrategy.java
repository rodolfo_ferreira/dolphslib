package dolphs.media.lyrics.extraction;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

/**
 * This method uses template method to extract the content of a HTML body
 */
public abstract class ExtractionStrategy {

    private final int parentFactor;

    private String stringDelimiter;

    ExtractionStrategy(int parentFactor, String stringDelimiter) {
        this.parentFactor = parentFactor;
        this.stringDelimiter = stringDelimiter;
    }

    /**
     * This method will extract the best String content of the body element. It will extract the content and then uses
     * the {@link #parentFactor} and {@link #stringDelimiter} to choose the content coverage
     *
     * @param body, the element to have its content extracted
     * @return the string containing the relevant content
     */
    public String extract(Element body, String title) {
        Element element = extractContent(body, title);

        if (stringDelimiter != null && !stringDelimiter.isEmpty()) {
            for (int i = 0; i < parentFactor; i++) {
                Element parent = element.parent();
                Elements wrongElement = parent.getElementsMatchingText(stringDelimiter);
                if (wrongElement.isEmpty()) {
                    element = parent;
                }
            }
        }

        return Jsoup.clean(element.html(), "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false)).trim();
    }

    /**
     * This method is called to extract the content, it will return the part of the body element has the content.
     *
     * @param body, the body element to be evaluated
     * @return the Element that has the content matching the strategy rule
     */
    abstract Element extractContent(Element body, String title);
}
