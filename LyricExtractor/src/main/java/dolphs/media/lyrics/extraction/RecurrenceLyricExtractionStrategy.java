package dolphs.media.lyrics.extraction;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;

import java.util.logging.Logger;

/**
 * This class uses a recurrence logic to extract the content of an HTML element.
 */
public class RecurrenceLyricExtractionStrategy extends ExtractionStrategy {

    private final static Logger logger = Logger.getLogger(RecurrenceLyricExtractionStrategy.class.getName());

    private boolean isLyricRecurrence;

    public RecurrenceLyricExtractionStrategy(int parentFactor, boolean lyricRecurrence, String stringDelimiter) {
        super(parentFactor, stringDelimiter);
        this.isLyricRecurrence = lyricRecurrence;
    }

    /**
     * (non-Javadoc)
     * <p>
     * Implementation of abstract method
     *
     * @see ExtractionStrategy#extract(Element, String)
     */
    @Override
    Element extractContent(Element body, String title) {
        logger.info("Finding element with the most occurrence of " + title);
        Element moreRecurrentElement = body;
        int biggerRecurrence;
        int biggerLyricRecurrence;

        while (!moreRecurrentElement.children().isEmpty()) {
            biggerRecurrence = 0;
            biggerLyricRecurrence = 0;
            for (Element elementChild : moreRecurrentElement.children()) {
                String text = elementChild.text();
                int recurrence = StringUtils.countMatches(text.toLowerCase(), title.toLowerCase());
                int lyricRecurrence = StringUtils.countMatches(elementChild.html(), "lyric".toLowerCase());
                if ((recurrence >= biggerRecurrence && isLyricRecurrence && lyricRecurrence >= biggerLyricRecurrence) || recurrence >= biggerRecurrence && !isLyricRecurrence) {
                    biggerRecurrence = recurrence;
                    biggerLyricRecurrence = lyricRecurrence;
                    moreRecurrentElement = elementChild;
                }
            }
        }

        return moreRecurrentElement;
    }
}
