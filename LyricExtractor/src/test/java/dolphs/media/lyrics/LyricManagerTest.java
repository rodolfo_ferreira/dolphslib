package dolphs.media.lyrics;

import dolphs.media.lyrics.agents.*;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LyricManagerTest {

    private LyricManager lyricManager;

    @Before
    public void setUp() throws Exception {
        lyricManager = new LyricManager();
        lyricManager.registerLyricAgent(new MusixMatch());
        lyricManager.registerLyricAgent(new AzLyrics());
        lyricManager.registerLyricAgent(new GeniusLyrics());
        lyricManager.registerLyricAgent(new LetrasMusLyrics());
    }

    @Test
    public void findBestLyric() {

        assertLyricCheck("Demi Lovato", "Sorry Not Sorry", "Payback is a bad bitch");
        assertLyricCheck("Lady GaGa", "Boys boys boys", "Saw you twice at the pop show");
        assertLyricCheck("Ariana Grande", "Dangerous Woman", "Don't need permission");
        assertLyricCheck("Chloe Howl", "Magnetic", "we let it pull me back to you");
        assertLyricCheck("Ariana Grande", "No tears left to cry", "I wanna be in like all the time");
        assertLyricCheck("G-Eazy & Halsey", "Him & I", "Cross my heart hope to die");
        assertLyricCheck("Katy Perry", "Déjà Vu", "You suck my hope up in a vacuum");
        assertLyricCheck("Jojo", "Honest.", "Trees lose their leaves to the snow");
        assertLyricCheck("Andrew Lloyd Webber", "Memory", "She is smiling alone");
    }

    private void assertLyricCheck(String artist, String title, String part) {
        String lyric = lyricManager.findBestLyric(artist, title);
        Assert.assertNotNull(lyric);
        if (!lyric.toLowerCase().replaceAll(",", "").contains(part.toLowerCase())) {
            System.out.println(lyric);
        }
        Assert.assertTrue(lyric.toLowerCase().replaceAll(",", "").contains(part.toLowerCase()));
    }
}