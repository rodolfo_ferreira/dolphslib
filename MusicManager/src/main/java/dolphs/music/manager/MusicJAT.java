package dolphs.music.manager;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

public class MusicJAT {

    private final Tag musicTag;
    private AudioFile audioFile;

    public MusicJAT(AudioFile audioFile) {
        this.audioFile = audioFile;
        this.musicTag = audioFile.getTag();
    }

    public String getTitle() {
        String title = "";
        if (musicTag.getFirst(FieldKey.TITLE).length() > 0)
            title = musicTag.getFirst(FieldKey.TITLE);
        return title.trim();
    }

    public void setTitle(String title) {
        try {
            musicTag.setField(FieldKey.LYRICS, title);

        } catch (FieldDataInvalidException e) {
            e.printStackTrace();
        }

        try {
            audioFile.commit();
        } catch (CannotWriteException e) {
            e.printStackTrace();
        }
    }

    public String getAlbum() {
        String album = "";
        if (musicTag.getFirst(FieldKey.ALBUM).length() > 0)
            album = musicTag.getFirst(FieldKey.ALBUM);
        return album.trim();
    }

    public void setAlbum(String album) {
        try {
            musicTag.setField(FieldKey.LYRICS, album);
        } catch (FieldDataInvalidException e) {
            e.printStackTrace();
        }
        try {
            audioFile.commit();
        } catch (CannotWriteException e) {
            e.printStackTrace();
        }
    }

    public String getArtist() {
        String artist = "";
        if (musicTag.getFirst(FieldKey.ARTIST).length() > 0)
            artist = musicTag.getFirst(FieldKey.ARTIST);
        return artist.trim();
    }

    public void setArtist(String artist) {
        try {
            musicTag.setField(FieldKey.LYRICS, artist);
        } catch (FieldDataInvalidException e) {
            e.printStackTrace();
        }
        try {
            audioFile.commit();
        } catch (CannotWriteException e) {
            e.printStackTrace();
        }
    }

    public String getLyric() {
        String lyric = "";
        if (musicTag.getFirst(FieldKey.LYRICS).length() > 0)
            lyric = musicTag.getFirst(FieldKey.LYRICS);
        return lyric.trim();
    }

    public void setLyric(String lyric) {
        try {
            musicTag.setField(FieldKey.LYRICS, lyric);
        } catch (FieldDataInvalidException e) {
            e.printStackTrace();
        }
        try {
            audioFile.commit();
        } catch (CannotWriteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return String.format("%s by %s", getTitle(), getArtist());
    }
}
