package dolphs.music.manager;

import dolphs.io.FileFilterD;
import dolphs.io.FileUtils;
import dolphs.media.lyrics.LyricManager;
import dolphs.media.lyrics.agents.AzLyrics;
import dolphs.media.lyrics.agents.GeniusLyrics;
import dolphs.media.lyrics.agents.LetrasMusLyrics;
import dolphs.media.lyrics.agents.MusixMatch;
import dolphs.media.lyrics.exception.CouldNotExtractLyricException;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MusicManager {

    private static final Logger logger = Logger.getLogger(MusicManager.class.getSimpleName());

    private static final String OUTPUT_FOLDER =
            "D:\\Users\\rodol" + File.separator + "Music" + File.separator + "Available" + File.separator;

    private static final String INPUT_FOLDER = "D:\\Users\\rodol" + File.separator + "Downloads" + File.separator;

    public static void main(String[] args) {
        File directory = new File(INPUT_FOLDER);
        FileFilterD fileFilterD = new FileFilterD(false, true, ".mp3", ".m4a");
        List<File> files = FileUtils.list(directory, fileFilterD);
        files.forEach(MusicManager::process);
    }

    private static void process(File file) {
        MusicJAT musicJAT = null;
        try {
            musicJAT = new MusicJAT(AudioFileIO.read(file));
        } catch (CannotReadException | IOException | TagException | ReadOnlyFileException | InvalidAudioFrameException e) {
            logger.severe(e.getMessage());
        }
        if (musicJAT != null) {
            putLyric(musicJAT);

            try {
                structureAndMoveTo(musicJAT, file);
            } catch (IOException e) {
                logger.severe(e.getMessage());
            }

        }
    }

    private static void structureAndMoveTo(MusicJAT musicJAT, File file) throws IOException {

        // Checks if need move file and move it if does.
        File currDirectory = file.getParentFile();
        String newDirPath =
                OUTPUT_FOLDER + FileUtils.normalizeForWindows(musicJAT.getArtist().replaceAll("\\\\/", "") + File.separator + musicJAT.getAlbum().replaceAll("\\\\/", "") + File.separator);
        File newDirectory = new File(newDirPath);
        if (!currDirectory.equals(newDirectory)) {
            file = FileUtils.moveTo(file, newDirectory, true);
            if (file.exists()) {
                logger.log(Level.INFO, "File {0} moved to {1}", new String[]{file.getAbsolutePath(),
                        newDirectory.getAbsolutePath()});
            } else {
                logger.log(Level.SEVERE, "File {0} could not be moved to {1}", new String[]{file.getAbsolutePath(),
                        newDirectory.getAbsolutePath()});
            }
        }

        // Checks if need change the name of the file.
        String format = file.getName().substring(file.getName().lastIndexOf('.'));
        String newFileName = musicJAT.getTitle() + " - " + musicJAT.getArtist() + format;
        File newFileD = new File(file.getParent(), newFileName);

        if (!file.getAbsolutePath().equals(newFileD.getAbsolutePath())) {
            if (newFileD.exists()) {
                Files.delete(newFileD.toPath());
            }
            if (FileUtils.rename(file, newFileName).exists()) {
                logger.log(Level.INFO, "File renamed to {0}", new String[]{newFileName});

            } else {
                logger.log(Level.SEVERE, "File \"{0}\" could not be renamed to \"{1}\"",
                        new String[]{file.getName(), newFileName});
            }
        }

    }

    private static void putLyric(MusicJAT musicJAT) {
        if (musicJAT != null) {
            String lyric = musicJAT.getLyric();
            if (lyric == null || lyric.isEmpty()) {
                LyricManager lyricManager = new LyricManager();
                lyricManager.registerLyricAgent(new AzLyrics());
                lyricManager.registerLyricAgent(new GeniusLyrics());
                lyricManager.registerLyricAgent(new LetrasMusLyrics());
                lyricManager.registerLyricAgent(new MusixMatch());
                lyric = lyricManager.findBestLyric(musicJAT.getArtist(), musicJAT.getTitle());
                if (lyric != null && !lyric.isEmpty()) {
                    musicJAT.setLyric(lyric);
                    logger.info("Lyric found for " + musicJAT.getArtist() + " by " + musicJAT.getTitle());
                } else {
                    logger.info("Lyric was not found for " + musicJAT.getArtist() + " by " + musicJAT.getTitle());
                }
            }
        }
    }
}
