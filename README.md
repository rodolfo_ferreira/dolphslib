# README #

This project is just a library to reuse codes done by me

### What is this repository for? ###

* Manipulating files, strings, arrays.
* Abstract even more libraries as the StyledSheet
* Data Structure, Machine learning algorithms and others kind of algorithms

### How do I get set up? ###

* Download, import in your IDE then build the jar

### For further info ###

* For more information contact me through rodolfoandreferreira@gmail.com