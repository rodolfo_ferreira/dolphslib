package com.dolphs.rest.json;

import com.dolphs.rest.json.beans.ClassConstants;
import com.google.gson.Gson;
import com.sun.xml.internal.ws.util.StringUtils;

import java.util.*;

public class FileClass {

    private String className;
    private String packge;
    private Map<String, String> attributes;

    private FileClass() {
        this.attributes = new HashMap<>();
        this.className = "";
    }

    public FileClass(String className, String packge) {
        this();
        this.className = className + "DTO";
        this.packge = packge;
    }

    public String getClassObjectName() {
        return className;
    }

    public String getClassName() {
        return StringUtils.capitalize(className);
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void addAttribute(String name, String type) {
        this.attributes.put(name, type);
    }

    public String getFileContent() {
        return String.format(FormatterConstants.FILE_CLASS_FORMATTER, getPackageFormatted(), getImportsFormatted(),
                getClassContent());
    }

    private String getInnerContent() {
        StringBuilder innerContent = new StringBuilder("");
        Set<String> keys = attributes.keySet();
        for (String name : keys) {
            String type = attributes.get(name);
            innerContent.append(String.format(FormatterConstants.ANNOTATION_PROERTY_FORMATTER, name));
            innerContent.append(String.format(FormatterConstants.ATTRIBUTE_FORMATTER, type, name));
            innerContent.append("\n");
        }

        for (String name : keys) {
            String type = attributes.get(name);
            innerContent.append(String.format(FormatterConstants.METHOD_GET_FORMATTER, type, StringUtils.capitalize(name), name));
            innerContent.append("\n");
            innerContent.append(String.format(FormatterConstants.METHOD_SET_FORMATTER, StringUtils.capitalize(name), type, name,
                    name, name));
            innerContent.append("\n");
        }
        return innerContent.toString();
    }

    private String getPackageFormatted() {
        return String.format(FormatterConstants.PACKAGE_FORMATTER, packge);
    }

    private String getImportsFormatted() {
        StringBuilder result = new StringBuilder(String.format(FormatterConstants.IMPORT_FORMATTER, "com.google.gson.annotations",
                "SerializedName"));
        Set<String> objectDependecies = new HashSet<>();
        findListDependecy(objectDependecies);
        appendImport(result, objectDependecies);
        return result.toString();
    }

    private void appendImport(StringBuilder result, Set<String> objectDependecies) {
        for (String dependency : objectDependecies) {
            boolean matchesList = dependency.equals("List");
            if(!ClassConstants.NON_IMPORTABLE_TYPES.contains(dependency)) {
                result.append(String.format(FormatterConstants.IMPORT_FORMATTER, packge, dependency));
            } else if (matchesList) {
                result.append(String.format(FormatterConstants.IMPORT_FORMATTER, "java.util", "List"));
            }
        }
    }

    private void findListDependecy(Set<String> objectDependecies) {
        for (Map.Entry<String, String> attribute : attributes.entrySet()) {
            String type = attribute.getValue();
            if (type.matches("List<.+>")) {
                objectDependecies.add("List");
                type = type.replaceAll("List<(.+)>", "$1");
            }
            objectDependecies.add(type);
        }
    }

    private String getClassContent() {
        return String.format(FormatterConstants.CLASS_FORMATTER, StringUtils.capitalize(className), getInnerContent());
    }

    @Override
    public int hashCode() {
        int result = 0;
        Set<String> att = this.attributes.keySet();
        for (String attribute : att) {
            result += Objects.hashCode(attribute);
        }
        result += Objects.hashCode(className);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        Set<String> att = this.attributes.keySet();
        if (obj instanceof FileClass) {
            FileClass externalFileClass = (FileClass) obj;
            if (this.getClassName().equalsIgnoreCase(externalFileClass.getClassName())) {
                result = true;
            }
            if (this.attributes.isEmpty() || externalFileClass.getAttributes().isEmpty()) {
                return result;
            }
            Set<String> externalKeySet = externalFileClass.getAttributes().keySet();
            Set<String> internalKeySet = this.getAttributes().keySet();
            if (externalKeySet.containsAll(internalKeySet)) {
                if (externalKeySet.size() > internalKeySet.size()) {
                    attributes.putAll(externalFileClass.getAttributes());
                }
                result = true;
            } else if (internalKeySet.containsAll(externalKeySet)) {
                if (internalKeySet.size() > externalKeySet.size()) {
                    externalFileClass.getAttributes().putAll(attributes);
                }
                result = true;
            } else {
                if (this.attributes.keySet().size() == externalKeySet.size()) {
                    boolean hasAllAttributes = true;
                    for (String attribute : att) {
                        if (externalFileClass.getAttributes().get(attribute) == null) {
                            hasAllAttributes = false;
                        }
                    }
                    if (hasAllAttributes) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
