package com.dolphs.rest.json;

public class FormatterConstants {

    public static final String PRIMITIVE_ASSERTION_FORMATTER = "%s %s = %s;";
    public static final String OBJECT_ASSERTION_FORMATTER = "%s %s = new %s();";
    public static final String LIST_ASSERTION_FORMATTER = "List<%s> %s = new ArrayList<%s>();";
    public static final String OBJECT_SET_FORMATTER = "%s.set%s(%s);";
    public static final String IMPORT_FORMATTER = "import %s.%s;\n";
    public static final String FILE_CLASS_FORMATTER = "%s\n" + "\n" + "%s\n" + "\n" + "%s";
    public static final String PACKAGE_FORMATTER = "package %s;";
    public static final String CLASS_FORMATTER = "public class %s {\n    \n%s\n}";
    public static final String ANNOTATION_PROERTY_FORMATTER = "    @SerializedName(\"%s\")\n";
    public static final String ATTRIBUTE_FORMATTER = "    private %s %s;\n";
    public static final String METHOD_GET_FORMATTER = "    public %s get%s() {\n" + "        return %s;\n" + "    " +
            "}\n";
    public static final String METHOD_SET_FORMATTER = "    public void set%s(%s %s) {\n" + "        this.%s = %s;\n"
            + "" + "    " + "}\n";

    private FormatterConstants() {
    }
}
