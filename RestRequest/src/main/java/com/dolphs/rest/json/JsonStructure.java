package com.dolphs.rest.json;

import com.dolphs.rest.json.beans.ClassConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.dolphs.rest.json.beans.JsonMember;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class JsonStructure {

    private List<FileClass> classes;

    private JsonObject jsonObject;

    private String packge;

    private ObjectCreator objectCreator;

    public JsonStructure(JsonObject jsonObject, String packge) {
        this.jsonObject = jsonObject;
        this.packge = packge;
        this.classes = new ArrayList<>();
        this.objectCreator = new ObjectCreator(packge);
    }

    private FileClass createObjectClass(String classVariable, JsonObject jsonObject) {
        String classFileName = StringUtils.capitalize(classVariable);
        FileClass fileClass = new FileClass(classFileName, packge);

        updateFields(fileClass, jsonObject);

        if (!classes.contains(fileClass)) {
            classes.add(fileClass);
        }

        objectCreator.addNew(fileClass.getClassName(), classVariable);
        return fileClass;
    }

    private void updateFields(FileClass newFileClass, JsonElement jsonElement) {
        JsonMember jsonMember = getJsonMember(jsonElement);
        switch (jsonMember.getType()) {
            case ClassConstants.STRING_TYPE:
                newFileClass.setClassName("String");
                break;
            case ClassConstants.DOUBLE_PRIMITIVE_TYPE:
                newFileClass.setClassName("Double");
                break;
            case ClassConstants.INT_PRIMITIVE_TYPE:
                newFileClass.setClassName("Integer");
                break;
            case ClassConstants.OBJECT_TYPE:
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                updateObjectClass(newFileClass, jsonObject);
                break;
        }
    }

    private void updateObjectClass(FileClass fileClass, JsonObject jsonObject) {
        Set<String> keys = jsonObject.keySet();

        for (String attributeName : keys) {
            JsonMember member = getJsonMember(jsonObject.get(attributeName));
            member.setAttributeName(attributeName);

            if (member.getType().equals(ClassConstants.OBJECT_TYPE) ) {
                JsonObject newJsonObject = jsonObject.get(attributeName).getAsJsonObject();
                FileClass newFileClass = createObjectClass(attributeName, newJsonObject);
                member.setType(newFileClass.getClassName());
            } else if (member.getType().equals("List")) {
                JsonArray jsonArray = jsonObject.get(attributeName).getAsJsonArray();
                FileClass newFileClass = findFileClasseByAttributeName(attributeName);
                if (newFileClass == null) {
                    newFileClass = new FileClass(attributeName, packge);
                }
                for (JsonElement jsonElement : jsonArray) {
                    updateFields(newFileClass, jsonElement);
                }

                int index = classes.indexOf(newFileClass);
                if (index == -1) {
                    member.setType("List<" + newFileClass.getClassName() + ">");
                    classes.add(newFileClass);
                } else {
                    FileClass listClass = classes.get(index);
                    member.setType("List<" + listClass.getClassName() + ">");
                }
            }

            objectCreator.addAssertion(member.getType(), member.getAttributeName(), member.getValue());
            fileClass.addAttribute(member.getAttributeName(), member.getType());
        }
    }

    private FileClass findFileClasseByAttributeName(String attributeName) {
        FileClass result = null;
        for (FileClass fileClass : classes) {
            if (fileClass.getClassName().equalsIgnoreCase(attributeName)) {
                result = fileClass;
                break;
            }
        }
        return result;
    }

    private JsonMember getJsonMember(JsonElement jsonElement) {
        JsonMember jsonMember = new JsonMember();
        String value = "";
        String type = ClassConstants.OBJECT_TYPE;

        String valTmp = isString(jsonElement);
        if (valTmp != null) {
            type = ClassConstants.STRING_TYPE;
            value = valTmp;
        }

        valTmp = isBoolean(jsonElement);
        if (valTmp != null) {
            type = ClassConstants.BOOLEAN_PRIMITIVE_TYPE;
            value = valTmp;
        }

        valTmp = isDouble(jsonElement);
        if (valTmp != null) {
            type = ClassConstants.DOUBLE_PRIMITIVE_TYPE;
            value = valTmp;
        }

        valTmp = isInt(jsonElement);
        if (valTmp != null) {
            type = ClassConstants.INT_PRIMITIVE_TYPE;
            value = valTmp;
        }

        valTmp = isObject(jsonElement);
        if (valTmp != null) {
            type = ClassConstants.OBJECT_TYPE;
            value = valTmp;
        }

        valTmp = isList(jsonElement);
        if (valTmp != null) {
            type = ClassConstants.LIST_TYPE;
            value = valTmp;
        }

        jsonMember.setValue(value);
        jsonMember.setType(type);

        return jsonMember;
    }

    private String isBoolean(JsonElement jsonElement) {
        String result;
        try {
            result = String.valueOf(jsonElement.getAsBoolean());
            if (!result.equals(jsonElement.getAsString())) {
                result = null;
            }
        } catch (UnsupportedOperationException | IllegalStateException | NumberFormatException e) {
            result = null;
        }
        return result;
    }

    public String isString(JsonElement jsonElement) {
        String result;
        try {
            result = jsonElement.getAsString();
        } catch (UnsupportedOperationException | IllegalStateException | NumberFormatException e) {
            result = null;
        }
        return result;
    }

    private String isDouble(JsonElement jsonElement) {
        String result;
        try {
            result = String.valueOf(jsonElement.getAsDouble());
        } catch (UnsupportedOperationException | IllegalStateException | NumberFormatException e) {
            result = null;
        }
        return result;
    }

    private String isInt(JsonElement jsonElement) {
        String result;
        try {
            result = String.valueOf(jsonElement.getAsInt());
        } catch (UnsupportedOperationException | IllegalStateException | NumberFormatException e) {
            result = null;
        }
        return result;
    }

    private String isObject(JsonElement jsonElement) {
        String result;
        try {
            result = jsonElement.getAsJsonObject().toString();
        } catch (UnsupportedOperationException | IllegalStateException e) {
            result = null;
        }
        return result;
    }

    private String isList(JsonElement jsonElement) {
        String result;
        try {
            result = jsonElement.getAsJsonArray().toString();
        } catch (UnsupportedOperationException | IllegalStateException e) {
            result = null;
        }
        return result;
    }

    public void createStructure(String upperClassName) {
        createObjectClass(upperClassName, this.jsonObject);
    }

    public void createFiles(String path) {
        File filePath = new File(path);
        filePath.mkdirs();

        for (FileClass fileClass : classes) {
            String fileStringPath = filePath.getAbsolutePath() + File.separator + fileClass.getClassName();
            File file = new File(fileStringPath + ".java");
            int i = 0;
            while (file.exists()) {
                file = new File(fileStringPath + i + ".java");
                i++;
            }
            System.out.println("Saving " + file.getAbsolutePath());
            String content = fileClass.getFileContent();
            try (FileWriter fileWriter = new FileWriter(file); BufferedWriter bufferedWriter = new BufferedWriter
                    (fileWriter)) {
                bufferedWriter.write(content);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String fileStringPath = filePath.getAbsolutePath() + File.separator + "ObjectCreator.java";
        File file = new File(fileStringPath);

        try (FileWriter fileWriter = new FileWriter(file); BufferedWriter bufferedWriter = new BufferedWriter
                (fileWriter)) {
            bufferedWriter.write(objectCreator.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
