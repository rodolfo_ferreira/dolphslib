package com.dolphs.rest.json;

import org.apache.commons.lang3.StringUtils;

public class ObjectCreator {

    private StringBuilder code;

    private String packge;

    private String psvmFormatter = "package %s;\n" + "\n" + "public class %s {\n" + "\n" + "    public static void " +
            "main(String[] args) {\n";

    public ObjectCreator(String packge) {
        this.code = new StringBuilder(String.format(psvmFormatter, packge, ObjectCreator.class.getSimpleName()));
    }

    public void addAssertion(String type, String attributeName, String value) {
        switch (type) {
            case "String":
                value = '"' + value + '"';
                break;
            case "int":
            case "double":
            case "boolean":
                append(String.format(FormatterConstants.PRIMITIVE_ASSERTION_FORMATTER, type, attributeName, value));
                break;
            case "Object":
                addNew(type, attributeName);
                break;
            case "List":
                append(String.format(FormatterConstants.LIST_ASSERTION_FORMATTER, StringUtils.capitalize(attributeName),
                        attributeName + "List", StringUtils.capitalize(attributeName)));
                break;
            default:
                append(String.format(FormatterConstants.OBJECT_ASSERTION_FORMATTER, type, attributeName, type, value));
                break;
        }
    }

    public void addSet(String objectVar, String objectAttribute, String var) {
        append(String.format(FormatterConstants.OBJECT_SET_FORMATTER, objectVar, objectAttribute, var));
    }

    public void addNew(String className, String classVariable) {
        append(String.format(FormatterConstants.OBJECT_ASSERTION_FORMATTER, StringUtils.capitalize(className), classVariable, StringUtils
                .capitalize(className)));
    }

    private void append(String string) {
        this.code.append("        ");
        this.code.append(string);
        this.code.append("\n");
    }

    @Override
    public String toString() {
        this.code.append("\n    }\n" + "}");
        return this.code.toString();
    }
}
