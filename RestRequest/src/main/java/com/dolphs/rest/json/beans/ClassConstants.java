package com.dolphs.rest.json.beans;

import java.util.Arrays;
import java.util.List;

public class ClassConstants {

    public static final String INTEGER_TYPE = "Integer";
    public static final String FLOAT_TYPE = "Float";
    public static final String DOUBLE_TYPE = "Double";
    public static final String LONG_TYPE = "Long";
    public static final String INT_PRIMITIVE_TYPE = "int";
    public static final String FLOAT_PRIMITIVE_TYPE = "float";
    public static final String DOUBLE_PRIMITIVE_TYPE = "double";
    public static final String LONG_PRIMITIVE_TYPE = "long";
    public static final String STRING_TYPE = "String";
    public static final String OBJECT_TYPE = "Object";
    public static final String BOOLEAN_PRIMITIVE_TYPE = "boolean";
    public static final String LIST_TYPE = "List";

    public static final List<String> NON_IMPORTABLE_TYPES = Arrays.asList(INTEGER_TYPE, FLOAT_TYPE, DOUBLE_TYPE, LONG_TYPE,
            INT_PRIMITIVE_TYPE, FLOAT_PRIMITIVE_TYPE, DOUBLE_PRIMITIVE_TYPE, LONG_PRIMITIVE_TYPE, STRING_TYPE,
            BOOLEAN_PRIMITIVE_TYPE, LIST_TYPE, OBJECT_TYPE);
}
