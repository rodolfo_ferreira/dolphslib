package com.dolphs.rest.json.beans;

import com.google.gson.Gson;

public class JsonMember {

    private String attributeName;

    private String value;

    private String type;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        if (attributeName.equals("class")) {
            attributeName = "cls";
        }
        this.attributeName = attributeName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
