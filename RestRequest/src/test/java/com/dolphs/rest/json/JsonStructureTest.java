package com.dolphs.rest.json;

import com.dolphs.rest.request.RestRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dolphs.exceptions.CouldNotReadException;
import dolphs.io.FileUtils;
import dolphs.io.exceptions.CouldNotWriteException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class JsonStructureTest {

    @Test
    public void main() throws IOException, CouldNotReadException, CouldNotWriteException {
        String outputPath = "classes";
        String field = "others";
        String url = String.format("");

        String path = outputPath + File.separator + field + File.separator;
        File file = new File(System.getProperty("user.home") + File.separator + "Documents" + File.separator + "jsonfile.txt");
        JsonStructure jsonStructure = createClassesByFile(file, "StatisticsInfo", "com.dolphs.music.domain");
        jsonStructure.createFiles(path);
    }

    @Test
    public void createStructure() throws IOException, CouldNotReadException, CouldNotWriteException {
        File file = new File(System.getProperty("user.home") + File.separator + "file.json");
        String json = "{\n" + "   \"time\": \"11:57:52 AM\",\n" + "   \"milliseconds_since_epoch\": 1531310272215,\n"
                + "   \"date\": \"07-11-2018\"\n" + "}";
        String url = "http://date.jsontest.com/";
        String outputPath = "classes";

        FileUtils.write(file, json);

        String baseName = "DateTime";
        String pack = "com.jsontest";

        JsonStructure jsonStructure0 = createClassesByFile(file, baseName, pack);
        JsonStructure jsonStructure1 = createClassesByString(json, baseName, pack);
        JsonStructure jsonStructure2 = createClassesByJsonRequest(url, baseName, pack);

        Assert.assertNotNull(jsonStructure0);
        Assert.assertNotNull(jsonStructure1);
        Assert.assertNotNull(jsonStructure2);

        String path0 = outputPath + File.separator + "file" + File.separator;
        String path1 = outputPath + File.separator + "json" + File.separator;
        String path2 = outputPath + File.separator + "url" + File.separator;

        jsonStructure0.createFiles(path0);
        jsonStructure1.createFiles(path1);
        jsonStructure2.createFiles(path2);

        Assert.assertTrue(new File(path0).exists());
        Assert.assertTrue(new File(path1).exists());
        Assert.assertTrue(new File(path2).exists());

        FileUtils.delete(new File(outputPath));
    }

    @Test
    public void api() throws IOException {
        String outputPath = "classes";
        String field = "others";
        String url = "https://us.api.battle.net/wow/data/talents?locale=en_US&apikey=87vwjf3xc3fcge59uqhw9b8vqr72h2w7";

        String path = outputPath + File.separator + field + File.separator;

        JsonStructure jsonStructure = createClassesByJsonRequest(url, "TalentsDTO", "");
        jsonStructure.createFiles(path);

    }

    private JsonStructure createClassesByFile(File file, String baseName, String pack) throws CouldNotReadException,
            IOException {
        String json = FileUtils.read(file);
        return createClassesByString(json, baseName, pack);
    }

    private JsonStructure createClassesByString(String json, String baseName, String pack) throws IOException {
        return createClasses(new Gson().fromJson(json, JsonObject.class), baseName, pack);
    }

    private JsonStructure createClassesByJsonRequest(String url, String baseName, String pack) throws IOException {
        return createClasses(RestRequest.get(url, JsonObject.class), baseName, pack);
    }

    private JsonStructure createClasses(JsonObject jsonObject, String baseName, String pack) throws IOException {
        System.out.println(jsonObject);
        JsonStructure jsonStructure = new JsonStructure(jsonObject, pack);
        jsonStructure.createStructure(baseName);
        return jsonStructure;
    }
}