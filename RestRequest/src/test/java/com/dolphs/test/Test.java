package com.dolphs.test;

import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Test {

    public static void main(String[] args) {
        int i = 0;
        int maxTries = 100;
        while (i++ < 10) {
            double num = new Random().nextDouble();
            num = Double.parseDouble(String.valueOf(num).replaceAll("\\.", "").substring(0, 4));

            String converted = convertString(String.valueOf(num));
            String reversed = reverse(converted);
            int it = 0;

            do {
                System.out.printf("Num %s, Tries %d\n", converted, it++);
                num = Float.parseFloat(converted) + Float.parseFloat(reversed);
                converted = convertString(String.valueOf(num));
                reversed = reverse(converted);
            } while (!converted.equals(reversed) && num < Math.pow(2, 100) && it < maxTries);

            if (converted.equals(reversed)) {
                System.out.println("The string " + converted + " is palindrome");
            } else if (num > Math.pow(2, 100)) {
                System.out.println("Max number reached");
            } else if (it >= maxTries) {
                System.out.println("Max tries reached");
            }
        }
    }

    private static String convertString(String valueOf) {
        if (valueOf.contains("E")) {
            valueOf = convertToNormal(valueOf);
        }
        return valueOf.replaceAll("\\.[0]+$", "").replaceAll("[0]+$", "");
    }

    private static String reverse(String converted) {
        StringBuilder reversed = new StringBuilder();
        for (int i = converted.length() - 1; i >= 0; i--) {
            reversed.append(converted.charAt(i));
        }
        return reversed.toString();
    }

    @org.junit.Test
    public void name() {
        List<String> soruces = Arrays.asList("1.220874E10", "2.321324E15", "3.3213214E12", "4.235469874E13", "5.63874E10", "6" +
                ".23374E15", "7.2354454E12", "8.874E13", "1.220874E-10", "2.321324E-15", "3.3213214E-12", "4.235469874E-13", "5" +
                ".63874E-10", "6.23374E-15", "7.2354454E-12", "8.874E-13");
        for (String source : soruces) {
            double sourceDouble = Double.parseDouble(source);
            String converted = convertToNormal(source);
            Assert.assertEquals(sourceDouble, Double.parseDouble(converted), 1e-15);
        }
    }

    public static String convertToNormal(String digit) {
        String casas = digit.substring(digit.indexOf("E") + 1);
        int num = Integer.parseInt(casas);
        StringBuilder value;
        if (num > 0) {

            int pos = digit.indexOf(".") + 1;
            int numZeros = num - (digit.substring(0, digit.indexOf("E")).length() - pos);

            value = new StringBuilder(digit.substring(0, digit.indexOf("E")).replaceAll("[\\.E]+", ""));
            for (int i = 0; i < numZeros; i++) {
                value.append("0");
            }
        } else {
            int pos = digit.indexOf(".") + 1;
            num *= -1;
            int numZeros = num - (digit.substring(0, digit.indexOf(".")).length());
            value = new StringBuilder("0.");

            for (int i = 0; i < numZeros; i++) {
                value.append("0");
            }
            value.append(digit.substring(0, digit.indexOf("E")).replaceAll("[\\.E]+", ""));
        }

        return value.toString();
    }
}
