package sheets;

import sheets.exception.ExtensionNotSupportedException;
import sheets.ioable.Ioable;
import sheets.beans.ISpreadBook;
import sheets.ioable.CsvIo;
import sheets.ioable.MicrosoftSpreadSheet2003;
import sheets.ioable.MicrosoftSpreadSheet2017;

import java.io.File;
import java.io.IOException;

public class SSheetIO {

    public static final int CSV_TYPE = 0;

    public static final int XLS_TYPE = 1;

    public static final int XLSX_TYPE = 2;

    public static ISpreadBook read(File file) throws IOException {
        String fileName = file.getName().toLowerCase();
        Ioable ioable;
        if (fileName.endsWith(".csv")) {
            ioable = new CsvIo();
        } else if (fileName.endsWith(".xls")) {
            ioable = new MicrosoftSpreadSheet2003();
        } else if (fileName.endsWith(".xlsx")) {
            ioable = new MicrosoftSpreadSheet2017();
        } else {
            throw new ExtensionNotSupportedException(fileName);
        }

        return ioable.read(file);

    }

    public static void write(ISpreadBook table, int fileType, File file) throws IOException {
        Ioable ioable;

        switch (fileType) {
            case CSV_TYPE:
                ioable = new CsvIo();
                break;
            case XLS_TYPE:
                ioable = new MicrosoftSpreadSheet2003();
                break;
            case XLSX_TYPE:
                ioable = new MicrosoftSpreadSheet2017();
                break;
            default:
                throw new ExtensionNotSupportedException(file.getName());
        }

        ioable.write(table, file);
    }

}
