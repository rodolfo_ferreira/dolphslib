package sheets.beans;

public interface ISpreadBook {


    ISpreadSheet createSheet(String sheetName);

    ISpreadSheet getSheetAt(int i);

    int getNumberOfSheets();

}
