package sheets.beans;

import java.util.List;

public interface ISpreadSheet {

    void addRow(List<Object> row);

    String getSheetName();

    void setSheetName(String name);

    int rowSize();

    List<Object> getRow(int i);

    void removeColumn(int index);
}
