package sheets.beans;

import sheets.exception.SheetNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class WorkingBook implements ISpreadBook {

    private List<ISpreadSheet> sheets;

    public WorkingBook() {
        this.sheets = new ArrayList<>();
    }

    public WorkingBook(String pageName) {
        this.sheets = new ArrayList<>();
        this.sheets.add(new WorkingSheet(pageName));
    }

    @Override
    public ISpreadSheet createSheet(String pageName) {
        int index = sheets.size();
        sheets.add(new WorkingSheet(pageName));
        return sheets.get(index);
    }

    @Override
    public ISpreadSheet getSheetAt(int i) {
        return sheets.get(i);
    }

    @Override
    public int getNumberOfSheets() {
        return sheets.size();
    }

    public ISpreadSheet createSheet() {
        int index = sheets.size();
        sheets.add(new WorkingSheet(""));
        return sheets.get(index);
    }

    public void removeSheet(String pageName) {
        int index = findPageIndex(pageName);

        if (index >= 0) {
            removeSheet(index);
        } else {
            throw new SheetNotFoundException(pageName);
        }
    }

    public void removeSheet(int index) {
        int size = sheets.size();
        if (index >= 0 && index < size) {
            sheets.remove(index);
        } else {
            throw new SheetNotFoundException(index, size);
        }
    }

    public void removeSheet(String curName, String newName) {
        int index = findPageIndex(curName);

        if (index >= 0) {
            removeSheet(index, newName);
        } else {
            throw new SheetNotFoundException(curName);
        }
    }

    public void removeSheet(int index, String newName) {
        int size = sheets.size();
        if (index < 0 || index >= size) {
            throw new SheetNotFoundException(index, size);
        }

        sheets.get(index).setSheetName(newName);
    }

    private int findPageIndex(String pageName) {
        int size = sheets.size();
        int count = 0;
        int index = -1;

        while (count < size && index == -1) {
            ISpreadSheet mySheet = sheets.get(count);
            if (mySheet.getSheetName().equalsIgnoreCase(pageName)) {
                index = count;
            }
            count++;
        }

        return index;
    }
}
