package sheets.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class WorkingSheet implements ISpreadSheet {

    private String name;

    private List<List<Object>> table;

    public WorkingSheet() {
        this.name = "";
        this.table = new ArrayList<>();
    }

    public WorkingSheet(String pageName) {
        this();
        this.name = pageName;
    }

    public String getSheetName() {
        return name;
    }

    public void setSheetName(String name) {
        this.name = name;
    }

    public void addRow(List<Object> values) {
        table.add(values);
    }

    public void removeRow(int index) {
        table.remove(index);
    }

    public void setRow(int index, List<Object> values) {
        table.set(index, values);
    }

    public List<Object> getRow(int index) {
        return table.get(index);
    }

    public void addColumn(List<String> values) {
        Iterator<String> valuesIterator = values.iterator();
        Iterator<List<Object>> tableIterator = table.iterator();
        while (valuesIterator.hasNext() && tableIterator.hasNext()) {
            String value = valuesIterator.next();
            List<Object> row = tableIterator.next();
            row.add(value);
        }
    }

    public void removeColumn(int index) {
        table.forEach(row -> row.remove(index));
    }

    public void setColumn(int index, List<String> values) {
        Iterator<String> valuesIterator = values.iterator();
        Iterator<List<Object>> tableIterator = table.iterator();
        while (valuesIterator.hasNext() && tableIterator.hasNext()) {
            String value = valuesIterator.next();
            List<Object> row = tableIterator.next();
            row.set(index, value);
        }
    }

    public List<Object> getColumn(int index) {
        List<Object> result = new ArrayList<>();
        table.forEach(row -> result.add(row.get(index)));
        return result;
    }

    public Object getCell(int row, int column) {
        return table.get(row).get(column);
    }

    public void setCell(int row, int column, String element) {
        table.get(row).set(column, element);
    }

    public int rowSize() {
        return table.size();
    }

    public int columnSize() {
        return table.isEmpty() ? 0 : table.get(0).size();
    }
}
