package sheets.exception;

public class SheetNotFoundException extends RuntimeException {

    private static final String FORMATTER_PAGE_NAME = "Could not found Sheet Page with name \"%s\"";
    private static final String FORMATTER_PAGE_INDEX = "Could not found Sheet Page at index %d, current size is %d";

    public SheetNotFoundException(String pageName) {
        super(String.format(FORMATTER_PAGE_NAME, pageName));
    }

    public SheetNotFoundException(int pageIndex, int size) {
        super(String.format(FORMATTER_PAGE_INDEX, pageIndex, size));
    }
}
