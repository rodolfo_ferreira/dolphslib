package sheets.ioable;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import sheets.beans.ISpreadBook;
import sheets.beans.ISpreadSheet;
import sheets.beans.WorkingBook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class AbstractMicrosftSpreadSheet {

    private Workbook workbook;

    public AbstractMicrosftSpreadSheet(Workbook workbook) {
        this.workbook = workbook;
    }

    public ISpreadBook read(File file) throws IOException {
        ISpreadBook workingBook = new WorkingBook();

        FileInputStream fis = new FileInputStream(file);

        readWorkbook(workingBook);

        return workingBook;
    }

    private void readWorkbook(ISpreadBook workingBook) {
        int size = workbook.getNumberOfSheets();
        for (int i = 0; i < size; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            populateSheet(sheet, workingBook.createSheet(sheet.getSheetName()));
        }
    }

    private void populateSheet(Sheet src, ISpreadSheet dst) {
        Iterator<Row> rowIterator = src.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            populateRow(dst, row.cellIterator());
        }
    }

    void populateRow(ISpreadSheet dst, Iterator<Cell> cellIterator) {
        List<Object> row = new ArrayList<>();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            switch (cell.getCellTypeEnum()) {
                case STRING:
                    row.add(cell.getStringCellValue());
                    break;
                case NUMERIC:
                    row.add(String.valueOf(cell.getNumericCellValue()));
                    break;
                case BOOLEAN:
                    row.add(String.valueOf(cell.getBooleanCellValue()));
                    break;
                default:
            }
        }
        dst.addRow(row);
    }

    public void write(ISpreadBook table, File file) throws IOException {
        FileOutputStream stream = new FileOutputStream(file.getAbsolutePath());

        populateWorkbook(table);

        workbook.write(stream);
    }

    private void populateWorkbook(ISpreadBook dst) {
        int size = dst.getNumberOfSheets();
        for (int i = 0; i < size; i++) {
            ISpreadSheet sheet = dst.getSheetAt(i);
            populateSheet(sheet, workbook.createSheet(sheet.getSheetName()));
        }
    }

    private void populateSheet(ISpreadSheet src, Sheet dst) {
        int rowSize = src.rowSize();
        for (int i = 0; i < rowSize; i++) {
            populateRow(dst, i, src.getRow(i));
        }
    }

    private void populateRow(Sheet src, int index, List<Object> values) {
        Row newRow = src.createRow(index);
        for (int i = 0; i < values.size(); i++) {
            CellStyle cellStyle = workbook.createCellStyle();
            DataFormat format = workbook.createDataFormat();
            Cell cell = newRow.createCell((short) i);
            if (values.get(i) instanceof Integer) {
                cellStyle.setDataFormat(format.getFormat("#"));
                cell.setCellValue((Integer) values.get(i));
                cell.setCellStyle(cellStyle);
            } else if (values.get(i) instanceof Double) {
                cellStyle.setDataFormat(format.getFormat("#"));
                cell.setCellValue((Double) values.get(i));
                cell.setCellStyle(cellStyle);
            } else if (values.get(i) instanceof String) {
                cell.setCellValue((String) values.get(i));
            } else if (values.get(i) instanceof Date) {
                cellStyle = workbook.createCellStyle();
                CreationHelper createHelper = workbook.getCreationHelper();
                cellStyle.setDataFormat(
                        createHelper.createDataFormat().getFormat("dd/mm/yyyy"));
                cell.setCellValue((Date) values.get(i));
                cell.setCellStyle(cellStyle);
            } else {
                cell.setCellValue(values.get(i).toString());
            }
        }
    }
}
