package sheets.ioable;

import sheets.beans.ISpreadBook;

import java.io.File;

public class CsvIo implements Ioable {

    @Override
    public ISpreadBook read(File file) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void write(ISpreadBook table, File file) {
        throw new UnsupportedOperationException();
    }
}
