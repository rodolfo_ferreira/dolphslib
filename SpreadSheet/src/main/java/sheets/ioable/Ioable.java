package sheets.ioable;

import sheets.beans.ISpreadBook;

import java.io.File;
import java.io.IOException;

public interface Ioable {

    ISpreadBook read(File file) throws IOException;

    void write(ISpreadBook table, File file) throws IOException;
}
