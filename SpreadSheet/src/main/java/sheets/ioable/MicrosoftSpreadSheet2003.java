package sheets.ioable;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class MicrosoftSpreadSheet2003 extends AbstractMicrosftSpreadSheet implements Ioable {

    public MicrosoftSpreadSheet2003() {
        super(new HSSFWorkbook());
    }
}
