package sheets.ioable;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MicrosoftSpreadSheet2017 extends AbstractMicrosftSpreadSheet implements Ioable {

    public MicrosoftSpreadSheet2017() {
        super(new XSSFWorkbook());
    }
}
